package eu.chorevolution.urbantrafficcoordination.seada.trafficareainformationcollection.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.trafficareainformationcollection.client.cdTrafficInformationCollector.AreaOfInterestRequest;
import eu.chorevolution.urbantrafficcoordination.seada.trafficareainformationcollection.client.cdTrafficInformationCollector.AreaOfInterestRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.trafficareainformationcollection.client.cdTrafficInformationCollector.TrafficInformationCollectorPT;
import eu.chorevolution.urbantrafficcoordination.seada.trafficareainformationcollection.client.cdTrafficInformationCollector.TrafficInformationCollectorService;

public class TrafficInformationCollector {
	
	private static Logger LOGGER = LoggerFactory.getLogger(TrafficInformationCollector.class);

	public static void main(String[] args) {
		
		LOGGER.info("*******TRAFFIC INFORMATION COLLECTOR*******");
		LOGGER.info("*      Set Area Of Interest 	  *");
		
		TrafficInformationCollectorService trafficInformationCollectorService = new TrafficInformationCollectorService();
		TrafficInformationCollectorPT trafficInformationCollectorPT = trafficInformationCollectorService.getTrafficInformationCollectorPort();
		
		AreaOfInterestRequest areaOfInterestRequest = new AreaOfInterestRequest();
		areaOfInterestRequest.setAreaName("Lindholmen2Nordstan");
		AreaOfInterestRequestType areaOfInterestRequestType = new AreaOfInterestRequestType();
		areaOfInterestRequestType.setMessageData(areaOfInterestRequest);
		
		trafficInformationCollectorPT.setAreaOfInterest(areaOfInterestRequestType);

	}
      
}
