var browserSync = require('browser-sync').create();
var gulp        = require('gulp');

gulp.task('browserSync', ['build'], function() {
  browserSync.init(['build/**'], {
    server: {
      baseDir: ['build', 'src'],
      middleware: function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
      }
    },
    port: 3010,
    https: true,
    cors: true,
    ghostMode: false
  });
});
