var Base = require('../base');
var Routing = Base.extend({

  constructor: function(options) {
    this.base(options);
    this.bindAll(this, 'start', 'error', 'success', 'fetchCombination');

    $('#load-combination').click(this.fetchCombination);
    var self = this;
    $('#player-start').click(function(){ if (self.player==null) {alert("Select routing service, Origin/Destination and press Load button first");} else {self.player.start(); $('#routing').toggle();}});
    $('#player-pause').click(function(){ if (self.player==null) {alert("Select routing service, Origin/Destination and press Load button first");} else self.player.pause(); });
    $('#player-reset').click(function(){ if (self.player==null) {alert("Select routing service, Origin/Destination and press Load button first");} else self.player.reset(); });

    ffwdme.on('routecalculation:start', this.start);
    ffwdme.on('routecalculation:error', this.error);
    ffwdme.on('routecalculation:success', this.success);
  },

  player: null,

  start: function(data) {
    console.info('routing started');
  },

  error: function(data) {
    console.error('routing FAILED');
  },

  success: function(response) {
    console.info('routing SUCCESSFULL!');
    ffwdme.navigation.setRoute(response.route).start();

    trackId = "Realtime";
    try {
      this.player = new ffwdme.GeoProviderRealtime({
        id: trackId,
        route: ffwdme.navigation.route,
        track: ffwdme.navigation.route.track
      });
    } catch(e) {
      $('#geoprovider-track').text('Could not fetch the recorded track!: ' + trackId);
    }
  },

  fetchCombination: function() {
    //var values = $('#select-combination').val().split(';');
    // var trackId = values[0];

    var slat = $('#custom-route-start-lat').val();
    var slng = $('#custom-route-start-lng').val();
    var dlat = $('#custom-route-dest-lat').val();
    var dlng = $('#custom-route-dest-lng').val();
    var routingProvider = $('#select-routing').val();

/*     if (this.player !== null) {
      this.player.reset();
    }; */

    ffwdme.reInitialize({
      apiKey: CREDENTIALS.routingApiKey,
      routing: routingProvider,
      bridgeStatus: false
    });
    new ffwdme.routingService({
      start: { lat: slat, lng: slng },
      dest:  { lat: dlat, lng: dlng }
    }).fetch();

    var data = {
      geoposition: new ffwdme.LatLng(slat, slng),
      point: new ffwdme.LatLng(slat, slng)
    };
    //ffwdme.trigger('geoposition:update', data);

    $('#geoprovider-track').text(routingProvider);
  },
});

module.exports = Routing;
