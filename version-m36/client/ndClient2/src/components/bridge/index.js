var Base = require('../base');
//var Say = require('say'); 

var Bridge = Base.extend({

    constructor: function (options) {
        this.base(options);
        this.render();
    },

    attrAccessible: ['map', 'grid'],

    iconElBridge: null,

    iconBridge: 'bridge/bridge.svg',
    iconBridge2: 'bridge/bridge2.svg',

    classes: 'w3-orange w3-container ffwdme-grid-w3 ffwdme-grid-h1 w3-center',

    imgUrl: function (icon) {
        return this.getRetinaImageUrl(ffwdme.defaults.imageBaseUrl + icon);
    },

    setIcons: function () {
        var img;

        if (!this.iconElBridge) {
            img = document.createElement('img');
            img.setAttribute("id", "iconBridge");
            this.iconElBridge = $(img).addClass('ffwdme-components-zoom').appendTo($(this.el));
        }
        this.iconElBridge[0].src = this.imgUrl(this.iconBridge2);
        $.ajax({
            type: "POST",
            url: "https://jinx.viktoria.chalmers.se:3009/routeSegmentBridgeStatusInformation",
            data: JSON.stringify({
                "getBridgeStatus": 0
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                //alert("Bridge is now " + (data.bridgeStatus? "openned": "closed"));
                ffwdme.options.bridgeStatus = data.bridgeStatus;
                if (data.bridgeStatus) {
                    $("#iconBridge").attr('src', 'components/bridge/bridge.svg');
                } else {
                    $("#iconBridge").attr('src', 'components/bridge/bridge2.svg');
                }
            },
            failure: function (errMsg) {

            }
        });
    },

    make: function () {
        this.base();
        var self = this;
        this.setIcons();
        $(this.iconElBridge).click(function (e) {
            e.stopPropagation();
            $.ajax({
                type: "POST",
                url: "https://jinx.viktoria.chalmers.se:3009/routeSegmentBridgeStatusInformation",
                data: JSON.stringify({
                    "toggleBridgeStatus": 0
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    ffwdme.options.bridgeStatus = data.bridgeStatus;
                    if (ffwdme.navigation.hasOwnProperty('route')) {
                        ffwdme.navigation.reroute();
                    }
                    
                    if (data.bridgeStatus) {
                        $("#iconBridge").attr('src', 'components/bridge/bridge.svg');
                    } else {
                        $("#iconBridge").attr('src', 'components/bridge/bridge2.svg');
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        });
        return this;
    }

});

module.exports = Bridge;