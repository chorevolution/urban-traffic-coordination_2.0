var BaseIcon = require('../base_icon');

var Eco = BaseIcon.extend({

  icon: 'eco/co2_ico.png',

  defaultUnit: 'g/km',
  
  navigationOnRoute: function(e) {
    //var eco = Math.round(Math.random() * (300) + 15);
    var eco = e.navInfo.raw.geoposition.coords.eco;
    var ecoThis = e.navInfo.raw.geoposition.coords.ecoThis;
    var ecoVariance = (parseFloat(ecoThis)-parseFloat(eco))/parseFloat(eco); 
    if (eco) this.label(ecoThis + "/" + eco);
    if (ecoVariance < 0.05) {
      this.labelColor("green");
    } else if (ecoVariance < 0.3) {
      this.labelColor("yellow");
    } else {
      this.labelColor("red");
    };
  }
});

module.exports = Eco;
