var BaseIcon = require('../base_icon');

var TrafficLight = BaseIcon.extend({

  icon: 'traffic_light/traffic-light-off.svg',
  defaultUnit: '',
  label: '',
  classes: 'w3-container ffwdme-grid-w3 ffwdme-grid-h1 w3-center',
  navigationOnRoute: function(e) {
    var currentTime = new Date().getTime();
    var currentTimeinCycle = currentTime % 30000;
    if (currentTimeinCycle <15000) {
      this.icon = 'traffic_light/traffic-light-red.svg';
    } else if (currentTimeinCycle <18000) {
      this.icon = 'traffic_light/traffic-light-yellow.svg';
    } else {
      this.icon = 'traffic_light/traffic-light-green.svg';
    }
    this.updateIcon(this.icon);
  }
});

module.exports = TrafficLight;
