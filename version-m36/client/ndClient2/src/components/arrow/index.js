var Base = require('../base');

var Arrow = Base.extend({

  constructor: function(options){
    this.base(options);

    this.turnTypeIconPaths = {
      'C':      'arrow/svg/continue.svg',//'arrow/straight.svg',
      'TL':     'arrow/svg/turn_left.svg',//'arrow/left.svg',
      'TSLL':   'arrow/svg/turn_slight_left.svg',//'arrow/half-left.svg',
      'TSHL':   'arrow/svg/turn_sharp_left.svg',//'arrow/hard-left.svg',
      'TR':     'arrow/svg/turn_right.svg',//'arrow/right.svg',
      'TSLR':   'arrow/svg/turn_slight_right.svg',//'arrow/half-right.svg',
      'TSHR':   'arrow/svg/turn_sharp_right.svg',//'arrow/hard-right.svg',
      'EXIT0':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT1':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT2':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT3':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT4':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT5':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'EXIT6':  'arrow/svg/roundabout.svg',//'arrow/roundabout.svg',
      'TU':     'arrow/svg/uturn.svg',//'arrow/u-turn.svg',
      'FINISH': 'arrow/svg/arrive.svg',//'arrow/flag.svg'
      'FL': 'arrow/svg/fork_left.svg',
      'FR': 'arrow/svg/fork_right.svg',
      'FS': 'arrow/svg/fork_straight.svg',
      'ML': 'arrow/svg/merge_left.svg',
      'MR': 'arrow/svg/merge_right.svg',
      'MS': 'arrow/svg/merge_slight_left.svg',
      'RL': 'arrow/svg/on_ramp_left.svg',
      'RR': 'arrow/svg/on_ramp_right.svg',
      'EL': 'arrow/svg/off_ramp_left.svg',
      'ER': 'arrow/svg/off_ramp_right.svg',
      'DEPART': 'arrow/svg/depart.svg',
      
    }; //turn types to icon files

    this.bindAll(this, 'onRoute');
    ffwdme.on('navigation:onroute', this.onRoute);

    this.render();
  },

  classes: 'ffwdme-components-container ffwdme-grid-w3 ffwdme-grid-h2',

  turnTypeIconPaths: null,

  arrowEl: null,

  labelEl: null,

  lastTurn: null,

  imgUrl: function(){
    return ffwdme.defaults.imageBaseUrl;
  },

  make: function() {
    this.base();

    //create arrow ele
    var img = document.createElement('img');
    img.src = this.getRetinaImageUrl(this.imgUrl() + this.turnTypeIconPaths.C); //straight as default
    this.arrowEl = $(img).addClass('ffwdme-components-small-arrow').appendTo($(this.el));

    //label for exit
    var label = document.createElement('span');
    this.labelEl = $(label).addClass('ffwdme-components-label').addClass('ffwdme-components-label-roundabout').appendTo($(this.el));
  },

  onRoute: function(e) {
    var turnType = null;

    if (e.navInfo.finalDirection === true){
      turnType = 'FINISH';
    } else {
      var direction = e.navInfo.nextDirection;
      if (!direction) return;

      turnType = direction.turnType;
      if (!turnType) return;


      if (/^EXIT/.test(turnType)){
        this.labelEl.html(turnType.split('EXIT')[1]);
      } else {
        this.labelEl.html('');
      }
    }

    this.updateIcon(turnType);
  },

  updateIcon: function(turnType){
    if (turnType != this.lastTurn){ // set img src only when turn type changes
      this.arrowEl[0].src = this.getRetinaImageUrl(this.imgUrl() + this.turnTypeIconPaths[turnType]);
      this.lastTurn = turnType;
    }
  }

});

module.exports = Arrow;
