var BaseIcon = require('../base_icon');

var Weather = BaseIcon.extend({

  icon: 'weather/weather_ico.svg',

  defaultUnit: '&#176;C',
  
  navigationOnRoute: function(e) {
    var weather = e.navInfo.currentDirection.weatherInfo.roadTemperature;
    if (weather) this.label(weather);
  }
});

module.exports = Weather;
