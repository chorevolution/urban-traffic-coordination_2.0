var Base = require('./base');
var Arrow = require('./arrow');
var ArrivalTime = require('./arrival_time');
var AudioInstructions = require('./audio_instructions');
var AutoReroute = require('./auto_reroute');
var AutoZoom = require('./auto_zoom');
var DistanceToDestination = require('./distance_to_destination');
var DistanceToNextTurn = require('./distance_to_next_turn');
var Leaflet = require('./leaflet');
var MapRotator = require('./map_rotator');
var NextStreet = require('./next_street');
var RouteOverview = require('./route_overview');
var Speed = require('./speed');
var Eco = require('./eco');
var Zoom = require('./zoom');
var Bridge = require('./bridge');
var TimeToDestination = require('./time_to_destination');
var Weather = require('./weather');
var Congestion = require('./congestion');
var Accidents = require('./accidents');
var BridgeStatus = require('./bridge_status');
var AccidentBridge = require('./accident_bridge');
var EcoSpeed = require('./eco_speed');
var TrafficLight = require('./traffic_light');
var Routing = require('./routing');
var GeoLocation = require('./geolocation');
var NavigationInfo = require('./nav_info');

;(function(ffwdme){
  ffwdme.components = {
    Base: Base,
    ArrivalTime: ArrivalTime,
    Arrow: Arrow,
    AudioInstructions: AudioInstructions,
    AutoReroute: AutoReroute,
    AutoZoom: AutoZoom,
    DistanceToDestination: DistanceToDestination,
    DistanceToNextTurn: DistanceToNextTurn,
    Leaflet: Leaflet,
    MapRotator: MapRotator,
    NextStreet: NextStreet,
    RouteOverview: RouteOverview,
    Speed: Speed,
    Eco: Eco,
    TimeToDestination: TimeToDestination,
    Zoom: Zoom,
    Bridge: Bridge,
    Weather: Weather,
    Congestion: Congestion,
    Accidents: Accidents,
    BridgeStatus: BridgeStatus,
    EcoSpeed: EcoSpeed,
    AccidentBridge: AccidentBridge,
    TrafficLight: TrafficLight,
    //GeoLocation: GeoLocation,
    Routing: Routing,
    NavigationInfo: NavigationInfo
  };
})(ffwdme);
