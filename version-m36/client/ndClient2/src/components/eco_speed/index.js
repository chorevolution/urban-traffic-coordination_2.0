var BaseIcon = require('../base_icon');

var EcoSpeed = BaseIcon.extend({

  icon: 'eco_speed/eco_car.svg',

  defaultUnit: 'km/h',

  navigationOnRoute: function(e) {
    var speed = e.navInfo.currentDirection.ecoSpeed;
    if (speed) this.label(speed);
  }
});

module.exports = EcoSpeed;
