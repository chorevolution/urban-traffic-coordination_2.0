var Base = require('../base');
var rbush = require('../../core/utils/rbush');

var Leaflet = Base.extend({
  /**
   * Max Zoom is 18
   * @augments ffwdme.Class
   * @constructs
   *
   */
  constructor: function (options) {
    this.base(options);
    this.bindAll(this, 'resize', 'drawRoute', 'drawMarkerWithoutRoute', 'onRouteSuccess', 'navigationOnRoute', 'navigationOffRoute', 'rotateMarker', 'setupMap');

    Leaflet.defineLeafletExtensions();

    this.mapReadyCallbacks = [];

    this.setupMap();

    ffwdme.on('geoposition:update', this.drawMarkerWithoutRoute);
    ffwdme.on('routecalculation:success', this.onRouteSuccess);
    ffwdme.on('routecalculation:success', this.drawRoute);
    ffwdme.on('reroutecalculation:success', this.drawRoute);
  },

  attrAccessible: ['el', 'apiKey'],

  map: null,

  polylines: null,

  helpLine: null,

  marker: null,

  origin: null,

  markerIcon: null,

  zoomLevel: 17,

  inRoutingMode: false,

  inRouteOverview: false,

  mapReady: false,

  mapReadyCallbacks: null,

  userZoom: 0,

  trafficLightIcon: [],

  canControlMap: function (component) {
    return true; // Added to test if can control Leaflet map 09 Nov 2017
    if (component instanceof ffwdme.components.AutoZoom && this.inRouteOverview) {
      return false;
    }
    if (component instanceof ffwdme.components.MapRotator && this.inRouteOverview) {
      return false;
    }
    return true;
  },

  setupEventsOnMapReady: function () {
    ffwdme.on('navigation:onroute', this.navigationOnRoute);
    // ffwdme.on('navigation:offroute', this.navigationOffRoute);
    ffwdme.on('geoposition:update', this.rotateMarker);
  },

  setupMap: function (bridgeStatus) {
    var destination = new L.LatLng(this.options.center.lat, this.options.center.lng);

    this.map = new L.Map(this.el.attr('id'), {
      attributionControl: false,
      zoomControl: true
    });
    L.control.attribution({
      position: 'topleft',
      attribution: 'Maps © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors © <a href= "http://cartodb.com/attributions#basemaps">CartoDB</a>'
    }).addTo(this.map);
    var myLayer = L.tileLayer(this.options.tileURL, {
      minZoom: 5,
      maxZoom: 18,
      id: 'myLayer',
      interactive: true
    }).addTo(this.map);

    // display traffic light on map
    var trafficLightLayer = L.LayerGroup.collision({
      margin: 5
    });
    var trafficLightArray = require('../data/trafficLightsGoteborg.json');
    trafficLightIcon = [];
    for (var i = 0; i < trafficLightArray.length; i++) {
      trafficLightIcon[i] = new L.Icon({
        iconUrl: ffwdme.defaults.imageBaseUrl + 'traffic_light/semaforo.svg', //map_marker.png
        shadowUrl: '',
        iconSize: new L.Point(20, 20),
        shadowSize: new L.Point(20, 20),
        iconAnchor: new L.Point(10, 10),
        popupAnchor: new L.Point(-3, -7)
      });
      var tMarker = L.marker([parseFloat(trafficLightArray[i].lat), parseFloat(trafficLightArray[i].lon)], {
        icon: trafficLightIcon[i],
        title: "Traffic light "+trafficLightArray[i].id+" at: "+trafficLightArray[i].lat+' ,'+trafficLightArray[i].lon,
        interactive: true, // Post-0.7.3
        clickable: true //      0.7.3
      }).bindPopup('Traffic-Light');
/*       tMarker.on('click', function(e) {
        var popup = e.target.getPopup();
        $.ajax({
          url: 'https://jinx.viktoria.chalmers.se:3009/routeSegmentBridgeStatusInformation',
          type: 'POST',
          dataType: 'json',
          headers: ['"content-type":"application/json"'],
          body: '{"toggleBridgeStatus": 20}',
          success: function(response){
            popup.setContent("Hello");
            popup.update();
          }
        });
      }); */
      trafficLightLayer.addLayer(tMarker);
    }
    trafficLightLayer.addTo(this.map);

    /*     new L.Control.GPlaceAutocomplete({
          callback: function(place){
            var loc = place.geometry.location;
            this.map.setView( [loc.lat(), loc.lng()], 18);
          }
        }).addTo(this.map); */

    this.map.on('click', function (e) { //Modify to just export to somewhere instead of hardcoding the HTML ID tag
      var popupContent = '<div">' +
        '<label><strong>Latitude:    </strong></label>' +
        '<input type="number" id="latitude" name="latitude"  class="w3-input"  value="' + e.latlng.lat.toFixed(6) + '">' +
        '<label><strong>Longitude: </strong></label>' +
        '<input type="number" id="longitude" name="longitude"  class="w3-input"  value="' + e.latlng.lng.toFixed(6) + '">' +
        '</div>' +
        '<div style="text-align:center;"><button type="button" class="w3-orange w3-round w3-btn" onClick = "{$(\'#custom-route-start-lat\').val(' + e.latlng.lat.toString() + ');' +
        '$(\'#custom-route-start-lng\').val(' + e.latlng.lng.toString() + '); var geocoder = new google.maps.Geocoder; geocodeLatLng(geocoder, \'start\');' +
        '}" >Set Origin</button><button type="button"  class="w3-green w3-round w3-btn"  onClick = "{$(\'#custom-route-dest-lat\').val(' + e.latlng.lat.toString() + ');' +
        '$(\'#custom-route-dest-lng\').val(' + e.latlng.lng.toString() + '); var geocoder = new google.maps.Geocoder; geocodeLatLng(geocoder, \'end\');' +
        '}" >Set Destination</button></div>';

      var formpopup = L.popup({attribution: "SEADA"})
        .setLatLng(e.latlng)
        .setContent(popupContent) //('<a href="' + e.latlng + '" target="_blank">Form</a>')
        .openOn(this);

    });

    if (!this.options.disableLeafletLocate) {
      this.map.locate({
        setView: true,
        maxZoom: 17
      });
    }

    this.setupEventsOnMapReady();

    for (var i = 0; i < this.mapReadyCallbacks.length; i++) {
      this.mapReadyCallbacks[i]();
    }

    this.mapReadyCallbacks = [];

    this.mapReady = true;
  },


  hideMarker: function () {
    this.marker && $(this.marker._icon).hide();
  },

  rotateMarker: function (e) {
    var heading = e.geoposition.coords.heading;
    if (!isNaN(heading) && heading !== null && this.marker) {
      this.marker.setIconAngle(heading);
    }
  },

  drawMarkerWithoutRoute: function (e) {
    if (this.inRoutingMode) return;

    var markerIcon;

    if (!this.marker) {
      markerIcon = new L.Icon({
        iconUrl: ffwdme.defaults.imageBaseUrl + 'leaflet/car.svg', //map_marker.png
        shadowUrl: ffwdme.defaults.imageBaseUrl + 'leaflet/map_marker_shadow.png',
        iconSize: new L.Point(50, 45),
        shadowSize: new L.Point(40, 40),
        iconAnchor: new L.Point(25, 27),
        popupAnchor: new L.Point(-3, -76)
      });

      this.marker = new L.Compass(e.point, {
        icon: markerIcon
      });
      this.disableMarker || this.map.addLayer(this.marker);
    } else {
      this.drawMarkerOnMap(e.point.lat, e.point.lng, true);
    }

    var bridgeLine = new L.Polyline([
      [57.715419, 11.965735],
      [57.713871, 11.967923]
    ], {
      color: 'green',
      opacity: 1,
      weight: 16
    });
    var bridgeLine2 = new L.Polyline([
      [57.715419, 11.965735],
      [57.713871, 11.967923]
    ], {
      color: 'white',
      opacity: 1,
      weight: 12
    });
    this.map.addLayer(bridgeLine);
    this.map.addLayer(bridgeLine2);
  },

  drawRoute: function (e) {
    if (!this.mapReady) {
      var self = this;
      this.mapReadyCallbacks.push(function () {
        self.removeHelpLine();
        self.drawPolylineOnMap(e.route, false);
      });
      return;
    }


    this.removeHelpLine();
    this.drawPolylineOnMap(e.route, false);
  },

  onRouteSuccess: function (e) {
    this.inRoutingMode = true;

    var destination = e.route.destination();

    var finishMarkerIcon = new L.Icon({
      iconUrl: ffwdme.defaults.imageBaseUrl + 'leaflet/map_marker_finish.png',
      shadowUrl: ffwdme.defaults.imageBaseUrl + 'leaflet/map_marker_shadow.png',
      iconSize: new L.Point(32, 32),
      shadowSize: new L.Point(32, 32),
      iconAnchor: new L.Point(16, 32),
      popupAnchor: new L.Point(-3, -76)
    });

    if (!this.finishMarker) {
      this.finishMarker = new L.Marker(destination, {
        icon: finishMarkerIcon
      });
      this.map.addLayer(this.finishMarker);
    } else {
      this.finishMarker.setLatLng(destination);
    }

  },

  navigationOnRoute: function (e) {
    var p = e.navInfo.position;
    this.removeHelpLine();
    this.drawMarkerOnMap(p.lat, p.lng, true);
  },

  navigationOffRoute: function (e) {
    var p = e.navInfo.positionRaw;
    this.drawMarkerOnMap(p.lat, p.lng, true);
    this.drawHelpLine(e.navInfo.positionRaw, e.navInfo.position);
  },

  drawPolylineOnMap: function (route, center) {
    var directions = route.directions,
      len = directions.length,
      len2, path;

    var point, latlngs = [];
    for (var i = 0; i < len; i++) {
      if (directions[i].path) {
        path = directions[i].path;
        len2 = path.length;
        for (var j = 0; j < len2; j++) {
          latlngs.push(new L.LatLng(path[j].lat, path[j].lng));
        }
      }
    }

    if (!this.polylines) {
      this.polylines = {};

      this.polylines.underlay = new L.Polyline(latlngs, {
        color: 'brown',
        opacity: 1,
        weight: 8
      });
      this.polylines.overlay = new L.Polyline(latlngs, {
        color: 'blue',
        opacity: 1,
        weight: 4
      });

      this.map.addLayer(this.polylines.underlay);
      this.map.addLayer(this.polylines.overlay);
    } else {
      this.polylines.underlay.setLatLngs(latlngs);
      this.polylines.overlay.setLatLngs(latlngs);
    }

    // zoom the map to the polyline
    if (center && !this.inRouteOverview) this.map.fitBounds(new L.LatLngBounds(latlngs));
  },

  drawMarkerOnMap: function (lat, lng, center) {
    var loc = new L.LatLng(lat, lng);
    this.marker.setLatLng(loc);
    if (center && !this.inRouteOverview) {
      //this.map.setView(loc, this.getZoom()); //comment this not to reset room when moving the car
      this.map.setView(loc);
    } else {
      this.map.fitBounds(this.polylines.overlay.getBounds());
    }
  },

  removeHelpLine: function () {
    this.helpLine && this.map.removeLayer(this.helpLine);
  },

  drawHelpLine: function (rawPos, desiredPos) {
    var latlngs = [
      new L.LatLng(rawPos.lat, rawPos.lng),
      new L.LatLng(desiredPos.lat, desiredPos.lng)
    ];

    if (!this.helpLine) {
      this.helpLine = new L.Polyline(latlngs, {
        color: 'blue',
        opacity: 0.5,
        weight: 2
      });
      this.map.addLayer(this.helpLine);
    } else {
      this.helpLine.setLatLngs(latlngs);
      this.map.addLayer(this.helpLine);
    }
  },

  changeUserZoom: function (value) {
    this.userZoom += value;
  },

  getZoom: function () {
    return this.zoomLevel + this.userZoom;
  },

  setZoom: function (zoom) {
    this.zoomLevel = zoom;
    return this.zoomLevel;
  },

  setMapContainerSize: function (width, height, top, left, rotate) {
    this.el && (this.el[0].style.transform = 'rotate(' + rotate + 'deg');
    this.el.css({
      width: width + 'px',
      height: height + 'px',
      top: top,
      left: left
    });
    if (this.map) this.map._onResize();
  },

  toggleRouteOverview: function () {
    this.inRouteOverview = !this.inRouteOverview;

    if (this.inRouteOverview) {
      this.setMapContainerSize($(window).width(), $(window).height(), 0, 0, 0);
    }
    return this.inRouteOverview;
  }

}, {
  defineLeafletExtensions: function () {

    // see https://github.com/CloudMade/Leaflet/issues/386
    L.Compass = L.Marker
      .extend({

        setIconAngle: function (iconAngle) {
          this.options.iconAngle = iconAngle;
          this.update();
        },

        _setPos: function (pos) {
          L.Marker.prototype._setPos.call(this, pos);

          var iconAngle = this.options.iconAngle;

          if (iconAngle) {
            this._icon.style[L.DomUtil.TRANSFORM] = L.DomUtil.getTranslateString(pos) + ' rotate(' + iconAngle + 'deg)';
          }
        }
      });
    L.LayerGroup.Collision = L.LayerGroup.extend({

      _originalLayers: [],
      _visibleLayers: [],
      _staticLayers: [],
      _rbush: [],
      _cachedRelativeBoxes: [],
      _margin: 0,

      initialize: function (options) {
        L.LayerGroup.prototype.initialize.call(this, options);
        this._margin = options.margin || 0;
      },

      addLayer: function (layer) {
        if (!'_icon' in layer) {
          this._staticLayers.push(layer);
          L.LayerGroup.prototype.addLayer.call(this, layer);
          return;
        }

        this._originalLayers.push(layer);
        if (this._map) {
          this._maybeAddLayerToRBush(layer);
        }
      },

      onAdd: function (map) {
        this._map = map;

        this._onZoomEnd();
        map.on('zoomend', this._onZoomEnd, this);
      },


      _maybeAddLayerToRBush: function (layer) {
        var z = this._map.getZoom();
        var bush = this._rbush;

        var boxes = this._cachedRelativeBoxes[layer._leaflet_id];
        var visible = false;
        if (!boxes) {
          // Add the layer to the map so it's instantiated on the DOM,
          //   in order to fetch its position and size.
          L.LayerGroup.prototype.addLayer.call(this, layer);
          var visible = true;
          // 			var htmlElement = layer._icon;
          var box = this._getIconBox(layer._icon);
          boxes = this._getRelativeBoxes(layer._icon.children, box);
          boxes.push(box);
          this._cachedRelativeBoxes[layer._leaflet_id] = boxes;
        }

        boxes = this._positionBoxes(this._map.latLngToLayerPoint(layer.getLatLng()), boxes);

        var collision = false;
        for (var i = 0; i < boxes.length && !collision; i++) {
          collision = bush.search(boxes[i]).length > 0;
        }

        if (!collision) {
          if (!visible) {
            L.LayerGroup.prototype.addLayer.call(this, layer);
          }
          this._visibleLayers.push(layer);
          bush.load(boxes);
        } else {
          L.LayerGroup.prototype.removeLayer.call(this, layer);
        }
      },


      // Returns a plain array with the relative dimensions of a L.Icon, based
      //   on the computed values from iconSize and iconAnchor.
      _getIconBox: function (el) {

        // 		if (isMSIE8) {
        // 			// Fallback for MSIE8, will most probably fail on edge cases
        // 			return [ 0, 0, el.offsetWidth, el.offsetHeight];
        // 		}

        var styles = window.getComputedStyle(el);

        // getComputedStyle() should return values already in pixels, so using arseInt()
        //   is not as much as a hack as it seems to be.

        return [
          parseInt(styles.marginLeft),
          parseInt(styles.marginTop),
          parseInt(styles.marginLeft) + parseInt(styles.width),
          parseInt(styles.marginTop) + parseInt(styles.height)
        ];
      },


      // Much like _getIconBox, but works for positioned HTML elements, based on offsetWidth/offsetHeight.
      _getRelativeBoxes: function (els, baseBox) {
        var boxes = [];
        for (var i = 0; i < els.length; i++) {
          var el = els[i];
          var box = [
            el.offsetLeft,
            el.offsetTop,
            el.offsetLeft + el.offsetWidth,
            el.offsetTop + el.offsetHeight
          ];
          box = this._offsetBoxes(box, baseBox);
          boxes.push(box);

          if (el.children.length) {
            var parentBox = baseBox;
            // 				if (!isMSIE8) {
            var positionStyle = window.getComputedStyle(el).position;
            if (positionStyle === 'absolute' || positionStyle === 'relative') {
              parentBox = box;
            }
            // 				}
            boxes = boxes.concat(this._getRelativeBoxes(el.children, parentBox));
          }
        }
        return boxes;
      },

      _offsetBoxes: function (a, b) {
        return [
          a[0] + b[0],
          a[1] + b[1],
          a[2] + b[0],
          a[3] + b[1]
        ];
      },

      // Adds the coordinate of the layer (in pixels / map canvas units) to each box coordinate.
      _positionBoxes: function (offset, boxes) {
        var newBoxes = []; // Must be careful to not overwrite references to the original ones.
        for (var i = 0; i < boxes.length; i++) {
          newBoxes.push(this._positionBox(offset, boxes[i]));
        }
        return newBoxes;
      },

      _positionBox: function (offset, box) {

        return [
          box[0] + offset.x - this._margin,
          box[1] + offset.y - this._margin,
          box[2] + offset.x + this._margin,
          box[3] + offset.y + this._margin,
        ]
      },

      _onZoomEnd: function () {

        for (var i = 0; i < this._visibleLayers.length; i++) {
          L.LayerGroup.prototype.removeLayer.call(this, this._visibleLayers[i]);
        }

        this._rbush = rbush();

        var z = this._map.getZoom();

        for (var i = 0; i < this._originalLayers.length; i++) {
          this._maybeAddLayerToRBush(this._originalLayers[i]);
        }

      }
    });


    // MSIE8 fails to use rbush properly (see https://github.com/mourner/rbush/issues/31),
    //   so work around that by making L.LayerGroup.Collision into a plain L.LayerGroup
    //   and crossing our fingers.
    // If MSIE8 support is ever to be done for this, then care has to be taken with
    //   calls to window.getComputedStyle().
    var isMSIE8 = 'ActiveXObject' in window && document.documentMode < 9;

    if (isMSIE8) {
      L.LayerGroup.Collision = L.LayerGroup;
    }


    L.LayerGroup.collision = function (options) {
      return new L.LayerGroup.Collision(options);
    };
  }
});

module.exports = Leaflet;