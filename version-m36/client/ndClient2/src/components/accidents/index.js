var BaseIcon = require('../base_icon');

var Accidents = BaseIcon.extend({

  icon: 'accidents/roadwork_ico.svg',

  defaultUnit: '',
  
  navigationOnRoute: function(e) {
    var accident = e.navInfo.currentDirection.situationInfo;
    if (accident) this.label(accident);
  }
});

module.exports = Accidents;
