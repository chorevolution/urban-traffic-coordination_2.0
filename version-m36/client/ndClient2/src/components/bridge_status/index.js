var BaseIcon = require('../base_icon');

var BridgeStatus = BaseIcon.extend({

  icon: 'bridge_status/bridge_ico3.svg',

  defaultUnit: '',
  
  navigationOnRoute: function(e) {
    var bridgeStatus = e.navInfo.currentDirection.bridgeStatus;
    if (bridgeStatus) this.label(bridgeStatus);
  }
});

module.exports = BridgeStatus;

