var BaseIcon = require('../base_icon');

var Congestion = BaseIcon.extend({

  icon: 'congestion/congestion3_ico.png',

  defaultUnit: '',
  
  navigationOnRoute: function(e) {
    var congestion = e.navInfo.currentDirection.congestionStatus;
    if (congestion) this.label(congestion);
  }
});

module.exports = Congestion;
