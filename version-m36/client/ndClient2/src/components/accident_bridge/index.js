var BaseIcon = require('../base_icon');

var AccidentBridge = BaseIcon.extend({

  icon: 'accident_bridge/ok_ico.svg',

  defaultUnit: '',
  
  navigationOnRoute: function(e) {
    this.label('');
    var congestion = e.navInfo.currentDirection.congestionStatus;
    var accident = e.navInfo.currentDirection.situationInfo;
    var bridgeStatus = e.navInfo.currentDirection.bridgeStatus;
/*     if (!bridgeStatus) //The priority order is: bridge, roadwork and congestion. Following the severity of the situation. 
    {
      this.icon = 'accident_bridge/bridge.svg';
    } else  */
    
    if (accident !== 'noSituationInfo') { //accident may be roadworks, EmergencyInfo, or other responses from trafikverket. No info found on dictionary
      this.icon = 'accident_bridge/roadwork_ico.svg';
      this.label(accident);
    } else if (congestion === 1) { //0: no congestion, 1: congested, i.e. the driver cannot drive faster than 75% of the desired speed
      this.icon = 'accident_bridge/congestion3_ico.png';
    } else this.icon = 'accident_bridge/ok_ico.svg';
    this.updateIcon(this.icon);
  }
});

module.exports = AccidentBridge;
