var Base = require('./base');
var InstructionParser = require('../utils/instruction-parser');

var GoogleDirections = Base.extend({
  /**

   * @class The class represents a client for the ffwdme routing service
   * @augments ffwdme.Class
   * @constructs
   *
   */
  constructor: function (options) {
    this.base(options);
    this.bindAll(this, 'parse', 'error');
    this.apiKey = CREDENTIALS.routingApiKey.GoogleDirections;
    
    if (options.anchorPoint) {
      this.anchorPoint = options.anchorPoint;
      this.direction = this.start;
      this.start = this.anchorPoint;
    }
  },

  /**
   * The base url for the service.
   *
   * @type String
   */
  BASE_URL: '//maps.googleapis.com/maps/api/directions/',

  // set via constructor

  modifier: 'fastest',

  routeType: 'DRIVING', //'car',

  lang: 'en_EN',

  route: null,

  anchorPoint: null,

  direction: null,

  fetch: function () {

    var via = '';
    this.apiKey = CREDENTIALS.routingApiKey.GoogleDirections;
    
    if (this.direction) {
      via += '&waypoints=' + [this.direction.lat, this.direction.lng].join('%2C');
    }

    var reqUrl = [
      this.BASE_URL,
      'json?',
      '&key=',
      this.apiKey,
      '&mode=',
      this.routeType,
      '&origin=', [
        this.start.lat,
        this.start.lng,
      ].join('%2C'),
      //via,
      '&destination=', [
        this.dest.lat,
        this.dest.lng
      ].join('%2C')
    ];

    ffwdme.trigger(this.eventPrefix() + ':start', {
      routing: this
    });

     $.ajax({
      url: reqUrl.join(''),
      type: 'GET',
      dataType: 'json',
      crossDomain: true,
      success: this.parse,
      error: this.error
    }); 
    return ffwdme;
  },

  error: function (error) {
    console.dir(error);
    this.base(error);
  },

  parse: function (response) {

    // check for error codes
    // https://github.com/graphhopper/graphhopper/blob/master/docs/web/api-doc.md
    // if (response.info.errors) return this.error(response);

    var route = response.routes[0];

    var routeStruct = {
      directions: []
    };
    routeStruct.summary = {
      distance: parseInt(route.legs[0].distance.value, 10),
      duration: route.legs[0].duration.value
    };

    var path = ffwdme.Route.decodePolyline(route.overview_polyline.points);

    var instruction, d, extractedStreet, geomArr;
    var instructions = route.legs[0].steps;

    // we remove the last instruction as it only says "Finish!" in
    // GraphHopper and has no value for us.
    //instructions.pop();

    for (var i = 0, len = instructions.length; i < len; i++) {
      instruction = instructions[i];
      d = {
        instruction: instruction.html_instructions,
        distance: parseInt(instruction.distance.value, 10),
        duration: instruction.duration.value,
        turnAngle: 0,
        turnType: InstructionParser.extractTurnType(instruction.html_instructions, 'google', instruction.maneuver),
        weatherInfo: {
          roadTemperature: -100,
          airTemperature: -100,
          airRelativeHumidity: -100,
          windForce: -100
        },
        ecoValue: -100,
        ecoSpeed: -100,
        bridgeStatus: false,
        congestionStatus: 1,
        situationInfo: 'noSituationInfo'
      };

      d.path = ffwdme.Route.decodePolyline(instruction.polyline.points);

      // Strip the streetname out of the route description
      extractedStreet = d.instruction;
      d.street = d.instruction; //extractedStreet.length == d.instruction.length ? '' : extractedStreet;

      routeStruct.directions.push(d);
    }

    this.route = new ffwdme.Route().parse(routeStruct);

    this.success(response, this.route);
  },

});

module.exports = GoogleDirections;