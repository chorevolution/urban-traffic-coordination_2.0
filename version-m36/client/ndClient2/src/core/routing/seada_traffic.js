var Base = require('./base');

var SEADA = Base.extend({
  /**

   *
   * @class The class represents a client for the ffwdme routing service
   *
   * @augments ffwdme.Class
   * @constructs
   *
   */
  constructor: function (options) {
    this.base(options);
    this.bindAll(this, 'parse', 'error');

    if (ffwdme.options.bridgeStatus) {
      this.avoidBridge = '&avoidareas=57.715454,11.965512;57.713829,11.967937';
    }
  },

  /**
   * The base url for the service.
   *
   * @type String
   */
  BASE_URL_Routing: '//route.cit.api.here.com/routing/7.2/',
  BASE_URL: '//jinx.viktoria.chalmers.se:3009/seadaAreaTraffic',

  // set via constructor
  //apiKey: 'AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw',//null,


  routeType: 'fastest;car;traffic:enabled', //'car',

  lang: 'en_EN',

  route: null,

  anchorPoint: null,

  direction: null,

  fetch: function () {

    var via = '';

    if (this.direction) {
      via += '&waypoints=' + [this.direction.lat, this.direction.lng].join('%2C');
    }

    var reqUrl = [
      this.BASE_URL_Routing,
      'calculateroute.json?',
      '&app_id=k0YXz4I407cw0jDmwdf9',
      '&app_code=MhWNbYvql9cQeswuIYUCfg',
      '&mode=',
      this.routeType,
      '&departure=now',
      '&returnelevation=true',
      '&representation=display',
      '&routeattributes=waypoints,summary,shape,legs',
      '&maneuverattributes=direction,action',
      '&instructionformat=text',
      this.avoidBridge,
      '&waypoint0=geo!', [
        this.start.lat,
        this.start.lng,
      ].join('%2C'),
      //via,
      '&waypoint1=geo!', [
        this.dest.lat,
        this.dest.lng
      ].join('%2C')
    ];

    ffwdme.trigger(this.eventPrefix() + ':start', {
      routing: this
    });

    $.ajax({
      url: reqUrl.join(''),
      dataType: 'json',
      success: this.parse,
      error: this.error
    });

    return ffwdme;
  },

  error: function (error) {
    this.base(error);
  },

  parse: function (response) {

    var route = response.response.route[0]; //Take the first route from the response

    var path = ffwdme.Route.decodeHEREShape(route.shape); //full path
    var routeStruct = {
      directions: []
    };
    var trafficRouteInformationRequest = {
      routes: [{
        id: 0,
        provider: "here",
        origin: {
          lat: route.waypoint[0].mappedPosition.latitude,
          lon: route.waypoint[0].mappedPosition.longitude
        },
        destination: {
          lat: route.waypoint[1].mappedPosition.latitude,
          lon: route.waypoint[1].mappedPosition.longitude
        },
        routePolyline: '',
        routeSegments:[]
      }]
    };
    var instruction, d, extractedStreet, geomArr;
    var instructions = route.leg[0].maneuver;
    var totalDistance = 0;
    var totalTime = 0;

    for (var i = 0, len = instructions.length; i < len; i++) {
      instruction = instructions[i];
      var spdMetersPerSec = Math.round(parseFloat(instruction.length) / parseFloat(instruction.travelTime));
      var ecoCal=Math.round((0.000065 * Math.pow(spdMetersPerSec, 2) + 0.533 / spdMetersPerSec + 0.09) * 1000.0) / 1000.0;
      d = {
        instruction: instruction.instruction,
        distance: parseInt(instruction.length, 10),
        duration: instruction.travelTime,
        turnAngle: 0,
        turnType: this.extractTurnType(instruction),
        weatherInfo: {
          roadTemperature: Math.random(),
          airTemperature: -100,
          airRelativeHumidity: -100,
          windForce: -100
        },
        ecoValue: ecoCal,
        ecoSpeed: -100,
        bridgeStatus: false,
        congestionStatus: 1,
        situationInfo: 'noSituationInfo'
      };
      d.path = ffwdme.Route.decodeHEREShape(instruction.shape);
      // Strip the streetname out of the route description
      extractedStreet = d.instruction.split('</span>. <span class=\"distance-description\">')[0].split(/(?:on |near |onto |at |Head )/).pop();
      d.street = extractedStreet.length == d.instruction.length ? '' : extractedStreet;
      routeStruct.directions.push(d);
      totalDistance += d.distance;
      totalTime += d.duration;
      // Now calculate the request for trafficInfo
      segItem = {
        waypoint0: {
          lat:d.path[0][0],
          lon:d.path[0][1]
        },
        waypoint1: {
          lat:d.path[d.path.length-1][0],
          lon:d.path[d.path.length-1][1]
        },
        instruction: instruction.instruction,
        distance: parseInt(instruction.length, 10),
        time: instruction.travelTime,
        polyline: ''
      };
      trafficRouteInformationRequest.routes[0].routeSegments.push(segItem);
    }
    
    routeStruct.summary = {
      distance: totalDistance,
      duration: totalTime
    };
    
    $.ajax({
      url: this.BASE_URL,
      type: "POST",
      dataType: 'json',
      data: '{ "trafficRouteInformationRequest": ' + JSON.stringify(trafficRouteInformationRequest) +"}",
      success: this.parseSeada,
      error: this.error
    });

    this.route = new ffwdme.Route().parse(routeStruct);
    this.success(response, this.route);
  },

  parseSeada: function(response){
    console.log(response);
  },

  // depart, 
  // arrive, 
  // leftUTurn,
  // sharpLeftTurn, 
  // leftTurn, 
  // continue, 
  // slightRightTurn, 
  // rightTurn,
  // sharpRightTurn, 
  // rightUTurn, 
  // ferry 
  // leftExit, 
  // rightExit, 
  // leftRamp, 
  // rightRamp, 
  // leftFork, 
  // middleFork, 
  // rightFork, 
  // leftMerge, 
  // rightMerge, 
  // nameChange, 
  // leftRoundaboutExit1, 
  // leftRoundaboutExit12, 
  // rightRoundaboutExit1, 
  // rightRoundaboutExit12, 

  extractTurnType: function (instruction) {
    var name;
    if (typeof instruction.action == 'undefined') {
      return 'C';
    }
    //name = 'C';
    switch (instruction.action) {
      case 'depart': //continue (go straight)
        name = 'DEPART';
        break;
      case 'continue': //continue (go straight)
        name = 'C';
        break;
      case 'leftTurn': // turn left
        name = 'TL';
        break;
      case 'slightLeftTurn': // turn slight left
        name = 'TSLL';
        break;
      case 'sharpLeftTurn': // turn sharp left
        name = 'TSHL';
        break;
      case 'rightTurn': // turn right
        name = 'TR';
        break;
      case 'slightRightTurn': // turn slight right
        name = 'TSLR';
        break;
      case 'sharpRightTurn': // turn sharp right
        name = 'TSHR';
        break;
      case 'leftFork': // turn left
        name = 'FL';
        break;
      case 'middleFork': // turn slight left
        name = 'FS';
        break;
      case 'rightFork': // turn sharp left
        name = 'FR';
        break;
      case 'leftRamp': // turn left
        name = 'RL';
        break;
      case 'rightRamp': // turn slight left
        name = 'RR';
        break;
      case 'leftExit': // turn left
        name = 'EL';
        break;
      case 'rightExit': // turn slight left
        name = 'ER';
        break;
      case 'leftMerge': // turn sharp left
        name = 'ML';
        break;
      case 'rightMerge': // turn sharp left
        name = 'MR';
        break;
      case 'leftRoundaboutExit1', 'rightRoundaboutExit1':
        name = 'EXIT1';
        break;
      case 'leftRoundaboutExit2', 'rightRoundaboutExit2':
        name = 'EXIT2';
        break;
      case 'leftRoundaboutExit3', 'rightRoundaboutExit3':
        name = 'EXIT3';
        break;
      case 'leftRoundaboutExit4', 'rightRoundaboutExit4':
        name = 'EXIT4';
        break;
      case 'leftRoundaboutExit5', 'rightRoundaboutExit5':
        name = 'EXIT5';
        break;
      case 'leftRoundaboutExit6', 'rightRoundaboutExit6':
        name = 'EXIT6';
        break;
      case 'righUTurn', 'leftUTurn': // U-turn
        name = 'TU';
        break;
    }
    return name;
  },

  // see https://github.com/graphhopper/graphhopper/blob/master/docs/web/api-doc.md
  extractTurnAngle: function (indication) {
    var angle;
    switch (indication) {
      case 0: //continue (go straight)
        angle = 0;
        break;
      case -2: // turn left
        angle = 90;
        break;
      case -1: // turn slight left
        angle = 45;
        break;
      case -3: // turn sharp left
        angle = 135;
        break;
      case 2: // turn right
        angle = -90;
        break;
      case 1: // turn slight right
        angle = -45;
        break;
      case 3: // turn sharp right
        angle = -135;
        break;
        // case 'TU': // U-turn
        //   angle = 180;
        //   break;
    }
    return angle;
  }
});

module.exports = SEADA;