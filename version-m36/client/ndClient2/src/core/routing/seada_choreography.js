var Base = require('./base');
var InstructionParser = require('../utils/instruction-parser');

var SEADAChoreography = Base.extend({
  /**

   *
   * @class The class represents a client for the ffwdme routing service
   *
   * @augments ffwdme.Class
   * @constructs
   *
   */
  constructor: function (options) {
    this.base(options);
    this.bindAll(this, 'parse', 'error');

    if (ffwdme.options.bridgeStatus) {
      this.avoidBridge = '&avoidareas=57.715454,11.965512;57.713829,11.967937';
    }
  },

  /**
   * The base url for the service.
   *
   * @type String
   */
  BASE_URL: '//localhost:9090/bcND/getEcoRoutes',

  // set via constructor
  //apiKey: 'AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw',//null,


  routeType: '', //'car',

  lang: 'en_EN',

  route: null,

  anchorPoint: null,

  direction: null,

  fetch: function () {

    ffwdme.trigger(this.eventPrefix() + ':start', {
      routing: this
    });
    //build ecoRoutesRequest
    var ecoRoutesRequest = {};
    ecoRoutesRequest.origin = {};
    ecoRoutesRequest.destination = {};
    ecoRoutesRequest.origin.lat = this.start.lat;
    ecoRoutesRequest.origin.lon = this.start.lng;
    ecoRoutesRequest.destination.lat = this.dest.lat;
    ecoRoutesRequest.destination.lon = this.dest.lng;

    //    this.avoidBridge,


    $.ajax({
      url: 'http://localhost:9090/bcND/getEcoRoutes',
      type: "POST",
      contentType: 'application/json',
      dataType: 'json',
      data: '{"ecoRoutesRequest": ' + JSON.stringify(ecoRoutesRequest) + "}",
      success: this.parse,
      error: this.error
    });
    // console.log('{ "ecoRoutesRequest": ' + JSON.stringify(ecoRoutesRequest) + "}");

    return ffwdme;
  },

  error: function (error) {
    this.base(error);
  },

  parse: function (response) {

    var routes = response.ecoRoutesResponseReturnType.messageData.routes; //Take the list of ecoRouteResponses
    var ecoRouteArray = [];
    var minEco = Number.MAX_VALUE;
    var selectedEcoRoute = {};
    for (var i = 0, len = routes.length; i < len; i++) {
      route = routes[i];
      var path = ffwdme.Route.decodePolyline(route.routePolyline); //full path
      var routeStruct = {
        directions: []
      };
      var instruction, d, extractedStreet;
      var routeSegmentsInfo = route.routeSegmentInfo;
      var totalDistance = 0;
      var totalTime = 0;
      var totalEco = 0;

      for (var i = 0, len = routeSegmentsInfo.length; i < len; i++) { //build a routeSegment inside a route
        routeSegmentInfo = routeSegmentsInfo[i];
        var spdMetersPerSec = Math.round(parseFloat(routeSegmentInfo.routeSegment.distance) / parseFloat(routeSegmentInfo.routeSegment.time));
        d = {
          instruction: routeSegmentInfo.routeSegment.instruction,
          distance: parseInt(routeSegmentInfo.routeSegment.distance, 10),
          duration: routeSegmentInfo.routeSegment.time,
          turnAngle: 0,
          weatherInfo: routeSegmentInfo.weatherInfo,
          ecoValue: Math.round(routeSegmentInfo.ecoValue *1000)/1000,
          ecoSpeed: routeSegmentInfo.speed,
          bridgeStatus: routeSegmentInfo.bridgeStatus,
          congestionStatus: routeSegmentInfo.congestion,
          situationInfo: routeSegmentInfo.situationInfo
        };
        d.path = ffwdme.Route.decodePolyline(routeSegmentInfo.routeSegment.polyline);
        d.instructionSign =  d.instruction.split('<div class="hiddentext" style="display: none;">')[1].split('</div>')[0];
        d.turnType = InstructionParser.extractTurnType(d.instruction, route.provider, d.instructionSign),
        extractedStreet = InstructionParser.extractStreetName(d.instruction.split('<div class="hiddentext" style="display: none;">')[0], route.provider);
        d.street = extractedStreet;
        routeStruct.directions.push(d);
        totalDistance += d.distance;
        totalTime += d.duration;
        totalEco += routeSegmentInfo.ecoValue * routeSegmentInfo.routeSegment.distance;
        if (d.bridgeStatus !== 2 && this.avoidBridge && route.provider !== 'here') {
          totalEco += 100000000;
        }
      };

      routeStruct.summary = { //Add summary
        distance: totalDistance,
        duration: totalTime,
        ecovalue: totalEco
      };
      ecoRouteArray.push(routeStruct);
      
      if (minEco > totalEco) { //select the most ecofriendly route
        selectedEcoRoute = routeStruct;
        minEco = totalEco;
      }
    };
    this.route = new ffwdme.Route().parse(selectedEcoRoute);
    this.success(response, this.route);
  },

  extractStreetName: function (instruction, provider) {
    var extractedStreet = '';
    switch (provider) {
      case 'here':
        extractedStreet = instruction.split('</span>. <span class=\"distance-description\">')[0].split(/(?:on |near |onto |at |Head )/).pop();
        break;
      case 'google':
        extractedStreet = instruction;
        break;
      case 'graphHopper':
        extractedStreet = instruction.split(/(?:on |near |onto |at |Head )/).pop();
        break;
    }
    return extractedStreet;
  },
});

module.exports = SEADAChoreography;