var Base = require('./base');
var InstructionParser = require('../utils/instruction-parser');

var HereDirections = Base.extend({
  /**

   *
   * @class The class represents a client for the ffwdme routing service
   *
   * @augments ffwdme.Class
   * @constructs
   *
   */
  constructor: function (options) {
    this.base(options);
    this.bindAll(this, 'parse', 'error');
    this.apiKey = CREDENTIALS.routingApiKey.HERE;
    this.avoidBridge = '';
    if (ffwdme.options.bridgeStatus) {
      this.avoidBridge = '&avoidareas=57.715454,11.965512;57.713829,11.967937';
    }

    if (options.anchorPoint) {
      this.anchorPoint = options.anchorPoint;
      this.direction = this.start;
      this.start = this.anchorPoint;
    }
  },

  /**
   * The base url for the service.
   *
   * @type String
   */
  BASE_URL: '//route.cit.api.here.com/routing/7.2/',

  // set via constructor
  //apiKey: 'AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw',//null,

  modifier: 'fastest;car;traffic:enabled',

  routeType: 'fastest;car;traffic:enabled', //'car',

  lang: 'en_EN',

  route: null,

  anchorPoint: null,

  direction: null,

  fetch: function () {
    this.apiKey = CREDENTIALS.routingApiKey.HERE;
    this.avoidBridge = '';
    if (ffwdme.options.bridgeStatus) {
      this.avoidBridge = '&avoidareas=57.715454,11.965512;57.713829,11.967937';
    }
    var via = '';

    var reqUrl = [
      this.BASE_URL,
      'calculateroute.json?',
      '&app_id=',
      this.apiKey.appId,
      '&app_code=',
      this.apiKey.appCode,
      '&mode=',
      this.routeType,
      '&departure=now',
      '&alternatives=3',
      '&returnelevation=true',
      '&representation=display',
      '&routeattributes=waypoints,summary,shape,legs',
      '&maneuverattributes=direction,action',
      '&instructionformat=text',
      this.avoidBridge,
      '&waypoint0=geo!', [
        this.start.lat,
        this.start.lng,
      ].join('%2C'),
      //via,
      '&waypoint1=geo!', [
        this.dest.lat,
        this.dest.lng
      ].join('%2C')
    ];
    /* ffwdme.trigger(this.eventPrefix() + ':start', {
      routing: this
    }); */

    $.ajax({
      url: reqUrl.join(''),
      dataType: 'json',
      success: this.parse,
      error: this.error
    });
   // console.log(reqUrl.join(''));
    return ffwdme;
  },

  error: function (error) {
    this.base(error);
  },
  
  parse: function (response) {
    var route = response.response.route[0];
    var path = ffwdme.Route.decodeHEREShape(route.shape); //full path
    var routeStruct = {
      directions: []
    };
    var instruction, d, extractedStreet, geomArr;
    var instructions = route.leg[0].maneuver;
    var totalDistance = 0;
    var totalTime = 0;
    // we remove the last instruction as it only says "Finish!" in
    // GraphHopper and has no value for us.
    //instructions.pop();

    for (var i = 0, len = instructions.length; i < len; i++) {
      instruction = instructions[i];
      d = {
        instruction: instruction.instruction,
        distance: parseInt(instruction.length, 10),
        duration: instruction.travelTime,
        turnAngle: 0,
        turnType: InstructionParser.extractTurnType(instruction, 'here', instruction.action),
        weatherInfo: {
          roadTemperature: -100,
          airTemperature: -100,
          airRelativeHumidity: -100,
          windForce: -100
        },
        ecoValue: -100,
        ecoSpeed: -100,
        bridgeStatus: false,
        congestionStatus: 1,
        situationInfo: 'noSituationInfo'
      };

      d.path = ffwdme.Route.decodeHEREShape(instruction.shape);

      // Strip the streetname out of the route description
      extractedStreet = d.instruction.split('</span>. <span class=\"distance-description\">')[0].split(/(?:on |near |onto |at |Head )/).pop();
      d.street = extractedStreet.length == d.instruction.length ? '' : extractedStreet;
      routeStruct.directions.push(d);
      totalDistance += d.distance;
      totalTime += d.duration;
    }

    routeStruct.summary = {
      distance: totalDistance,
      duration: totalTime
    };

    var thisRoute = new ffwdme.Route().parse(routeStruct);
    this.success(response, thisRoute);
  },
});

module.exports = HereDirections;