var Player = require('./player');
var Track = require('./track');

var PlayerRealtime = Player.extend({
  /**
   *  A geoprovider player that loads recorded tracks from the hard disk.
   *  It uses ajax and the ffwdme development server, so it will probably only work
   *  on the localhost.
   *
   *  @augments ffwdme.geoprovider.Player
   *  @constructs
   *
   *  @params {Integer} id
   *    The id of the track to load from the server.
   */
  constructor: function (options) {
    this.base(options);
    this.id = options.id;
    this.route = options.route;
    this.track = ffwdme.Route.parseRoute2Track(this.route);
  },

  /**
   *  The base url to load the files from.
   *  @type {String}
   */
  BASE_URL: "/recorded_routes/",

  /**
   *  The id of the track to load (which is actually the file name,
   *  which itself is actually a timestamp)
   *
   *  @type {String}
   */
  id: null,
  route: null,
  /**
   *  Loads the track from the server.
   */
  load: function () {
    var mtrack = new Track();
    var timeStamp = +new Date();
    var timeInc = 0;
    var startTime = 0;
    var trafficLightArray = require('../../../src/components/data/trafficLightsGoteborg.json');
    var prevPoint = {};
    this.route.directions.forEach(function (leg) {
      timeInc = parseFloat(leg.duration) * 1000 / leg.path.length;
      leg.path.forEach(function (wayPoint) {
        if (startTime === 0) {
          startTime = Math.round(timeStamp);
        };
        timeStamp += timeInc;
        var spdMetersPerSec = Math.round(parseFloat(leg.distance) / parseFloat(leg.duration)); // * 3.6) default speed unit used is m/s, will be converted into km/h in Speed widget
        var baseEcoRate = 0.000065 * Math.pow(spdMetersPerSec, 2) + 0.533 / spdMetersPerSec + 0.09;
    		var ecoThis = Math.round((baseEcoRate * (1+(24.22*Math.exp(leg.weatherInfo.airTemperature*-0.0269) - 13.21)/100 + leg.congestionStatus * 0.32)) * 1000.0) / 1000.0;
        var distMin = 1000;
        var hasTrafficLight = false;
        var trafficLightIndex = null;
        if (prevPoint) {
          for (var i=0; i<trafficLightArray.length; i++) {
            var dist = Math.hypot(wayPoint.lat-trafficLightArray[i].lat, wayPoint.lng-trafficLightArray[i].lon) +
            Math.hypot(prevPoint.lat-trafficLightArray[i].lat, prevPoint.lng-trafficLightArray[i].lon) -
            Math.hypot(wayPoint.lat-prevPoint.lat, wayPoint.lng-prevPoint.lng);
            if (dist < distMin) {
              distMin = dist;
              trafficLightIndex = i;
            } 
          };
          if (distMin< 0.00001) {
            hasTrafficLight = true;
            // console.log(trafficLightArray[trafficLightIndex]);
          }
        }
        var geoPos = {
          coords: {
            "latitude": wayPoint.lat,
            "longitude": wayPoint.lng,
            "altitude": 142,
            "accuracy": 5,
            "altitudeAccuracy": 0,
            "heading": 0,
            "speed": spdMetersPerSec,
            "ecoThis": ecoThis * 1000,
            "eco": leg.ecoValue * 1000, // to convert to g/km for displaying
            "trafficLight": hasTrafficLight? trafficLightArray[trafficLightIndex].id : 0
          },
          timestamp: Math.round(timeStamp),
          timestampRelative: Math.round(timeStamp) - startTime
        };
        //console.log("Player track: ", geoPos);
        mtrack.points.push(geoPos);
        prevPoint = wayPoint;
      });
    });
    this.track = mtrack;


    //console.log(this.route.navigation.route.directions[0]);
    /*          $.getJSON(url, function(data) {
              self.track = new Track();
              self.track.points = data.track.points;
            });  */

  }
});

module.exports = PlayerRealtime;