module.exports =
    /**
     * This is a container for helper functions that can be mixed into other objects
     */
    {
        extractStreetName: function (instruction, provider) {
            var extractedStreet = '';
            switch (provider) {
                case 'here':
                    extractedStreet = instruction.split('</span>. <span class=\"distance-description\">')[0].split(/(?:on |near |onto |at |Head )/).pop();
                    break;
                case 'google':
                    extractedStreet = instruction;
                    break;
                case 'graphHopper':
                    extractedStreet = instruction.split(/(?:on |near |onto |at |Head )/).pop();
                    break;
            }
            return extractedStreet;
        },
        extractTurnType: function (instruction, provider, instructionSign) {
            var name = 'C';

            switch (provider) {
                case 'here':
                    if (typeof instructionSign == 'undefined') {
                        return 'C';
                    };
                    switch (instructionSign) {
                        case 'depart': //continue (go straight)
                            name = 'DEPART';
                            break;
                        case 'continue': //continue (go straight)
                            name = 'C';
                            break;
                        case 'leftTurn': // turn left
                            name = 'TL';
                            break;
                        case 'slightLeftTurn': // turn slight left
                            name = 'TSLL';
                            break;
                        case 'sharpLeftTurn': // turn sharp left
                            name = 'TSHL';
                            break;
                        case 'rightTurn': // turn right
                            name = 'TR';
                            break;
                        case 'slightRightTurn': // turn slight right
                            name = 'TSLR';
                            break;
                        case 'sharpRightTurn': // turn sharp right
                            name = 'TSHR';
                            break;
                        case 'leftFork': // turn left
                            name = 'FL';
                            break;
                        case 'middleFork': // turn slight left
                            name = 'FS';
                            break;
                        case 'rightFork': // turn sharp left
                            name = 'FR';
                            break;
                        case 'leftRamp': // turn left
                            name = 'RL';
                            break;
                        case 'rightRamp': // turn slight left
                            name = 'RR';
                            break;
                        case 'leftExit': // turn left
                            name = 'EL';
                            break;
                        case 'rightExit': // turn slight left
                            name = 'ER';
                            break;
                        case 'leftMerge': // turn sharp left
                            name = 'ML';
                            break;
                        case 'rightMerge': // turn sharp left
                            name = 'MR';
                            break;
                        case 'leftRoundaboutExit1', 'rightRoundaboutExit1':
                            name = 'EXIT1';
                            break;
                        case 'leftRoundaboutExit2', 'rightRoundaboutExit2':
                            name = 'EXIT2';
                            break;
                        case 'leftRoundaboutExit3', 'rightRoundaboutExit3':
                            name = 'EXIT3';
                            break;
                        case 'leftRoundaboutExit4', 'rightRoundaboutExit4':
                            name = 'EXIT4';
                            break;
                        case 'leftRoundaboutExit5', 'rightRoundaboutExit5':
                            name = 'EXIT5';
                            break;
                        case 'leftRoundaboutExit6', 'rightRoundaboutExit6':
                            name = 'EXIT6';
                            break;
                        case 'righUTurn', 'leftUTurn': // U-turn
                            name = 'TU';
                            break;
                    };
                    break;
                case 'google':
                    if (typeof instructionSign == 'undefined') {
                        return 'C';
                    };
                    switch (instructionSign) {
                        case 'straight': //continue (go straight)
                            name = 'C';
                            break;
                        case 'turn-left': // turn left
                            name = 'TL';
                            break;
                        case 'turn-slight-left': // turn slight left
                            name = 'TSLL';
                            break;
                        case 'turn-sharp-left': // turn sharp left
                            name = 'TSHL';
                            break;
                        case 'turn-right': // turn right
                            name = 'TR';
                            break;
                        case 'turn-slight-right': // turn slight right
                            name = 'TSLR';
                            break;
                        case 'turn-sharp-right': // turn sharp right
                            name = 'TSHR';
                            break;
                        case 'roundabout-right':
                            var re = /(.*take\s+)(.*)(\s+exit.*)/;
                            var roundAboutExitStr = instruction.replace(re, "$2");
                            var exitNo = roundAboutExitStr.substring(7, 8);
                            name = 'EXIT' + exitNo;
                            break;
                        case 'fork-right':
                            name = 'FR';
                            break;
                        case 'fork-left':
                            name = 'FL';
                            break;
                        case 'ramp-right':
                            name = 'ER';
                            break;
                        case 'ramp-left':
                            name = 'EL';
                            break;
                        case 'merge':
                            name = 'MS';
                            break;
                        case 'uturn-left', 'uturn-right': // U-turn
                            name = 'TU';
                            break;
                    };
                    break;
                case 'graphHopper':
                    if (typeof instructionSign == 'undefined') {
                        return 'C';
                    };
                    switch (parseInt(instructionSign)) {
                        case 0: //continue (go straight)
                            name = 'C';
                            break;
                        case -2: // turn left
                            name = 'TL';
                            break;
                        case -1: // turn slight left
                            name = 'TSLL';
                            break;
                        case -3: // turn sharp left
                            name = 'TSHL';
                            break;
                        case 2: // turn right
                            name = 'TR';
                            break;
                        case 1: // turn slight right
                            name = 'TSLR';
                            break;
                        case 3: // turn sharp right
                            name = 'TSHR';
                            break;
                        case 6:
                            name = 'EXIT' + instruction.exit_number;
                            break;
                    }
                    break;
            }
            return name;
        },

        extractTurnAngle: function (indication) {
            var angle;
            switch (indication) {
                case 0: //continue (go straight)
                    angle = 0;
                    break;
                case -2: // turn left
                    angle = 90;
                    break;
                case -1: // turn slight left
                    angle = 45;
                    break;
                case -3: // turn sharp left
                    angle = 135;
                    break;
                case 2: // turn right
                    angle = -90;
                    break;
                case 1: // turn slight right
                    angle = -45;
                    break;
                case 3: // turn sharp right
                    angle = -135;
                    break;
            }
            return angle;
        }

    };