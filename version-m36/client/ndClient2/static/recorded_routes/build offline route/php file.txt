        <?php
        require_once 'PolylineEncoder.php';
$polylineEncoder = new PolylineEncoder();
////
////        $polylineEncoder->addPoint(30.5, -120.2);
////        $polylineEncoder->addPoint(40.7, -120.95);
////        $polylineEncoder->addPoint(43.252, -126.453);
////
////        $encodedString = $polylineEncoder->encodedString();
////        Decode a polyline
// Paris:  mnbiHstpMp@`@RLJAFInBgCdDuCDO?SKy@WcAQg@d@g@vFuFXGgDn]aBnPa@hDDXCFARFPDBB?@VHZnEzJpArCGZGTY^sA|A_BjBu@`AI^}GbIkD|EeCxD_DtEi@x@URc@JKCEGgDdFSV@DAFOn@@f@BJj@tA|@hBj@pAr@`Bj@nANPHLFJLFUJY\\Yp@[f@q@~@gGdK_IzMoErHk@fAUt@Kd@YbAs@nD}@bEuArGmCzLYzAa@tAeAjDeAzCk@hBuClHiApGy@nEaBaBkCgC
//        $decodedPoints = PolylineEncoder::decodeValue("a_f_Jkd{gAsE|DW`@_CcFiByD_BaDQe@kAeCOKGA?C?GAMGWOQMAKFIJGRAj@DROTi@rAEPUOi@qAk@}@KSOE_@Eq@?_@MQMQa@k@gBk@cB{@uBcBuD[[aKcTwF_MqBmEc@w@EQGWm@mAe@u@}AaBgBsBGO_@w@IYKq@GiBAuCIo@GaDCaCBkBXgETiBXwARkAh@wAp@oAh@e@~@g@vCqAxAk@v@]`CaA|As@^Yj@q@nAoBdAmBnCoEbC_EfC{Dz@{@TMn@W`@KZEf@AT@R?XFfB`@HBTRHNNb@d@~CFrA?n@G`@IFK@OIGSi@mBGeA@g@Jc@FOJKDEFGPOnAmAnCkCBXZdD");
$decodedPoints = PolylineEncoder::decodeValue("mnbiHstpMp@`@RLJAFInBgCdDuCDO?SKy@WcAQg@d@g@vFuFXGgDn]aBnPa@hDDXCFARFPDBB?@VHZnEzJpArCGZGTY^sA|A_BjBu@`AI^}GbIkD|EeCxD_DtEi@x@URc@JKCEGgDdFSV@DAFOn@@f@BJj@tA|@hBj@pAr@`Bj@nANPHLFJLFUJY\\Yp@[f@q@~@gGdK_IzMoErHk@fAUt@Kd@YbAs@nD}@bEuArGmCzLYzAa@tAeAjDeAzCk@hBuClHiApGy@nEaBaBkCgC");
//        //        
$timeStamp = time() * 1000; //1509089927000 Epoch time in millisecond
       $prevLat = -1;
       $prevLon = -1;
       $timeStampRelativeAccu = 0;
       foreach ($decodedPoints as $pnt) {
           $speed = 70; //km/h
           if ($prevLat == -1) {
               $timeStampRelative = 0;
           } else {
               $timeStampRelative = round(distance($prevLat, $prevLon, $pnt['x'], $pnt['y'], 'K')/$speed*3600000);
           }
           $prevLat = $pnt['x'];
           $prevLon = $pnt['y'];
           //$timeStampRelative = rand(10, 2000);
           $timeStamp += $timeStampRelative;
           $timeStampRelativeAccu += $timeStampRelative;
           $arr[] = array(
               'coords' => array(
                   'latitude' => $pnt['x'],
                   'longitude' => $pnt['y'],
                   'altitude' => 100,
                   'accuracy' => 0,
                   'altitudeAccuracy' => 0,
                   'heading' => null,
                   'speed' => 70
               ),
               'timestamp' => $timeStamp,
               'timestampRelative' => $timeStampRelativeAccu
           );
       }
//       print_r(json_encode($arr));