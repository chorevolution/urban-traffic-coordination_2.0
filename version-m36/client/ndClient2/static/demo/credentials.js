var CREDENTIALS = {
    /*
     * Access Token for the Mapbox API.
     * Sign up for a free starter account to get one and find it at
     * https://www.mapbox.com/account/apps/
     */
    mapboxToken: 'pk.eyJ1IjoidGhhbmhiaCIsImEiOiJjajk2ejgyeTIwOXdhMnFxc3hkbnVoeng0In0.URBsBeLyw-Wau18Qz3Uq5Q',
    /*
     * Id of the map style - you should not need to change this
     */
    mapboxId: 'instantnavi.kggpbnd3',

    /*
     * ffwdme.js uses the open source routing service GraphHopper (https://graphhopper.com/#directions-api)
     * for the route calculation.
     * They are pretty awesome and provide us with a free demo API key to try things out.
     * However, please be fair. Once your project gets more serious you should get in touch at
     * https://graphhopper.com/#directions-api and obtain your own API key.
     */
    routingApiKey: {
        graphHopper: 'c3b658e3-2e20-470f-9b96-9683e7e55795',//'c62916b3-35a1-4bac-bd03-ab564755ec0d'
        googleDirections: 'AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw',
        HERE: {
            appId: 'k0YXz4I407cw0jDmwdf9',
            appCode: 'MhWNbYvql9cQeswuIYUCfg'
        }
    }
};
