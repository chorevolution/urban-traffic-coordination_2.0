package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentWeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers.SegmentInformationRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers.SegmentWeatherInformationResponseWrapper;

@Service
public class RESTDTSWeatherServiceImpl implements DTSWeatherService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSWeatherServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;

	@Override
	public SegmentWeatherInformationResponse segmentWeatherInformation(SegmentInformationRequest parameters) {
		
		logger.info("CALLED segmentWeatherInformation ON REST bcDTS-WEATHER");
		SegmentWeatherInformationResponse segmentWeatherInformationResponse = null;
		try {
			Gson gson = new GsonBuilder().create();			
			HttpClient httpClient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();			
			HttpPost postRequest = new HttpPost(uri);
			SegmentInformationRequestWrapper segmentInformationRequestWrapper = new SegmentInformationRequestWrapper();
			segmentInformationRequestWrapper.setSegmentInformationRequest(parameters);
			StringEntity requestEntity = new StringEntity(gson.toJson(segmentInformationRequestWrapper), ContentType.APPLICATION_JSON);
			postRequest.setEntity(requestEntity);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String jsonResponse = EntityUtils.toString(httpResponse.getEntity());
			SegmentWeatherInformationResponseWrapper segmentWeatherInformationResponseWrapper = gson.fromJson(jsonResponse, SegmentWeatherInformationResponseWrapper.class);
			segmentWeatherInformationResponse = segmentWeatherInformationResponseWrapper.getSegmentWeatherInformationResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}	
		return segmentWeatherInformationResponse;
	}
}
