package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentWeatherInformationResponse;

public interface DTSWeatherService {
	SegmentWeatherInformationResponse segmentWeatherInformation(SegmentInformationRequest parameters);
}
