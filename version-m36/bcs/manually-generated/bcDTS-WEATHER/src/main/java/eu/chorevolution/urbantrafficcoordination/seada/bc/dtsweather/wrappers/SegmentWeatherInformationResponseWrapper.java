package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentWeatherInformationResponse;

public class SegmentWeatherInformationResponseWrapper {
	
	private SegmentWeatherInformationResponse segmentWeatherInformationResponse;

	public SegmentWeatherInformationResponse getSegmentWeatherInformationResponse() {
		return segmentWeatherInformationResponse;
	}

	public void setSegmentWeatherInformationResponse(SegmentWeatherInformationResponse segmentWeatherInformationResponse) {
		this.segmentWeatherInformationResponse = segmentWeatherInformationResponse;
	}
	
}
