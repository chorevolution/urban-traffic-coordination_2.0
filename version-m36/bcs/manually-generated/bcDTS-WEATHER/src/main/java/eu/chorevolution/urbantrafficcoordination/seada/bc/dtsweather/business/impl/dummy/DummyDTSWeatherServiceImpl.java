package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentWeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;

@Service
public class DummyDTSWeatherServiceImpl implements DTSWeatherService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSWeatherServiceImpl.class);	
	
	@Override
	public SegmentWeatherInformationResponse segmentWeatherInformation(SegmentInformationRequest parameters) {

		logger.info("CALLED segmentWeatherInformation ON DUMMY bcDTS-WEATHER");	
		return new SegmentWeatherInformationResponse();
	}
}
