package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.DTSWEATHERPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentWeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;

@Component(value="DTSWEATHERPTImpl")
public class DTSWEATHERPTImpl implements DTSWEATHERPT{

	private static Logger logger = LoggerFactory.getLogger(DTSWEATHERPTImpl.class);
	
	@Autowired
	DTSWeatherService service;

	@Override
	public SegmentWeatherInformationResponse segmentWeatherInformation(SegmentInformationRequest parameters) {

		logger.info("CALLED waypointWeatherInformation ON bcDTS-WEATHER");
		SegmentWeatherInformationResponse weatherInformationResponse = service.segmentWeatherInformation(parameters);
		return weatherInformationResponse;
	}
	
}
