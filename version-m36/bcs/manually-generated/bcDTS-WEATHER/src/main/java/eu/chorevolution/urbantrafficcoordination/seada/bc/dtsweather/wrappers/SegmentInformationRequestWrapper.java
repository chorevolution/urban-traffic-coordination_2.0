package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.SegmentInformationRequest;

public class SegmentInformationRequestWrapper {
	
	private SegmentInformationRequest segmentInformationRequest;

	public SegmentInformationRequest getSegmentInformationRequest() {
		return segmentInformationRequest;
	}

	public void setSegmentInformationRequest(SegmentInformationRequest segmentInformationRequest) {
		this.segmentInformationRequest = segmentInformationRequest;
	}

}
