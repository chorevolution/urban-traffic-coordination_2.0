
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="origin" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}waypoint"/&gt;
 *         &lt;element name="destination" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}waypoint"/&gt;
 *         &lt;element name="routePolyline" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;sequence maxOccurs="unbounded"&gt;
 *           &lt;element name="routeSegments" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegment"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeData", propOrder = {
    "id",
    "provider",
    "origin",
    "destination",
    "routePolyline",
    "routeSegments"
})
public class RouteData {

    protected int id;
    @XmlElement(required = true)
    protected String provider;
    @XmlElement(required = true)
    protected Waypoint origin;
    @XmlElement(required = true)
    protected Waypoint destination;
    @XmlElement(required = true)
    protected String routePolyline;
    @XmlElement(required = true)
    protected List<RouteSegment> routeSegments;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the provider property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Sets the value of the provider property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvider(String value) {
        this.provider = value;
    }

    /**
     * Gets the value of the origin property.
     * 
     * @return
     *     possible object is
     *     {@link Waypoint }
     *     
     */
    public Waypoint getOrigin() {
        return origin;
    }

    /**
     * Sets the value of the origin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Waypoint }
     *     
     */
    public void setOrigin(Waypoint value) {
        this.origin = value;
    }

    /**
     * Gets the value of the destination property.
     * 
     * @return
     *     possible object is
     *     {@link Waypoint }
     *     
     */
    public Waypoint getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     * 
     * @param value
     *     allowed object is
     *     {@link Waypoint }
     *     
     */
    public void setDestination(Waypoint value) {
        this.destination = value;
    }

    /**
     * Gets the value of the routePolyline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoutePolyline() {
        return routePolyline;
    }

    /**
     * Sets the value of the routePolyline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoutePolyline(String value) {
        this.routePolyline = value;
    }

    /**
     * Gets the value of the routeSegments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeSegments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteSegments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteSegment }
     * 
     * 
     */
    public List<RouteSegment> getRouteSegments() {
        if (routeSegments == null) {
            routeSegments = new ArrayList<RouteSegment>();
        }
        return this.routeSegments;
    }

}
