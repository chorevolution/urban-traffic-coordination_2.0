package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.model.ArtifactEndpointData;


public class SetInvocationAddressUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(SetInvocationAddressUtils.class);

	// role, ArtifactAddressData
	private static Map<String, ArtifactEndpointData> artifactsEndpointsData = new HashMap<>();
	
	public static void storeArtifactEndpointData(String role, String name, List<String> endpoints){
		
		artifactsEndpointsData.put(role, new ArtifactEndpointData(name, role, endpoints));
		LOGGER.info("BC bcND set invocation address for artifact " + name + " with role " + role + " to " + endpoints.get(0));
	}
	
	public static String getArtifactEndpointAddressFromRole(String role) {

		if (artifactsEndpointsData.containsKey(role)) {
			String address = artifactsEndpointsData.get(role).getEndpoints().get(0);
			LOGGER.info("BC bcND found invocation address to call artifact with role " + role + ": " + address);
			return address.endsWith("?wsdl") ? address : address + "?wsdl";
		} else {
			LOGGER.info("BC bcND has NOT found invocation address to call artifact with role " + role);
			return null;
		}
	}
	
	public static URL getArtifactEndpointURLFromRole(String role){
		
		if(artifactsEndpointsData.containsKey(role))
			try {
				String address = artifactsEndpointsData.get(role).getEndpoints().get(0);
				LOGGER.info("BC bcND found invocation address to call artifact with role " + role + ": " + address);
				return new URL(address.endsWith("?wsdl") ? address : address + "?wsdl");
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		else {
			LOGGER.info("BC bcND has NOT found invocation address to call artifact with role " + role);
			return null;
		}
	}
}
