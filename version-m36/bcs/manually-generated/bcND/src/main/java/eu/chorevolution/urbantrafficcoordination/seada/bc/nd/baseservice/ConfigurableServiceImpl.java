package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.baseservice;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.util.SetInvocationAddressUtils;

@Component(value = "ConfigurableServiceImpl")
public class ConfigurableServiceImpl implements ConfigurableService{

	private static Logger logger = LoggerFactory.getLogger(ConfigurableServiceImpl.class);
	
	@Override
	public void setInvocationAddress(String arg0, String arg1, List<String> arg2) {
		
		logger.info("CALLED setInvocationAddress ON bcND");
		SetInvocationAddressUtils.storeArtifactEndpointData(arg0, arg1, arg2);
	}

}
