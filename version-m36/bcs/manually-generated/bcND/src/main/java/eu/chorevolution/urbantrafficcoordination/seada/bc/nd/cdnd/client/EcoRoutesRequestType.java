
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoRoutesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoRoutesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="messageData" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}ecoRoutesRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoRoutesRequestType", propOrder = {
    "messageData"
})
public class EcoRoutesRequestType {

    @XmlElement(required = true)
    protected EcoRoutesRequest messageData;

    /**
     * Gets the value of the messageData property.
     * 
     * @return
     *     possible object is
     *     {@link EcoRoutesRequest }
     *     
     */
    public EcoRoutesRequest getMessageData() {
        return messageData;
    }

    /**
     * Sets the value of the messageData property.
     * 
     * @param value
     *     allowed object is
     *     {@link EcoRoutesRequest }
     *     
     */
    public void setMessageData(EcoRoutesRequest value) {
        this.messageData = value;
    }

}
