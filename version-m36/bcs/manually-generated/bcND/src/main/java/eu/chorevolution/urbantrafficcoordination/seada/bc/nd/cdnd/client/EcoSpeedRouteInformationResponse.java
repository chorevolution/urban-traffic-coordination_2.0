
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ecoSpeedRouteInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ecoSpeedRouteInformationResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded"&gt;
 *         &lt;element name="routeSegmentInfo" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegmentInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ecoSpeedRouteInformationResponse", propOrder = {
    "routeSegmentInfo"
})
public class EcoSpeedRouteInformationResponse {

    @XmlElement(required = true)
    protected List<RouteSegmentInfo> routeSegmentInfo;

    /**
     * Gets the value of the routeSegmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeSegmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteSegmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteSegmentInfo }
     * 
     * 
     */
    public List<RouteSegmentInfo> getRouteSegmentInfo() {
        if (routeSegmentInfo == null) {
            routeSegmentInfo = new ArrayList<RouteSegmentInfo>();
        }
        return this.routeSegmentInfo;
    }

}
