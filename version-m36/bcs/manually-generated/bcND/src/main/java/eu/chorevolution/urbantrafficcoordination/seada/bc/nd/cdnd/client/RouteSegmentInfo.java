
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeSegmentInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeSegmentInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="routeSegment" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegment"/&gt;
 *         &lt;element name="bridgeStatus" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="weatherInfo" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}weatherCondition"/&gt;
 *         &lt;element name="situationInfo" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}situationInfo"/&gt;
 *         &lt;element name="ecoValue" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="congestion" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="speed" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="updatedAt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeSegmentInfo", propOrder = {
    "id",
    "routeSegment",
    "bridgeStatus",
    "weatherInfo",
    "situationInfo",
    "ecoValue",
    "congestion",
    "speed",
    "updatedAt",
    "type"
})
public class RouteSegmentInfo {

    protected int id;
    @XmlElement(required = true)
    protected RouteSegment routeSegment;
    protected int bridgeStatus;
    @XmlElement(required = true)
    protected WeatherCondition weatherInfo;
    @XmlElement(required = true)
    protected SituationInfo situationInfo;
    protected double ecoValue;
    protected double congestion;
    protected double speed;
    @XmlElement(required = true)
    protected String updatedAt;
    @XmlElement(required = true)
    protected String type;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the routeSegment property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegment }
     *     
     */
    public RouteSegment getRouteSegment() {
        return routeSegment;
    }

    /**
     * Sets the value of the routeSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegment }
     *     
     */
    public void setRouteSegment(RouteSegment value) {
        this.routeSegment = value;
    }

    /**
     * Gets the value of the bridgeStatus property.
     * 
     */
    public int getBridgeStatus() {
        return bridgeStatus;
    }

    /**
     * Sets the value of the bridgeStatus property.
     * 
     */
    public void setBridgeStatus(int value) {
        this.bridgeStatus = value;
    }

    /**
     * Gets the value of the weatherInfo property.
     * 
     * @return
     *     possible object is
     *     {@link WeatherCondition }
     *     
     */
    public WeatherCondition getWeatherInfo() {
        return weatherInfo;
    }

    /**
     * Sets the value of the weatherInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeatherCondition }
     *     
     */
    public void setWeatherInfo(WeatherCondition value) {
        this.weatherInfo = value;
    }

    /**
     * Gets the value of the situationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SituationInfo }
     *     
     */
    public SituationInfo getSituationInfo() {
        return situationInfo;
    }

    /**
     * Sets the value of the situationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SituationInfo }
     *     
     */
    public void setSituationInfo(SituationInfo value) {
        this.situationInfo = value;
    }

    /**
     * Gets the value of the ecoValue property.
     * 
     */
    public double getEcoValue() {
        return ecoValue;
    }

    /**
     * Sets the value of the ecoValue property.
     * 
     */
    public void setEcoValue(double value) {
        this.ecoValue = value;
    }

    /**
     * Gets the value of the congestion property.
     * 
     */
    public double getCongestion() {
        return congestion;
    }

    /**
     * Sets the value of the congestion property.
     * 
     */
    public void setCongestion(double value) {
        this.congestion = value;
    }

    /**
     * Gets the value of the speed property.
     * 
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     */
    public void setSpeed(double value) {
        this.speed = value;
    }

    /**
     * Gets the value of the updatedAt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Sets the value of the updatedAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdatedAt(String value) {
        this.updatedAt = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
