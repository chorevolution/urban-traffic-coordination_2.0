
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for situationInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="situationInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;sequence maxOccurs="unbounded"&gt;
 *           &lt;element name="accidents" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}accidentInfo"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element name="situationSummaryInfo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "situationInfo", propOrder = {
    "accidents",
    "situationSummaryInfo",
    "type"
})
public class SituationInfo {

    @XmlElement(required = true)
    protected List<AccidentInfo> accidents;
    @XmlElement(required = true)
    protected String situationSummaryInfo;
    @XmlElement(required = true)
    protected String type;

    /**
     * Gets the value of the accidents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accidents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccidents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccidentInfo }
     * 
     * 
     */
    public List<AccidentInfo> getAccidents() {
        if (accidents == null) {
            accidents = new ArrayList<AccidentInfo>();
        }
        return this.accidents;
    }

    /**
     * Gets the value of the situationSummaryInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSituationSummaryInfo() {
        return situationSummaryInfo;
    }

    /**
     * Sets the value of the situationSummaryInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSituationSummaryInfo(String value) {
        this.situationSummaryInfo = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
