
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for trafficRouteInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="trafficRouteInformationResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="trafficRouteInfo" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trafficRouteInformationResponse", propOrder = {
    "trafficRouteInfo"
})
public class TrafficRouteInformationResponse {

    @XmlElement(required = true)
    protected RouteInfo trafficRouteInfo;

    /**
     * Gets the value of the trafficRouteInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RouteInfo }
     *     
     */
    public RouteInfo getTrafficRouteInfo() {
        return trafficRouteInfo;
    }

    /**
     * Sets the value of the trafficRouteInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteInfo }
     *     
     */
    public void setTrafficRouteInfo(RouteInfo value) {
        this.trafficRouteInfo = value;
    }

}
