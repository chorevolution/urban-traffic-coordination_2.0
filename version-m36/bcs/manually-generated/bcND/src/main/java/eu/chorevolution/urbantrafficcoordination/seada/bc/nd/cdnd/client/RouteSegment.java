
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeSegment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeSegment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="waypoint0" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}waypoint"/&gt;
 *         &lt;element name="waypoint1" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}waypoint"/&gt;
 *         &lt;element name="instruction" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="distance" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="polyline" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeSegment", propOrder = {
    "waypoint0",
    "waypoint1",
    "instruction",
    "time",
    "distance",
    "polyline"
})
public class RouteSegment {

    @XmlElement(required = true)
    protected Waypoint waypoint0;
    @XmlElement(required = true)
    protected Waypoint waypoint1;
    @XmlElement(required = true)
    protected String instruction;
    protected int time;
    protected int distance;
    @XmlElement(required = true)
    protected String polyline;

    /**
     * Gets the value of the waypoint0 property.
     * 
     * @return
     *     possible object is
     *     {@link Waypoint }
     *     
     */
    public Waypoint getWaypoint0() {
        return waypoint0;
    }

    /**
     * Sets the value of the waypoint0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Waypoint }
     *     
     */
    public void setWaypoint0(Waypoint value) {
        this.waypoint0 = value;
    }

    /**
     * Gets the value of the waypoint1 property.
     * 
     * @return
     *     possible object is
     *     {@link Waypoint }
     *     
     */
    public Waypoint getWaypoint1() {
        return waypoint1;
    }

    /**
     * Sets the value of the waypoint1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Waypoint }
     *     
     */
    public void setWaypoint1(Waypoint value) {
        this.waypoint1 = value;
    }

    /**
     * Gets the value of the instruction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstruction() {
        return instruction;
    }

    /**
     * Sets the value of the instruction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstruction(String value) {
        this.instruction = value;
    }

    /**
     * Gets the value of the time property.
     * 
     */
    public int getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     */
    public void setTime(int value) {
        this.time = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     */
    public void setDistance(int value) {
        this.distance = value;
    }

    /**
     * Gets the value of the polyline property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolyline() {
        return polyline;
    }

    /**
     * Sets the value of the polyline property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolyline(String value) {
        this.polyline = value;
    }

}
