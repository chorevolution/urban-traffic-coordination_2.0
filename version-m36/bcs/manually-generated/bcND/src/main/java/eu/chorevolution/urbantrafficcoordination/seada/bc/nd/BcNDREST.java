package eu.chorevolution.urbantrafficcoordination.seada.bc.nd;

import java.net.URL;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationResponseReturnType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.NDPortType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.ND;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.util.SetInvocationAddressUtils;

public class BcNDREST {

	private static Logger logger = LoggerFactory.getLogger(BcNDREST.class);
		
	@Path("/getEcoRoutes")
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoRoutesResponseReturnType getEcoRoutes(EcoRoutesRequest ecoRoutesRequest){
	
		logger.info("CALLED getEcoRoutes ON BC-ND-REST");	
		
		URL WSDL_URL = SetInvocationAddressUtils.getArtifactEndpointURLFromRole("ND");
		ND cdNDService = null;
		if (WSDL_URL == null) {
			cdNDService = new ND();
		} else {
			cdNDService = new ND(WSDL_URL);
		}
		NDPortType cdNDPT = cdNDService.getNDPort();
		/*
		Client client = ClientProxy.getClient(cdNDPT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);		
		*/
		// create ecoRoutesRequestType
		EcoRoutesRequestType ecoRoutesRequestType = new EcoRoutesRequestType();
		ecoRoutesRequestType.setMessageData(ecoRoutesRequest);
		// call getEcoRoutes on cdND
		return cdNDPT.getEcoRoutes(ecoRoutesRequestType);
	}

	@Path("/getEcoSpeedRouteInformation")	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoSpeedRouteInformationResponseReturnType getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType){
		
		logger.info("CALLED getEcoSpeedRouteInformation ON BC-ND-REST");
		
		URL WSDL_URL = SetInvocationAddressUtils.getArtifactEndpointURLFromRole("ND");
		ND cdNDService = null;
		if (WSDL_URL == null) {
			cdNDService = new ND();
		} else {
			cdNDService = new ND(WSDL_URL);
		}
		NDPortType cdNDPT = cdNDService.getNDPort();
		/*
		Client client = ClientProxy.getClient(cdNDPT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);	
		*/
		return cdNDPT.getEcoSpeedRouteInformation(ecoSpeedRouteInformationRequestType);
	}
	
}
