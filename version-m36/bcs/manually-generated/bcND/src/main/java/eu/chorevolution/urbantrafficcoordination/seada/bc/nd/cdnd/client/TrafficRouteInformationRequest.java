
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for trafficRouteInformationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="trafficRouteInformationRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="trafficRoute" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeData"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trafficRouteInformationRequest", propOrder = {
    "trafficRoute"
})
public class TrafficRouteInformationRequest {

    @XmlElement(required = true)
    protected RouteData trafficRoute;

    /**
     * Gets the value of the trafficRoute property.
     * 
     * @return
     *     possible object is
     *     {@link RouteData }
     *     
     */
    public RouteData getTrafficRoute() {
        return trafficRoute;
    }

    /**
     * Sets the value of the trafficRoute property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteData }
     *     
     */
    public void setTrafficRoute(RouteData value) {
        this.trafficRoute = value;
    }

}
