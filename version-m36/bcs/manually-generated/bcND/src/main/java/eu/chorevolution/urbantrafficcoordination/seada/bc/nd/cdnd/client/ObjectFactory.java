
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EcoRoutesRequestElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoRoutesRequestElementRequest");
    private final static QName _EcoRoutesResponseElementResponse_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoRoutesResponseElementResponse");
    private final static QName _EcoRoutesResponseElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoRoutesResponseElementRequest");
    private final static QName _EcoSpeedRouteInformationRequestElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoSpeedRouteInformationRequestElementRequest");
    private final static QName _EcoSpeedRouteInformationResponseElementResponse_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoSpeedRouteInformationResponseElementResponse");
    private final static QName _EcoSpeedRouteInformationResponseElementRequest_QNAME = new QName("http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", "ecoSpeedRouteInformationResponseElementRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EcoRoutesRequestType }
     * 
     */
    public EcoRoutesRequestType createEcoRoutesRequestType() {
        return new EcoRoutesRequestType();
    }

    /**
     * Create an instance of {@link EcoRoutesResponseReturnType }
     * 
     */
    public EcoRoutesResponseReturnType createEcoRoutesResponseReturnType() {
        return new EcoRoutesResponseReturnType();
    }

    /**
     * Create an instance of {@link EcoRoutesResponseType }
     * 
     */
    public EcoRoutesResponseType createEcoRoutesResponseType() {
        return new EcoRoutesResponseType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationRequestType }
     * 
     */
    public EcoSpeedRouteInformationRequestType createEcoSpeedRouteInformationRequestType() {
        return new EcoSpeedRouteInformationRequestType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationResponseReturnType }
     * 
     */
    public EcoSpeedRouteInformationResponseReturnType createEcoSpeedRouteInformationResponseReturnType() {
        return new EcoSpeedRouteInformationResponseReturnType();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationResponseType }
     * 
     */
    public EcoSpeedRouteInformationResponseType createEcoSpeedRouteInformationResponseType() {
        return new EcoSpeedRouteInformationResponseType();
    }

    /**
     * Create an instance of {@link Waypoint }
     * 
     */
    public Waypoint createWaypoint() {
        return new Waypoint();
    }

    /**
     * Create an instance of {@link WeatherCondition }
     * 
     */
    public WeatherCondition createWeatherCondition() {
        return new WeatherCondition();
    }

    /**
     * Create an instance of {@link AccidentInfo }
     * 
     */
    public AccidentInfo createAccidentInfo() {
        return new AccidentInfo();
    }

    /**
     * Create an instance of {@link SituationInfo }
     * 
     */
    public SituationInfo createSituationInfo() {
        return new SituationInfo();
    }

    /**
     * Create an instance of {@link CongestionInfo }
     * 
     */
    public CongestionInfo createCongestionInfo() {
        return new CongestionInfo();
    }

    /**
     * Create an instance of {@link RouteSegment }
     * 
     */
    public RouteSegment createRouteSegment() {
        return new RouteSegment();
    }

    /**
     * Create an instance of {@link RouteSegmentInfo }
     * 
     */
    public RouteSegmentInfo createRouteSegmentInfo() {
        return new RouteSegmentInfo();
    }

    /**
     * Create an instance of {@link Route }
     * 
     */
    public Route createRoute() {
        return new Route();
    }

    /**
     * Create an instance of {@link RouteData }
     * 
     */
    public RouteData createRouteData() {
        return new RouteData();
    }

    /**
     * Create an instance of {@link RouteInfo }
     * 
     */
    public RouteInfo createRouteInfo() {
        return new RouteInfo();
    }

    /**
     * Create an instance of {@link EcoRoutesRequest }
     * 
     */
    public EcoRoutesRequest createEcoRoutesRequest() {
        return new EcoRoutesRequest();
    }

    /**
     * Create an instance of {@link EcoFriendlyRoutesInformationRequest }
     * 
     */
    public EcoFriendlyRoutesInformationRequest createEcoFriendlyRoutesInformationRequest() {
        return new EcoFriendlyRoutesInformationRequest();
    }

    /**
     * Create an instance of {@link RoutesRequest }
     * 
     */
    public RoutesRequest createRoutesRequest() {
        return new RoutesRequest();
    }

    /**
     * Create an instance of {@link RoutesSuggestion }
     * 
     */
    public RoutesSuggestion createRoutesSuggestion() {
        return new RoutesSuggestion();
    }

    /**
     * Create an instance of {@link TrafficRouteInformationRequest }
     * 
     */
    public TrafficRouteInformationRequest createTrafficRouteInformationRequest() {
        return new TrafficRouteInformationRequest();
    }

    /**
     * Create an instance of {@link TrafficRouteInformationResponse }
     * 
     */
    public TrafficRouteInformationResponse createTrafficRouteInformationResponse() {
        return new TrafficRouteInformationResponse();
    }

    /**
     * Create an instance of {@link EcoFriendlyRoutesInformationResponse }
     * 
     */
    public EcoFriendlyRoutesInformationResponse createEcoFriendlyRoutesInformationResponse() {
        return new EcoFriendlyRoutesInformationResponse();
    }

    /**
     * Create an instance of {@link EcoRoutesResponse }
     * 
     */
    public EcoRoutesResponse createEcoRoutesResponse() {
        return new EcoRoutesResponse();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationRequest }
     * 
     */
    public EcoSpeedRouteInformationRequest createEcoSpeedRouteInformationRequest() {
        return new EcoSpeedRouteInformationRequest();
    }

    /**
     * Create an instance of {@link EcoSpeedRouteInformationResponse }
     * 
     */
    public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse() {
        return new EcoSpeedRouteInformationResponse();
    }

    /**
     * Create an instance of {@link ChoreographyInstanceRequest }
     * 
     */
    public ChoreographyInstanceRequest createChoreographyInstanceRequest() {
        return new ChoreographyInstanceRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoRoutesRequestElementRequest")
    public JAXBElement<EcoRoutesRequestType> createEcoRoutesRequestElementRequest(EcoRoutesRequestType value) {
        return new JAXBElement<EcoRoutesRequestType>(_EcoRoutesRequestElementRequest_QNAME, EcoRoutesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesResponseReturnType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoRoutesResponseElementResponse")
    public JAXBElement<EcoRoutesResponseReturnType> createEcoRoutesResponseElementResponse(EcoRoutesResponseReturnType value) {
        return new JAXBElement<EcoRoutesResponseReturnType>(_EcoRoutesResponseElementResponse_QNAME, EcoRoutesResponseReturnType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoRoutesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoRoutesResponseElementRequest")
    public JAXBElement<EcoRoutesResponseType> createEcoRoutesResponseElementRequest(EcoRoutesResponseType value) {
        return new JAXBElement<EcoRoutesResponseType>(_EcoRoutesResponseElementRequest_QNAME, EcoRoutesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoSpeedRouteInformationRequestElementRequest")
    public JAXBElement<EcoSpeedRouteInformationRequestType> createEcoSpeedRouteInformationRequestElementRequest(EcoSpeedRouteInformationRequestType value) {
        return new JAXBElement<EcoSpeedRouteInformationRequestType>(_EcoSpeedRouteInformationRequestElementRequest_QNAME, EcoSpeedRouteInformationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationResponseReturnType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoSpeedRouteInformationResponseElementResponse")
    public JAXBElement<EcoSpeedRouteInformationResponseReturnType> createEcoSpeedRouteInformationResponseElementResponse(EcoSpeedRouteInformationResponseReturnType value) {
        return new JAXBElement<EcoSpeedRouteInformationResponseReturnType>(_EcoSpeedRouteInformationResponseElementResponse_QNAME, EcoSpeedRouteInformationResponseReturnType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EcoSpeedRouteInformationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eu.chorevolution.urbantrafficcoordination.seada/cdnd", name = "ecoSpeedRouteInformationResponseElementRequest")
    public JAXBElement<EcoSpeedRouteInformationResponseType> createEcoSpeedRouteInformationResponseElementRequest(EcoSpeedRouteInformationResponseType value) {
        return new JAXBElement<EcoSpeedRouteInformationResponseType>(_EcoSpeedRouteInformationResponseElementRequest_QNAME, EcoSpeedRouteInformationResponseType.class, null, value);
    }

}
