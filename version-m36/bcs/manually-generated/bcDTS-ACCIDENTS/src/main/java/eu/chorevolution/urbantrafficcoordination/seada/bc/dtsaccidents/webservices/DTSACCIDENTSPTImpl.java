package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.DTSACCIDENTSPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentAccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;

@Component(value = "DTSACCIDENTSPTImpl")
public class DTSACCIDENTSPTImpl implements DTSACCIDENTSPT{

	private static Logger logger = LoggerFactory.getLogger(DTSACCIDENTSPTImpl.class);

	@Autowired
	private DTSAccidentsService service;	
	
	@Override
	public SegmentAccidentInformationResponse segmentAccidentInformation(SegmentInformationRequest parameters) {
		
		logger.info("CALLED segmentAccidentInformation ON bcDTS-ACCIDENTS");
		SegmentAccidentInformationResponse segmentAccidentInformationResponse = service.segmentAccidentInformation(parameters);
		return segmentAccidentInformationResponse;
	}
	
}
