package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentAccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;

@Service
public class DummyDTSAccidentsServiceImpl implements DTSAccidentsService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSAccidentsServiceImpl.class);
	
	@Override
	public SegmentAccidentInformationResponse segmentAccidentInformation(SegmentInformationRequest segmentInformationRequest) {

		logger.info("CALLED segmentAccidentInformation ON DUMMY bcDTS-ACCIDENTS");	
		return new SegmentAccidentInformationResponse();
	}
}
