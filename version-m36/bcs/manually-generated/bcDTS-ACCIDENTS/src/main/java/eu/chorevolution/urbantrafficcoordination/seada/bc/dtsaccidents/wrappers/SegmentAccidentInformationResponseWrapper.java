package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentAccidentInformationResponse;

public class SegmentAccidentInformationResponseWrapper {

	private SegmentAccidentInformationResponse segmentAccidentInformationResponse;

	public SegmentAccidentInformationResponse getSegmentAccidentInformationResponse() {
		return segmentAccidentInformationResponse;
	}

	public void setSegmentAccidentInformationResponse(
			SegmentAccidentInformationResponse segmentAccidentInformationResponse) {
		this.segmentAccidentInformationResponse = segmentAccidentInformationResponse;
	}

}
