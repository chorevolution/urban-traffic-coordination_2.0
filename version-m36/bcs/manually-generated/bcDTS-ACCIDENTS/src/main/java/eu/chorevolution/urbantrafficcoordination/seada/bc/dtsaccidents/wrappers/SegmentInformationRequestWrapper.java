package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentInformationRequest;

public class SegmentInformationRequestWrapper {

	private SegmentInformationRequest segmentInformationRequest;

	public SegmentInformationRequest getSegmentInformationRequest() {
		return segmentInformationRequest;
	}

	public void setSegmentInformationRequest(SegmentInformationRequest segmentInformationRequest) {
		this.segmentInformationRequest = segmentInformationRequest;
	}
	
}
