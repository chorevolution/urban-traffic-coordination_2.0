package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentAccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers.SegmentAccidentInformationResponseWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers.SegmentInformationRequestWrapper;

@Service
public class RESTDTSAccidentsServiceImpl implements DTSAccidentsService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSAccidentsServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;
	
	@Value("#{cfgproperties.path}")
	private String path;
	
	@Override
	public SegmentAccidentInformationResponse segmentAccidentInformation(SegmentInformationRequest segmentInformationRequest) {
		
		logger.info("CALLED segmentAccidentInformation ON REST bcDTS-ACCIDENTS");			
		SegmentAccidentInformationResponse segmentAccidentInformationResponse = null;
		Gson gson = new GsonBuilder().create();			
		HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			SegmentInformationRequestWrapper segmentInformationRequestWrapper = new SegmentInformationRequestWrapper();
			segmentInformationRequestWrapper.setSegmentInformationRequest(segmentInformationRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(segmentInformationRequestWrapper), ContentType.APPLICATION_JSON);
			postRequest.setEntity(requestEntity);
			HttpResponse httpResponse = httpClient.execute(postRequest);	
			String jsonResponse = EntityUtils.toString(httpResponse.getEntity());
			SegmentAccidentInformationResponseWrapper segmentAccidentInformationResponseWrapper = gson.fromJson(jsonResponse, SegmentAccidentInformationResponseWrapper.class);
			segmentAccidentInformationResponse = segmentAccidentInformationResponseWrapper.getSegmentAccidentInformationResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}		
		return segmentAccidentInformationResponse;
	}
	
}
