package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentInformationRequest;

public class SegmentInformationRequestWrapper {

	private SegmentInformationRequest segmentInformationRequest;

	public SegmentInformationRequest getSegmentInformationRequest() {
		return segmentInformationRequest;
	}

	public void setSegmentInformationRequest(SegmentInformationRequest segmentInformationRequest) {
		this.segmentInformationRequest = segmentInformationRequest;
	}
	
}
