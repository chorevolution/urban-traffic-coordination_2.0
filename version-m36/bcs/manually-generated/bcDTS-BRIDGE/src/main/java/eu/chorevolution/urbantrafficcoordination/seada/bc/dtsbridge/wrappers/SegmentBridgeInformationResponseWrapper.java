package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentBridgeInformationResponse;

public class SegmentBridgeInformationResponseWrapper {
	
	private SegmentBridgeInformationResponse segmentBridgeInformationResponse;

	public SegmentBridgeInformationResponse getSegmentBridgeInformationResponse() {
		return segmentBridgeInformationResponse;
	}

	public void setSegmentBridgeInformationResponse(SegmentBridgeInformationResponse segmentBridgeInformationResponse) {
		this.segmentBridgeInformationResponse = segmentBridgeInformationResponse;
	}
	
}
