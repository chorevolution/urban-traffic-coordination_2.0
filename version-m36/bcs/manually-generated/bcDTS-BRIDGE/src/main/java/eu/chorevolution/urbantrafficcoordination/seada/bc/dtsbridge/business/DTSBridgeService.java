package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentBridgeInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentInformationRequest;

public interface DTSBridgeService {
	SegmentBridgeInformationResponse bridgeStatusInformation(SegmentInformationRequest segmentInformationRequest);
}
