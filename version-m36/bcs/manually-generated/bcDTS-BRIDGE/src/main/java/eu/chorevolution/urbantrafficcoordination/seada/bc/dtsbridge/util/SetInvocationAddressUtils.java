package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.model.ArtifactEndpointData;

public class SetInvocationAddressUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SetInvocationAddressUtils.class);

	// role, ArtifactAddressData
	private static Map<String, ArtifactEndpointData> artifactsEndpointsData = new HashMap<>();
	
	public static void storeArtifactEndpointData(String role, String name, List<String> endpoints){
		
		artifactsEndpointsData.put(role, new ArtifactEndpointData(name, role, endpoints));
		LOGGER.info("BC JOURNEY PLANNER set invocation address for artifact " + name + " with role " + role + " to " + endpoints.get(0));
	}
	
	public static String getArtifactEndpointAddressFromRole(String role) {

		if (artifactsEndpointsData.containsKey(role)) {
			String address = artifactsEndpointsData.get(role).getEndpoints().get(0);
			LOGGER.info("BC JOURNEY PLANNER found invocation address to call artifact with role " + role + ": " + address);
			return address;
		} else {
			LOGGER.info("BC JOURNEY PLANNER has NOT found invocation address to call artifact with role " + role);
			return null;
		}
	}
}
