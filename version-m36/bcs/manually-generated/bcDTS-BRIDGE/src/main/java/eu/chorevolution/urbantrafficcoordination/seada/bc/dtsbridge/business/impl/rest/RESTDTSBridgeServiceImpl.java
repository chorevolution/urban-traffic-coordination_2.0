package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentBridgeInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers.SegmentBridgeInformationResponseWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers.SegmentInformationRequestWrapper;

@Service
public class RESTDTSBridgeServiceImpl implements DTSBridgeService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSBridgeServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;

	@Override
	public SegmentBridgeInformationResponse bridgeStatusInformation(
			SegmentInformationRequest segmentInformationRequest) {
		
		logger.info("CALLED bridgeStatusInformation ON REST bcDTS-BRIDGE");			
		SegmentBridgeInformationResponse segmentBridgeInformationResponse = null;
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();		
			Gson gson = new GsonBuilder().create();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			SegmentInformationRequestWrapper segmentInformationRequestWrapper = new SegmentInformationRequestWrapper();
			segmentInformationRequestWrapper.setSegmentInformationRequest(segmentInformationRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(segmentInformationRequestWrapper), ContentType.APPLICATION_JSON);
			postRequest.setEntity(requestEntity);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String jsonResponse = EntityUtils.toString(httpResponse.getEntity());
			SegmentBridgeInformationResponseWrapper segmentBridgeInformationResponseWrapper = gson.fromJson(jsonResponse, SegmentBridgeInformationResponseWrapper.class);
			segmentBridgeInformationResponse = segmentBridgeInformationResponseWrapper.getSegmentBridgeInformationResponse();
		}
		catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}
		return segmentBridgeInformationResponse;
	}
}
