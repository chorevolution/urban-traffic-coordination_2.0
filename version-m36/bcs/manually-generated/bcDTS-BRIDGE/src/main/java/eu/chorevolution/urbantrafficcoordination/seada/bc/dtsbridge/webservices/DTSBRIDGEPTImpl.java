package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.DTSBRIDGEPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentBridgeInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;

@Component(value = "DTSBRIDGEPTImpl")
public class DTSBRIDGEPTImpl implements DTSBRIDGEPT{
	
	private static Logger logger = LoggerFactory.getLogger(DTSBRIDGEPTImpl.class);
	
	@Autowired
	DTSBridgeService service;

	@Override
	public SegmentBridgeInformationResponse bridgeStatusInformation(SegmentInformationRequest parameters) {
		
		logger.info("CALLED bridgeStatusInformation ON bcDTS-BRIDGE");
		SegmentBridgeInformationResponse segmentBridgeInformationResponse = service.bridgeStatusInformation(parameters);
		return segmentBridgeInformationResponse;
	}
}
