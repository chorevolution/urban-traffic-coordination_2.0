package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentBridgeInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;

@Service
public class DummyDTSBridgeServiceImpl implements DTSBridgeService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSBridgeServiceImpl.class);	

	@Override
	public SegmentBridgeInformationResponse bridgeStatusInformation(
			SegmentInformationRequest segmentInformationRequest) {
		
		logger.info("CALLED bridgeStatusInformation ON DUMMY bcDTS-BRIDGE");	
		return new SegmentBridgeInformationResponse();
	}

}
