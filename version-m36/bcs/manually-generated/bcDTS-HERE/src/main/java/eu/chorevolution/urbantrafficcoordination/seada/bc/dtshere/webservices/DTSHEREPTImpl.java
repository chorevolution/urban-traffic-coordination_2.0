package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.DTSHEREPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business.DTSHEREService;

@Component(value="DTSHEREPTImpl")
public class DTSHEREPTImpl implements DTSHEREPT{

	private static Logger logger = LoggerFactory.getLogger(DTSHEREPTImpl.class);
	
	@Autowired
	DTSHEREService service;

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {
		
		logger.info("CALLED routesRequest ON bcDTS-HERE");
		RoutesSuggestion routesSuggestion = service.routesRequest(parameters);
		return routesSuggestion;
	}
	
}
