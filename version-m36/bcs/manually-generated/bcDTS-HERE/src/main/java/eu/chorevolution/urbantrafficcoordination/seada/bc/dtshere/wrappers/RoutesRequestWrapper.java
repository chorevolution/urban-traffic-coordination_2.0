package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;

public class RoutesRequestWrapper {
	
	RoutesRequest routesRequest;

	public RoutesRequest getRoutesRequest() {
		return routesRequest;
	}

	public void setRoutesRequest(RoutesRequest routesRequest) {
		this.routesRequest = routesRequest;
	}

}
