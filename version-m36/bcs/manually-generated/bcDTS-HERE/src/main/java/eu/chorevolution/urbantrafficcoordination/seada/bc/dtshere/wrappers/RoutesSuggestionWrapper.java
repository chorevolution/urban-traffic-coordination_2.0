package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;

public class RoutesSuggestionWrapper {

	RoutesSuggestion routesSuggestion;

	public RoutesSuggestion getRoutesSuggestion() {
		return routesSuggestion;
	}

	public void setRoutesSuggestion(RoutesSuggestion routesSuggestion) {
		this.routesSuggestion = routesSuggestion;
	}
	
}
