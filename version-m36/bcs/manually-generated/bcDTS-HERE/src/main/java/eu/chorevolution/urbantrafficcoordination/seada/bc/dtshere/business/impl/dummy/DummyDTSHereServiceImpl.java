package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business.DTSHEREService;

@Service
public class DummyDTSHereServiceImpl implements DTSHEREService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSHereServiceImpl.class);	

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {

		logger.info("CALLED routesRequest ON DUMMY bcDTS-HERE");
		return new RoutesSuggestion();
	}
}
