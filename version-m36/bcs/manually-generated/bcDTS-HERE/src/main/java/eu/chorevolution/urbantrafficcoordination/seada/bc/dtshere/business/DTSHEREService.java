package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;

public interface DTSHEREService {
	RoutesSuggestion routesRequest(RoutesRequest parameters) ;
}
