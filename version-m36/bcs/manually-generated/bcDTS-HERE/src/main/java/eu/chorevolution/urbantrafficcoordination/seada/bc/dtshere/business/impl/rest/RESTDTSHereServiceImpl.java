package eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.business.DTSHEREService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.wrappers.RoutesRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtshere.wrappers.RoutesSuggestionWrapper;

@Service
public class RESTDTSHereServiceImpl implements DTSHEREService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSHereServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {

		logger.info("CALLED routesRequest ON REST bcDTS-HERE");
		RoutesSuggestion routesSuggestion = null;
		try {
			Gson gson = new GsonBuilder().create();			
			HttpClient httpClient = HttpClientBuilder.create().build();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();			
			HttpPost postRequest = new HttpPost(uri);
			RoutesRequestWrapper routesRequestWrapper = new RoutesRequestWrapper();
			routesRequestWrapper.setRoutesRequest(parameters);
			StringEntity requestEntity = new StringEntity(gson.toJson(routesRequestWrapper), ContentType.APPLICATION_JSON);
			postRequest.setEntity(requestEntity);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String jsonResponse = EntityUtils.toString(httpResponse.getEntity());
			RoutesSuggestionWrapper routesSuggestionWrapper = gson.fromJson(jsonResponse, RoutesSuggestionWrapper.class);
			routesSuggestion = routesSuggestionWrapper.getRoutesSuggestion();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}		
		return routesSuggestion;
	}
}
