package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;

public interface DTSGOOGLEService {
	RoutesSuggestion routesRequest(RoutesRequest parameters) ;
}
