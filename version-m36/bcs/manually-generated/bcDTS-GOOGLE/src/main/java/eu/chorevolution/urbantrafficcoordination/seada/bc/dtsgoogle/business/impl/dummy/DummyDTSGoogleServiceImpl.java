package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.business.DTSGOOGLEService;

@Service
public class DummyDTSGoogleServiceImpl implements DTSGOOGLEService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSGoogleServiceImpl.class);	

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {

		logger.info("CALLED routesRequest ON DUMMY bcDTS-GOOGLE");
		return new RoutesSuggestion();
	}
}
