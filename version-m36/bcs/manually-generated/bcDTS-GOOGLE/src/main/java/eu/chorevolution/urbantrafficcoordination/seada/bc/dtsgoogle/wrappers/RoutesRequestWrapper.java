package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;

public class RoutesRequestWrapper {
	
	RoutesRequest routesRequest;

	public RoutesRequest getRoutesRequest() {
		return routesRequest;
	}

	public void setRoutesRequest(RoutesRequest routesRequest) {
		this.routesRequest = routesRequest;
	}

}
