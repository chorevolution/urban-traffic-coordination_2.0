package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;

public class RoutesSuggestionWrapper {

	RoutesSuggestion routesSuggestion;

	public RoutesSuggestion getRoutesSuggestion() {
		return routesSuggestion;
	}

	public void setRoutesSuggestion(RoutesSuggestion routesSuggestion) {
		this.routesSuggestion = routesSuggestion;
	}
	
}
