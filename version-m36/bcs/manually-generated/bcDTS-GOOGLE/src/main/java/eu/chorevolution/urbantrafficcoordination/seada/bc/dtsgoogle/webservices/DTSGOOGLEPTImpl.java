package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.DTSGOOGLEPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsgoogle.business.DTSGOOGLEService;

@Component(value="DTSGOOGLEPTImpl")
public class DTSGOOGLEPTImpl implements DTSGOOGLEPT{

	private static Logger logger = LoggerFactory.getLogger(DTSGOOGLEPTImpl.class);
	
	@Autowired
	DTSGOOGLEService service;

	@Override
	public RoutesSuggestion routesRequest(RoutesRequest parameters) {
		
		logger.info("CALLED routesRequest ON bcDTS-GOOGLE");
		RoutesSuggestion routesSuggestion = service.routesRequest(parameters);
		return routesSuggestion;
	}
	
}
