package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentCongestionInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.DTSCongestionService;

@Service
public class DummyDTSCongestionServiceImpl implements DTSCongestionService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSCongestionServiceImpl.class);

	@Override
	public SegmentCongestionInformationResponse segmentCongestionInformation(SegmentInformationRequest parameters) {

		logger.info("CALLED segmentCongestionInformation ON DUMMY bcDTS-CONGESTION");	
		return new SegmentCongestionInformationResponse();
	}

}
