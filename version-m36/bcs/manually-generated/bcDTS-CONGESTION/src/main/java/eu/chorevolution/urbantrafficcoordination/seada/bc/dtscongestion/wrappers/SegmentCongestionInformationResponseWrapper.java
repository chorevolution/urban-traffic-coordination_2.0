package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentCongestionInformationResponse;

public class SegmentCongestionInformationResponseWrapper {

	private SegmentCongestionInformationResponse segmentCongestionInformationResponse;

	public SegmentCongestionInformationResponse getSegmentCongestionInformationResponse() {
		return segmentCongestionInformationResponse;
	}

	public void setSegmentCongestionInformationResponse(
			SegmentCongestionInformationResponse segmentCongestionInformationResponse) {
		this.segmentCongestionInformationResponse = segmentCongestionInformationResponse;
	}
	
}
