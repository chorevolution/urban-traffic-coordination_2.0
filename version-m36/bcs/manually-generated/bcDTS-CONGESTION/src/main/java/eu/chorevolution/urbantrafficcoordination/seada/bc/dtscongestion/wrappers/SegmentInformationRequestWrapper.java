package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentInformationRequest;

public class SegmentInformationRequestWrapper {
	
	private SegmentInformationRequest segmentInformationRequest;

	public SegmentInformationRequest getSegmentInformationRequest() {
		return segmentInformationRequest;
	}

	public void setSegmentInformationRequest(SegmentInformationRequest segmentInformationRequest) {
		this.segmentInformationRequest = segmentInformationRequest;
	}

}
