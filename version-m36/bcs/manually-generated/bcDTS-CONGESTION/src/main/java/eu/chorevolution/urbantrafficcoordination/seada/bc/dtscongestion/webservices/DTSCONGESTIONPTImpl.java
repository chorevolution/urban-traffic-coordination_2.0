package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.DTSCONGESTIONPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentCongestionInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.DTSCongestionService;

@Component(value = "DTSCONGESTIONPTImpl")
public class DTSCONGESTIONPTImpl implements DTSCONGESTIONPT{

	private static Logger logger = LoggerFactory.getLogger(DTSCONGESTIONPTImpl.class);
	
	@Autowired
	DTSCongestionService service;
	
	@Override
	public SegmentCongestionInformationResponse segmentCongestionInformation(SegmentInformationRequest parameters) {

		logger.info("CALLED segmentCongestionInformation ON bcDTS-CONGESTION");
		SegmentCongestionInformationResponse segmentCongestionInformationResponse = service.segmentCongestionInformation(parameters);
		return segmentCongestionInformationResponse;
	}

}
