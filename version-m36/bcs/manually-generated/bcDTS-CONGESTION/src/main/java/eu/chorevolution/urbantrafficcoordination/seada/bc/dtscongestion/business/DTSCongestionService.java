package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentCongestionInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.SegmentInformationRequest;

public interface DTSCongestionService {
 
	SegmentCongestionInformationResponse segmentCongestionInformation(SegmentInformationRequest parameters);
	
}
