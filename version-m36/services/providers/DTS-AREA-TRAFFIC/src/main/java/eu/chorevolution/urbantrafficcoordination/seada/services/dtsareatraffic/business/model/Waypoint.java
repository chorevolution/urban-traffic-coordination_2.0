
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Waypoint implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9216949538359722460L;

	@Column(name = "lat", nullable = true)
	private double lat;

	@Column(name = "lon", nullable = true)
	private double lon;

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

}
