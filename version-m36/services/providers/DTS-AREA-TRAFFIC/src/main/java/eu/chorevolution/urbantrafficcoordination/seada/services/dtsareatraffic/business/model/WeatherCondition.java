
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Embeddable
public class WeatherCondition implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1801506672879444845L;

	@OneToOne
	@JoinColumn(name = "route_segment_info_id")
	private RouteSegmentTrafficInfo routeSegmentInfo;

	@Column(name = "road_temperature", nullable = true)
	private double roadTemperature;

	@Column(name = "air_temperature", nullable = true)
	private double airTemperature;

	@Column(name = "air_relative_humidity", nullable = true)
	private double airRelativeHumidity;

	@Column(name = "wind_force", nullable = true)
	private double windForce;

	public RouteSegmentTrafficInfo getRouteSegmentInfo() {
		return routeSegmentInfo;
	}

	public void setRouteSegmentInfo(RouteSegmentTrafficInfo routeSegmentInfo) {
		this.routeSegmentInfo = routeSegmentInfo;
	}

	public double getRoadTemperature() {
		return roadTemperature;
	}

	public void setRoadTemperature(double roadTemperature) {
		this.roadTemperature = roadTemperature;
	}

	public double getAirTemperature() {
		return airTemperature;
	}

	public void setAirTemperature(double airTemperature) {
		this.airTemperature = airTemperature;
	}

	public double getAirRelativeHumidity() {
		return airRelativeHumidity;
	}

	public void setAirRelativeHumidity(double airRelativeHumidity) {
		this.airRelativeHumidity = airRelativeHumidity;
	}

	public double getWindForce() {
		return windForce;
	}

	public void setWindForce(double windForce) {
		this.windForce = windForce;
	}

}
