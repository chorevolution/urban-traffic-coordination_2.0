
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class RouteSegment {

	private Waypoint waypoint0;
	private Waypoint waypoint1;
	private String instruction;
	private int time;
	private int distance;
	private String polyline;

	public Waypoint getWaypoint0() {
		return waypoint0;
	}

	public void setWaypoint0(Waypoint value) {
		this.waypoint0 = value;
	}

	public Waypoint getWaypoint1() {
		return waypoint1;
	}

	public void setWaypoint1(Waypoint value) {
		this.waypoint1 = value;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String value) {
		this.instruction = value;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int value) {
		this.time = value;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int value) {
		this.distance = value;
	}

	public String getPolyline() {
		return polyline;
	}

	public void setPolyline(String value) {
		this.polyline = value;
	}

}
