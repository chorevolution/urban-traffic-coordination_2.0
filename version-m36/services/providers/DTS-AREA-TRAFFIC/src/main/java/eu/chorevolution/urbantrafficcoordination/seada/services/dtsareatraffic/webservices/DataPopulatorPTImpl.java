package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.services.datapopulator.DataPopulatorPT;
import eu.chorevolution.urbantrafficcoordination.seada.services.datapopulator.PopulateDataRequestElementRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.DataPopulatorService;

@Component(value = "DataPopulatorPTImpl")
public class DataPopulatorPTImpl implements DataPopulatorPT {

	private static Logger logger = LoggerFactory.getLogger(DataPopulatorPTImpl.class);

	@Autowired
	private DataPopulatorService service;

	@Override
	public void populateData(PopulateDataRequestElementRequest parameters) {

		logger.info("CALLED populateData ON DTS-AREA-TRAFFIC");
		service.populateData(parameters.getDataPopulationMode().toString());
	}

}
