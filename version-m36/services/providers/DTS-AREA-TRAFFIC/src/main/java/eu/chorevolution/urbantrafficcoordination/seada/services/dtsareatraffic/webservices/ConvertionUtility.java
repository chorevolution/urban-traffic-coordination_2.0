package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.webservices;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.AccidentInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

public class ConvertionUtility {

	public static XMLGregorianCalendar date2XMLGregorianCalendar(Date date) {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(date.getTime());
		XMLGregorianCalendar xmlGregorianCalendar;
		try {
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			return xmlGregorianCalendar;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<RouteSegment> getRouteSegmentsFromWS2Model(List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegment> input) {

		List<RouteSegment> result = new ArrayList<>();
		for (eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegment routeSegmentInput : input) {
			RouteSegment routeSegment = new RouteSegment();
			Waypoint destination = new Waypoint();
			destination.setLat(routeSegmentInput.getWaypoint1().getLat());
			destination.setLon(routeSegmentInput.getWaypoint1().getLon());
			routeSegment.setDestination(destination);
			Waypoint origin = new Waypoint();
			origin.setLat(routeSegmentInput.getWaypoint0().getLat());
			origin.setLon(routeSegmentInput.getWaypoint0().getLon());
			routeSegment.setOrigin(origin);
			result.add(routeSegment);
		}

		return result;
	}

	public static List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentInfo>
			getRouteSegmentsInfoFromModel2WS(List<RouteSegmentTrafficInfo> input) {

		List<eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentInfo> result =
				new ArrayList<>();
		for (RouteSegmentTrafficInfo routeSegmentInfoInput : input) {
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentInfo routeSegmentInfo =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentInfo();
			routeSegmentInfo.setBridgeStatus(routeSegmentInfoInput.getBridgeStatus());
			routeSegmentInfo.setCongestion(routeSegmentInfoInput.getCongestion());
			routeSegmentInfo.setEcoValue(routeSegmentInfoInput.getEcoValue());
			routeSegmentInfo.setId(routeSegmentInfoInput.getOriginalID());

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegment routeSegment =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegment();
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint destination =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint();
			destination.setLat(routeSegmentInfoInput.getRouteSegment().getDestination().getLat());
			destination.setLon(routeSegmentInfoInput.getRouteSegment().getDestination().getLon());
			routeSegment.setWaypoint1(destination);
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint origin =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint();
			origin.setLat(routeSegmentInfoInput.getRouteSegment().getOrigin().getLat());
			origin.setLon(routeSegmentInfoInput.getRouteSegment().getOrigin().getLon());
			routeSegment.setWaypoint0(origin);
			routeSegmentInfo.setRouteSegment(routeSegment);

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.SituationInfo situationInfo =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.SituationInfo();
			for (AccidentInfo accidentInfoItem : routeSegmentInfoInput.getSituationInfo()
					.getAccidents()) {
				eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AccidentInfo accidentInfo =
						new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AccidentInfo();
				accidentInfo.setDescription(accidentInfoItem.getDescription());
				accidentInfo.setLatitude(accidentInfoItem.getLatitude());
				accidentInfo.setLongitude(accidentInfoItem.getLongitude());
				accidentInfo.setMessageCodeValue(accidentInfoItem.getMessageCodeValue());
				accidentInfo.setMessageTypeValue(accidentInfoItem.getMessageTypeValue());
				accidentInfo.setSeverityCode(accidentInfoItem.getSeverityCode());
				situationInfo.getAccidents().add(accidentInfo);
			}
			situationInfo.setSituationSummaryInfo(
					routeSegmentInfoInput.getSituationInfo().getSituationSummaryInfo());
			situationInfo
					.setType(routeSegmentInfoInput.getSituationInfo().getSituationSummaryInfo());
			routeSegmentInfo.setSituationInfo(situationInfo);

			routeSegmentInfo.setSpeed(routeSegmentInfoInput.getSpeed());
			routeSegmentInfo.setType(routeSegmentInfoInput.getType());
			routeSegmentInfo.setUpdatedAt(routeSegmentInfoInput.getUpdatedAt());

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.WeatherCondition weatherCondition =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.WeatherCondition();
			weatherCondition.setAirRelativeHumidity(
					routeSegmentInfoInput.getWeatherInfo().getAirRelativeHumidity());
			weatherCondition
					.setAirTemperature(routeSegmentInfoInput.getWeatherInfo().getAirTemperature());
			weatherCondition.setRoadTemperature(
					routeSegmentInfoInput.getWeatherInfo().getRoadTemperature());
			weatherCondition.setWindForce(routeSegmentInfoInput.getWeatherInfo().getWindForce());
			routeSegmentInfo.setWeatherInfo(weatherCondition);

			result.add(routeSegmentInfo);
		}

		return result;
	}

	public static List<RouteSegmentTrafficInfo> getRouteSegmentsTrafficInfoFromWS2Model(List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentTrafficInfo> input) {

		List<RouteSegmentTrafficInfo> result = new ArrayList<>();
		for (eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentTrafficInfo routeSegmentTrafficInfoInput : input) {
			RouteSegmentTrafficInfo routeSegmentInfo = new RouteSegmentTrafficInfo();
			routeSegmentInfo.setBridgeStatus(routeSegmentTrafficInfoInput.getBridgeStatus());
			routeSegmentInfo.setCongestion(routeSegmentTrafficInfoInput.getCongestion());
			routeSegmentInfo.setEcoValue(routeSegmentTrafficInfoInput.getEcoValue());
			routeSegmentInfo.setOriginalID(routeSegmentTrafficInfoInput.getId());

			RouteSegment routeSegment = new RouteSegment();
			Waypoint destination = new Waypoint();
			destination.setLat(
					routeSegmentTrafficInfoInput.getRouteSegmentPoints().getWaypoint1().getLat());
			destination.setLon(
					routeSegmentTrafficInfoInput.getRouteSegmentPoints().getWaypoint1().getLon());
			routeSegment.setDestination(destination);
			Waypoint origin = new Waypoint();
			origin.setLat(
					routeSegmentTrafficInfoInput.getRouteSegmentPoints().getWaypoint0().getLat());
			origin.setLon(
					routeSegmentTrafficInfoInput.getRouteSegmentPoints().getWaypoint0().getLon());
			routeSegment.setOrigin(origin);
			routeSegmentInfo.setRouteSegment(routeSegment);

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.SituationInfo situationInfo =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.SituationInfo();
			for (eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AccidentInfo accidentInfoItem : routeSegmentTrafficInfoInput
					.getSituationInfo().getAccidents()) {
				AccidentInfo accidentInfo = new AccidentInfo();
				accidentInfo.setDescription(accidentInfoItem.getDescription());
				accidentInfo.setLatitude(accidentInfoItem.getLatitude());
				accidentInfo.setLongitude(accidentInfoItem.getLongitude());
				accidentInfo.setMessageCodeValue(accidentInfoItem.getMessageCodeValue());
				accidentInfo.setMessageTypeValue(accidentInfoItem.getMessageTypeValue());
				accidentInfo.setSeverityCode(accidentInfoItem.getSeverityCode());
				situationInfo.addAccident(accidentInfo);
			}
			situationInfo.setSituationSummaryInfo(
					routeSegmentTrafficInfoInput.getSituationInfo().getSituationSummaryInfo());
			situationInfo.setType(
					routeSegmentTrafficInfoInput.getSituationInfo().getSituationSummaryInfo());
			routeSegmentInfo.setSituationInfo(situationInfo);

			routeSegmentInfo.setSpeed(routeSegmentTrafficInfoInput.getSpeed());
			routeSegmentInfo.setType(routeSegmentTrafficInfoInput.getType());
			routeSegmentInfo.setUpdatedAt(routeSegmentTrafficInfoInput.getUpdatedAt());

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.WeatherCondition weatherCondition =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.WeatherCondition();
			weatherCondition.setAirRelativeHumidity(
					routeSegmentTrafficInfoInput.getWeatherInfo().getAirRelativeHumidity());
			weatherCondition.setAirTemperature(
					routeSegmentTrafficInfoInput.getWeatherInfo().getAirTemperature());
			weatherCondition.setRoadTemperature(
					routeSegmentTrafficInfoInput.getWeatherInfo().getRoadTemperature());
			weatherCondition
					.setWindForce(routeSegmentTrafficInfoInput.getWeatherInfo().getWindForce());
			routeSegmentInfo.setWeatherInfo(weatherCondition);

			result.add(routeSegmentInfo);
		}

		return result;
	}

	public static List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentPoints>
			getRouteSegmentsPointsFromModel2WS(List<RouteSegment> input) {

		List<eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentPoints> result =
				new ArrayList<>();

		for (RouteSegment routeSegmentInput : input) {
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentPoints routeSegmentPoints =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentPoints();
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint destination =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint();
			destination.setLat(routeSegmentInput.getDestination().getLat());
			destination.setLon(routeSegmentInput.getDestination().getLon());
			routeSegmentPoints.setWaypoint1(destination);
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint origin =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint();
			origin.setLat(routeSegmentInput.getOrigin().getLat());
			origin.setLon(routeSegmentInput.getOrigin().getLon());
			routeSegmentPoints.setWaypoint0(origin);
			result.add(routeSegmentPoints);
		}

		return result;
	}

	public static Waypoint getWaypointFromWS2Model(
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.Waypoint waypoint) {

		Waypoint result = new Waypoint();
		result.setLat(waypoint.getLat());
		result.setLon(waypoint.getLon());

		return result;
	}

	public static Map<String, Waypoint> convertOriginAndDestination(Waypoint origin,
			Waypoint destination) {

		Map<String, Waypoint> result = new HashMap<>();
		Waypoint originConverted = new Waypoint();
		originConverted.setLat(origin.getLat());
		originConverted.setLon(origin.getLon());
		Waypoint destinationConverted = new Waypoint();
		destinationConverted.setLat(destination.getLat());
		destinationConverted.setLon(destination.getLon());

		if (destination.getLon() < origin.getLon() && destination.getLat() < origin.getLat()) {
			originConverted.setLon(origin.getLon() - (origin.getLon() - destination.getLon()));
			destinationConverted
					.setLon(destination.getLon() + (origin.getLon() - destination.getLon()));
		} else if (origin.getLon() < destination.getLon()
				&& origin.getLat() < destination.getLat()) {
			originConverted.setLat(origin.getLat() + (destination.getLat() - origin.getLat()));
			destinationConverted
					.setLat(destination.getLat() - (destination.getLat() - origin.getLat()));
		} else if (destination.getLon() < origin.getLon()
				&& origin.getLat() < destination.getLat()) {
			originConverted.setLon(origin.getLon() - (origin.getLon() - destination.getLon()));
			originConverted.setLat(origin.getLat() + (destination.getLat() - origin.getLat()));
			destinationConverted
					.setLon(destination.getLon() + (origin.getLon() - destination.getLon()));
			destinationConverted
					.setLat(destination.getLat() - (destination.getLat() - origin.getLat()));
		}
		result.put("origin", originConverted);
		result.put("destination", destinationConverted);

		return result;
	}

}
