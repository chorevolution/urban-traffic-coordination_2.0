package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.SeadaAreaTrafficService;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.AccidentInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.RouteSegmentInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.TrafficRouteSegmentsInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.TrafficRouteSegmentsInformationRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.TrafficRouteSegmentsInformationResponseWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.Waypoint;

@Service
public class SeadaAreaTrafficServiceImpl implements SeadaAreaTrafficService {

	@Value("#{cfgproperties.seadaAreaTraffic_host}")
	private String seadaAreaTraffic_host;

	@Value("#{cfgproperties.seadaAreaTraffic_port}")
	private String seadaAreaTraffic_port;

	@Value("#{cfgproperties.seadaAreaTraffic_requestPath}")
	private String seadaAreaTraffic_requestPath;

	public List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment>
			getRouteSegmentsWithRouteSegmentsInfoFromSeadaAreaTraffic(List<
					eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment> routeSegments) {

		String seadaAreaTrafficURL = new StringBuilder("http://").append(seadaAreaTraffic_host)
				.append(":").append(seadaAreaTraffic_port).append(seadaAreaTraffic_requestPath)
				.toString();
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter =
				new MappingJackson2HttpMessageConverter();
		mappingJackson2HttpMessageConverter
				.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));
		restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		TrafficRouteSegmentsInformationRequest trafficRouteInformationRequest =
				new TrafficRouteSegmentsInformationRequest();
		trafficRouteInformationRequest.getRouteSegments()
				.addAll(getRouteSegmentsFromModel2SeadaAreaTraffic(routeSegments));

		TrafficRouteSegmentsInformationRequestWrapper trafficRouteSegmentsInformationRequestWrapper =
				new TrafficRouteSegmentsInformationRequestWrapper();
		trafficRouteSegmentsInformationRequestWrapper
				.setTrafficRouteSegmentsInformationRequest(trafficRouteInformationRequest);
		HttpEntity<Object> entity =
				new HttpEntity<Object>(trafficRouteSegmentsInformationRequestWrapper, headers);
		List<RouteSegmentInfo> routeSegmentInfos = restTemplate
				.postForObject(seadaAreaTrafficURL, entity,
						TrafficRouteSegmentsInformationResponseWrapper.class)
				.getTrafficRouteSegmentsInformationResponse().getRouteSegmentInfo();
		return getRouteSegmentsInfoFromSeadaAreaTraffic2RouteSegmentsFromModel(routeSegmentInfos);
	}

	private List<RouteSegment> getRouteSegmentsFromModel2SeadaAreaTraffic(List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment> routeSegments) {

		List<RouteSegment> result = new ArrayList<>();
		for (eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment routeSegmentItem : routeSegments) {
			RouteSegment routeSegment = new RouteSegment();
			Waypoint origin = new Waypoint();
			origin.setLat(routeSegmentItem.getOrigin().getLat());
			origin.setLon(routeSegmentItem.getOrigin().getLon());
			routeSegment.setWaypoint0(origin);
			Waypoint destination = new Waypoint();
			destination.setLat(routeSegmentItem.getDestination().getLat());
			destination.setLon(routeSegmentItem.getDestination().getLon());
			routeSegment.setWaypoint1(destination);
			routeSegment.setDistance(routeSegment.getDistance());
			routeSegment.setInstruction(routeSegment.getInstruction());
			routeSegment.setPolyline(routeSegment.getPolyline());
			routeSegment.setTime(routeSegment.getTime());
			routeSegment.setDistance(routeSegment.getDistance());
			result.add(routeSegment);
		}
		return result;
	}

	private List<
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment>
			getRouteSegmentsInfoFromSeadaAreaTraffic2RouteSegmentsFromModel(
					List<RouteSegmentInfo> routeSegmentsInfos) {

		List<eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment> results =
				new ArrayList<>();
		for (eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat.RouteSegmentInfo routeSegmentInfoItem : routeSegmentsInfos) {

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment routeSegment =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment();
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint origin =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint();
			origin.setLat(routeSegmentInfoItem.getRouteSegment().getWaypoint0().getLat());
			origin.setLon(routeSegmentInfoItem.getRouteSegment().getWaypoint0().getLon());
			routeSegment.setOrigin(origin);
			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint destination =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint();
			destination.setLat(routeSegmentInfoItem.getRouteSegment().getWaypoint1().getLat());
			destination.setLon(routeSegmentInfoItem.getRouteSegment().getWaypoint1().getLon());
			routeSegment.setDestination(destination);

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo routeSegmentInfo =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo();
			routeSegmentInfo.setBridgeStatus(routeSegmentInfoItem.getBridgeStatus());
			routeSegmentInfo.setCongestion(routeSegmentInfoItem.getCongestion());
			routeSegmentInfo.setEcoValue(routeSegmentInfoItem.getEcoValue());
			routeSegmentInfo.setOriginalID(routeSegmentInfoItem.getId());
			routeSegmentInfo.setSpeed(routeSegmentInfoItem.getSpeed());
			routeSegmentInfo.setType(routeSegmentInfoItem.getType());
			routeSegmentInfo.setUpdatedAt(routeSegmentInfoItem.getUpdatedAt());

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.SituationInfo situationInfo =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.SituationInfo();
			for (AccidentInfo accidentInfoItem : routeSegmentInfoItem.getSituationInfo()
					.getAccidents()) {
				eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.AccidentInfo accidentInfo =
						new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.AccidentInfo();
				accidentInfo.setDescription(accidentInfoItem.getDescription());
				accidentInfo.setLatitude(accidentInfoItem.getLatitude());
				accidentInfo.setLongitude(accidentInfoItem.getLongitude());
				accidentInfo.setMessageCodeValue(accidentInfoItem.getMessageCodeValue());
				accidentInfo.setMessageTypeValue(accidentInfoItem.getMessageTypeValue());
				accidentInfo.setSeverityCode(accidentInfoItem.getSeverityCode());
				situationInfo.addAccident(accidentInfo);
			}
			situationInfo.setSituationSummaryInfo(
					routeSegmentInfoItem.getSituationInfo().getSituationSummaryInfo());
			situationInfo.setType(routeSegmentInfoItem.getSituationInfo().getType());
			routeSegmentInfo.setSituationInfo(situationInfo);

			eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.WeatherCondition weatherCondition =
					new eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.WeatherCondition();
			weatherCondition.setAirRelativeHumidity(
					routeSegmentInfoItem.getWeatherInfo().getAirRelativeHumidity());
			weatherCondition
					.setAirTemperature(routeSegmentInfoItem.getWeatherInfo().getAirTemperature());
			weatherCondition
					.setRoadTemperature(routeSegmentInfoItem.getWeatherInfo().getRoadTemperature());
			weatherCondition.setWindForce(routeSegmentInfoItem.getWeatherInfo().getWindForce());
			routeSegmentInfo.setWeatherInfo(weatherCondition);

			routeSegmentInfo.setRouteSegment(routeSegment);
			routeSegment.setRouteSegmentInfo(routeSegmentInfo);
			results.add(routeSegment);
		}
		return results;
	}
}
