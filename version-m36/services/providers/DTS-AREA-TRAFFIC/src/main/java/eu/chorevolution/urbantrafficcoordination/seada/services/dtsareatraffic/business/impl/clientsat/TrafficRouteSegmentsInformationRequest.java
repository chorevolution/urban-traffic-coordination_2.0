
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

import java.util.ArrayList;
import java.util.List;

public class TrafficRouteSegmentsInformationRequest {

	private List<RouteSegment> routeSegments;

	public List<RouteSegment> getRouteSegments() {
		if (routeSegments == null) {
			routeSegments = new ArrayList<RouteSegment>();
		}
		return this.routeSegments;
	}

}
