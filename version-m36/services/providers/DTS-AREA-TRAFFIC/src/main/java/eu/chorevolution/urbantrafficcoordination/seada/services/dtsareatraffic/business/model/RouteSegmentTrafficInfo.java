
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "route_segments_traffic_info")
public class RouteSegmentTrafficInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6085788223849839055L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "route_segment_info_id")
	private Long id;

	@Column(name = "original_id", nullable = true)
	private int originalID;
	@Column(name = "bridge_status", nullable = true)
	private int bridgeStatus;

	@Embedded
	private WeatherCondition weatherInfo;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "situation_id", nullable = false)
	private SituationInfo situationInfo;

	@Column(name = "eco_value", nullable = true)
	private double ecoValue;
	@Column(name = "congestion", nullable = true)
	private double congestion;
	@Column(name = "speed", nullable = true)
	private double speed;

	@Column(name = "updated_at", nullable = true, length = 255)
	private String updatedAt;

	@Column(name = "type", nullable = true, length = 255)
	private String type;

	@OneToOne(mappedBy = "routeSegmentInfo")
	private RouteSegment routeSegment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOriginalID() {
		return originalID;
	}

	public void setOriginalID(int originalID) {
		this.originalID = originalID;
	}

	public int getBridgeStatus() {
		return bridgeStatus;
	}

	public void setBridgeStatus(int bridgeStatus) {
		this.bridgeStatus = bridgeStatus;
	}

	public WeatherCondition getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(WeatherCondition weatherInfo) {
		this.weatherInfo = weatherInfo;
	}

	public SituationInfo getSituationInfo() {
		return situationInfo;
	}

	public void setSituationInfo(SituationInfo situationInfo) {
		this.situationInfo = situationInfo;
	}

	public double getEcoValue() {
		return ecoValue;
	}

	public void setEcoValue(double ecoValue) {
		this.ecoValue = ecoValue;
	}

	public double getCongestion() {
		return congestion;
	}

	public void setCongestion(double congestion) {
		this.congestion = congestion;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public RouteSegment getRouteSegment() {
		return routeSegment;
	}

	public void setRouteSegment(RouteSegment routeSegment) {
		this.routeSegment = routeSegment;
	}

}
