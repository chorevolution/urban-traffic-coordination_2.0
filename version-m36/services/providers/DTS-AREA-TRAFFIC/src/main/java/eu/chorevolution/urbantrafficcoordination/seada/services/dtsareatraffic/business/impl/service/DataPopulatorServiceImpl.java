package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.geojson.Feature;
import org.geojson.FeatureCollection;
import org.geojson.LineString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.DataPopulatorService;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.repositories.RouteSegmentRepository;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

@Service
public class DataPopulatorServiceImpl implements DataPopulatorService {

	private static Logger logger = LoggerFactory.getLogger(DataPopulatorServiceImpl.class);

	@Autowired
	private RouteSegmentRepository routeSegmentRepository;

	@Value(value = "classpath:osm-data/OSM-Goteborg-Highways.geojson")
	private Resource OSM_Goteborg_Highways;

	@Value(value = "classpath:osm-data/OSM-Goteborg-Highways-maxspeed.geojson")
	private Resource OSM_Goteborg_Highways_maxspeed;

	@Override
	public void populateData(String dataPopulationMode) {

		List<RouteSegment> routeSegments = null;
		switch (dataPopulationMode) {
		case "ALL_HIGHWAYS":
			routeSegments = getRouteSegmentsFromLineStringFeatures(OSM_Goteborg_Highways);
			break;
		case "ONLY_HIGHWAYS_WITH_MAX_SPEED":
			routeSegments = getRouteSegmentsFromLineStringFeatures(OSM_Goteborg_Highways_maxspeed);
			break;
		default:
			throw new BusinessException("Illegal argument value for data population mode");
		}
		routeSegmentRepository.save(routeSegments);

		logger.info(routeSegments.size() + " route segments added into the DB");
	}

	private List<RouteSegment> getRouteSegmentsFromLineStringFeatures(Resource resource) {

		List<RouteSegment> routeSegments = new ArrayList<>();
		FeatureCollection featureCollection = null;
		try {
			featureCollection = new ObjectMapper().readValue(resource.getInputStream(),
					FeatureCollection.class);
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		for (Feature feature : featureCollection) {
			if (feature.getGeometry() instanceof LineString) {
				RouteSegment routeSegment = new RouteSegment();
				Waypoint origin = new Waypoint();
				origin.setLat(
						((LineString) feature.getGeometry()).getCoordinates().get(0).getLatitude());
				origin.setLon(((LineString) feature.getGeometry()).getCoordinates().get(0)
						.getLongitude());
				routeSegment.setOrigin(origin);
				Waypoint destination = new Waypoint();
				destination.setLat(((LineString) feature.getGeometry()).getCoordinates()
						.get(((LineString) feature.getGeometry()).getCoordinates().size() - 1)
						.getLatitude());
				destination.setLon(((LineString) feature.getGeometry()).getCoordinates()
						.get(((LineString) feature.getGeometry()).getCoordinates().size() - 1)
						.getLongitude());
				routeSegment.setDestination(destination);
				routeSegments.add(routeSegment);
			}
		}

		logger.info(routeSegments.size()
				+ " route segments created from features with LineString geometry type");

		return routeSegments;
	}
}
