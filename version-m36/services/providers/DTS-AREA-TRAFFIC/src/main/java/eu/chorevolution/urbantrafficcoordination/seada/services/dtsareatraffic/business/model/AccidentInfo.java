
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "accidents_info")
public class AccidentInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2397283607646792922L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "accident_id")
	private Long id;

	@Column(name = "longitude", nullable = true)
	private double longitude;

	@Column(name = "latitude", nullable = true)
	private double latitude;

	@Column(name = "message_code_value", nullable = true, length = 255)
	private String messageCodeValue;

	@Column(name = "message_type_value", nullable = true, length = 255)
	private String messageTypeValue;

	@Column(name = "severity_code", nullable = true)
	private int severityCode;

	@Column(name = "description", nullable = true, length = 2000)
	private String description;

	@JoinColumn(name = "situation_id")
	@ManyToOne(optional = false)
	private SituationInfo situationInfo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getMessageCodeValue() {
		return messageCodeValue;
	}

	public void setMessageCodeValue(String messageCodeValue) {
		this.messageCodeValue = messageCodeValue;
	}

	public String getMessageTypeValue() {
		return messageTypeValue;
	}

	public void setMessageTypeValue(String messageTypeValue) {
		this.messageTypeValue = messageTypeValue;
	}

	public int getSeverityCode() {
		return severityCode;
	}

	public void setSeverityCode(int severityCode) {
		this.severityCode = severityCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SituationInfo getSituationInfo() {
		return situationInfo;
	}

	public void setSituationInfo(SituationInfo situationInfo) {
		this.situationInfo = situationInfo;
	}

}
