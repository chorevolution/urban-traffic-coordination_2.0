package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class TrafficRouteSegmentsInformationResponseWrapper {

	TrafficRouteSegmentsInformationResponse trafficRouteSegmentsInformationResponse;

	public TrafficRouteSegmentsInformationResponse getTrafficRouteSegmentsInformationResponse() {
		return trafficRouteSegmentsInformationResponse;
	}

	public void setTrafficRouteSegmentsInformationResponse(
			TrafficRouteSegmentsInformationResponse trafficRouteSegmentsInformationResponse) {
		this.trafficRouteSegmentsInformationResponse = trafficRouteSegmentsInformationResponse;
	}

}