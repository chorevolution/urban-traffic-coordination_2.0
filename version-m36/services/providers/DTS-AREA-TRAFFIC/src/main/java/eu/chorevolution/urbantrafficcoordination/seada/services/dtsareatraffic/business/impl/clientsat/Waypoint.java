
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class Waypoint {

	private double lat;
	private double lon;

	public double getLat() {
		return lat;
	}

	public void setLat(double value) {
		this.lat = value;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double value) {
		this.lon = value;
	}

}
