package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business;

import java.util.List;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

public interface DTSAreaTrafficService {

	List<RouteSegmentTrafficInfo> getTrafficRouteInformation(List<RouteSegment> routeSegments)
			throws BusinessException;

	void setTrafficAreaInformation(List<RouteSegmentTrafficInfo> routeSegmentsInfo)
			throws BusinessException;

	List<RouteSegment> getAreaSegments(Waypoint origin, Waypoint destination)
			throws BusinessException;
}
