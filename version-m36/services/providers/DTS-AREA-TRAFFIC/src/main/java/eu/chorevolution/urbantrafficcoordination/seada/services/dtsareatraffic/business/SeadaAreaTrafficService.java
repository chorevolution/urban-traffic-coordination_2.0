package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business;

import java.util.List;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;

public interface SeadaAreaTrafficService {

	public List<RouteSegment> getRouteSegmentsWithRouteSegmentsInfoFromSeadaAreaTraffic(
			List<RouteSegment> routeSegments);

}
