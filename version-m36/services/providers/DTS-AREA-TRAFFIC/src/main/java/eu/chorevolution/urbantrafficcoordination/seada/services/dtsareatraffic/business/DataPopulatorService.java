package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business;

public interface DataPopulatorService {

	public void populateData(String dataPopulationMode);

}
