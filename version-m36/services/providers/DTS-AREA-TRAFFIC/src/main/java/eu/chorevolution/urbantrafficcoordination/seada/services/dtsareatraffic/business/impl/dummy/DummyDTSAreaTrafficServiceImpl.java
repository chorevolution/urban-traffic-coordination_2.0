package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.dummy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.DTSAreaTrafficService;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

@Service
public class DummyDTSAreaTrafficServiceImpl implements DTSAreaTrafficService {

	@Override
	public List<RouteSegmentTrafficInfo>
			getTrafficRouteInformation(List<RouteSegment> routeSegments) throws BusinessException {

		return new ArrayList<>();
	}

	@Override
	public void setTrafficAreaInformation(List<RouteSegmentTrafficInfo> routeSegmentsInfo)
			throws BusinessException {

	}

	@Override
	public List<RouteSegment> getAreaSegments(Waypoint origin, Waypoint destination)
			throws BusinessException {

		return new ArrayList<>();
	}

}
