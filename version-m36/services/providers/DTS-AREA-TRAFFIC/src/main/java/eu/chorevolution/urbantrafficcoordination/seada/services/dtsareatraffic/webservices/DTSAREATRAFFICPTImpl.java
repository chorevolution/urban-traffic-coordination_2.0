package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.webservices;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AreaSegmentsRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AreaSegmentsResponse;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.AreaTrafficInformation;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.DTSAREATRAFFICPT;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.TrafficRouteInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.DTSAreaTrafficService;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;

@Component(value = "DTSAREATRAFFICPTImpl")
public class DTSAREATRAFFICPTImpl implements DTSAREATRAFFICPT {

	private static Logger logger = LoggerFactory.getLogger(DTSAREATRAFFICPTImpl.class);

	@Autowired
	private DTSAreaTrafficService service;

	@Override
	public TrafficRouteInformationResponse
			getTrafficRouteInformation(TrafficRouteInformationRequest parameters) {

		logger.info("CALLED getTrafficRouteInformation ON DTS-AREA-TRAFFIC");

		TrafficRouteInformationResponse trafficRouteInformationResponse =
				new TrafficRouteInformationResponse();
		List<RouteSegment> routeSegments = ConvertionUtility
				.getRouteSegmentsFromWS2Model(parameters.getTrafficRoute().getRouteSegments());
		List<eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.RouteSegmentInfo> routeSegmentInfoWS =
				ConvertionUtility.getRouteSegmentsInfoFromModel2WS(
						service.getTrafficRouteInformation(routeSegments));

		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setOrigin(parameters.getTrafficRoute().getOrigin());
		routeInfo.setDestination(parameters.getTrafficRoute().getDestination());
		routeInfo.setId(parameters.getTrafficRoute().getId());
		routeInfo.setProvider(parameters.getTrafficRoute().getProvider());
		routeInfo.setRoutePolyline(parameters.getTrafficRoute().getRoutePolyline());
		routeInfo.getRouteSegmentInfo().addAll(routeSegmentInfoWS);
		trafficRouteInformationResponse.setTrafficRouteInfo(routeInfo);

		return trafficRouteInformationResponse;
	}

	@Override
	public void setTrafficAreaInformation(AreaTrafficInformation parameters) {

		logger.info("CALLED setTrafficAreaInformation ON DTS-AREA-TRAFFIC");

		List<RouteSegmentTrafficInfo> routeSegmentsInfo = ConvertionUtility
				.getRouteSegmentsTrafficInfoFromWS2Model(parameters.getAreaSegmentsTrafficInfo());
		service.setTrafficAreaInformation(routeSegmentsInfo);
	}

	@Override
	public AreaSegmentsResponse getAreaSegments(AreaSegmentsRequest parameters) {

		logger.info("CALLED getAreaSegments ON DTS-AREA-TRAFFIC");

		List<RouteSegment> routeSegments = service.getAreaSegments(
				ConvertionUtility.getWaypointFromWS2Model(parameters.getOrigin()),
				ConvertionUtility.getWaypointFromWS2Model(parameters.getDestination()));
		AreaSegmentsResponse result = new AreaSegmentsResponse();
		result.getAreaSegments()
				.addAll(ConvertionUtility.getRouteSegmentsPointsFromModel2WS(routeSegments));
		return result;
	}

}
