package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class TrafficRouteSegmentsInformationRequestWrapper {

	TrafficRouteSegmentsInformationRequest trafficRouteSegmentsInformationRequest;

	public TrafficRouteSegmentsInformationRequest getTrafficRouteSegmentsInformationRequest() {
		return trafficRouteSegmentsInformationRequest;
	}

	public void setTrafficRouteSegmentsInformationRequest(
			TrafficRouteSegmentsInformationRequest trafficRouteSegmentsInformationRequest) {
		this.trafficRouteSegmentsInformationRequest = trafficRouteSegmentsInformationRequest;
	}

}
