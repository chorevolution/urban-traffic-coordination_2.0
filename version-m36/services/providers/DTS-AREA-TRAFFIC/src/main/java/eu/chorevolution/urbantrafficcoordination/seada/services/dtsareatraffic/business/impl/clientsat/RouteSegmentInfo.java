
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class RouteSegmentInfo {

	private int id;
	private RouteSegment routeSegment;
	private int bridgeStatus;
	private WeatherCondition weatherInfo;
	private SituationInfo situationInfo;
	private double ecoValue;
	private double congestion;
	private double speed;
	private String updatedAt;
	private String type;

	public int getId() {
		return id;
	}

	public void setId(int value) {
		this.id = value;
	}

	public RouteSegment getRouteSegment() {
		return routeSegment;
	}

	public void setRouteSegment(RouteSegment value) {
		this.routeSegment = value;
	}

	public int getBridgeStatus() {
		return bridgeStatus;
	}

	public void setBridgeStatus(int value) {
		this.bridgeStatus = value;
	}

	public WeatherCondition getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(WeatherCondition value) {
		this.weatherInfo = value;
	}

	public SituationInfo getSituationInfo() {
		return situationInfo;
	}

	public void setSituationInfo(SituationInfo value) {
		this.situationInfo = value;
	}

	public double getEcoValue() {
		return ecoValue;
	}

	public void setEcoValue(double value) {
		this.ecoValue = value;
	}

	public double getCongestion() {
		return congestion;
	}

	public void setCongestion(double value) {
		this.congestion = value;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double value) {
		this.speed = value;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String value) {
		this.updatedAt = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		this.type = value;
	}

}
