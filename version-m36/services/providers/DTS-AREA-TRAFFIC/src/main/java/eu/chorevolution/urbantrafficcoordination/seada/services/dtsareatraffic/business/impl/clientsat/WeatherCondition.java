
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class WeatherCondition {

	protected double roadTemperature;
	protected double airTemperature;
	protected double airRelativeHumidity;
	protected double windForce;

	public double getRoadTemperature() {
		return roadTemperature;
	}

	public void setRoadTemperature(double value) {
		this.roadTemperature = value;
	}

	public double getAirTemperature() {
		return airTemperature;
	}

	public void setAirTemperature(double value) {
		this.airTemperature = value;
	}

	public double getAirRelativeHumidity() {
		return airRelativeHumidity;
	}

	public void setAirRelativeHumidity(double value) {
		this.airRelativeHumidity = value;
	}

	public double getWindForce() {
		return windForce;
	}

	public void setWindForce(double value) {
		this.windForce = value;
	}

}
