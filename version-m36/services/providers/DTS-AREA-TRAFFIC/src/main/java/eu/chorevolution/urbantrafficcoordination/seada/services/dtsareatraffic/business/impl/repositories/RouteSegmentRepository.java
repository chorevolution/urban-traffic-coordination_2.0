package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

public interface RouteSegmentRepository extends CrudRepository<RouteSegment, Long> {

	List<RouteSegment> findByOriginAndDestination(Waypoint origin, Waypoint destination);

	@Query("select rs from #{#entityName} rs where rs.origin in ?#{#origins} and rs.destination in ?#{#destinations}")
	List<RouteSegment> findByOriginsAndDestinations(@Param("origins") List<Waypoint> origins,
			@Param("destinations") List<Waypoint> destinations);

	@Query("select rs.routeSegmentInfo from #{#entityName} rs where rs.origin in ?#{#origins} and rs.destination in ?#{#destinations}")
	List<RouteSegmentTrafficInfo> findRouteSegmentsInfoByOriginsAndDestinations(
			@Param("origins") List<Waypoint> origins,
			@Param("destinations") List<Waypoint> destinations);

	@Query("select rs from #{#entityName} rs where rs.origin.lon >= ?#{#origin.lon} and rs.origin.lon <= ?#{#destination.lon} and rs.destination.lon >= ?#{#origin.lon} and rs.destination.lon <= ?#{#destination.lon} and rs.origin.lat >= ?#{#destination.lat} and rs.origin.lat <= ?#{#origin.lat} and rs.destination.lat >= ?#{#destination.lat} and rs.destination.lat <= ?#{#origin.lat}")
	List<RouteSegment> findByArea(@Param("origin") Waypoint origin,
			@Param("destination") Waypoint destination);

	@Query("select rs from #{#entityName} rs where sqrt(power(abs(?#{#center.lon} - rs.origin.lon),2) + power(abs(?#{#center.lat} - rs.origin.lat),2)) <= ?#{#radius} and sqrt(power(abs(?#{#center.lon} - rs.destination.lon),2) + power(abs(?#{#center.lat} - rs.destination.lat),2)) <= ?#{#radius}")
	List<RouteSegment> findByArea(@Param("center") Waypoint center, @Param("radius") double radius);

}
