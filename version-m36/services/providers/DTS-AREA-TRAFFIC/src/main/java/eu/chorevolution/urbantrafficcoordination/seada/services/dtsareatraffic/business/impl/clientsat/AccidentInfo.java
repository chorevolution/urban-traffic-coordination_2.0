
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

public class AccidentInfo {

	private double longitude;
	private double latitude;
	private String messageCodeValue;
	private String messageTypeValue;
	private int severityCode;
	private String description;

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double value) {
		this.longitude = value;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double value) {
		this.latitude = value;
	}

	public String getMessageCodeValue() {
		return messageCodeValue;
	}

	public void setMessageCodeValue(String value) {
		this.messageCodeValue = value;
	}

	public String getMessageTypeValue() {
		return messageTypeValue;
	}

	public void setMessageTypeValue(String value) {
		this.messageTypeValue = value;
	}

	public int getSeverityCode() {
		return severityCode;
	}

	public void setSeverityCode(int value) {
		this.severityCode = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String value) {
		this.description = value;
	}

}
