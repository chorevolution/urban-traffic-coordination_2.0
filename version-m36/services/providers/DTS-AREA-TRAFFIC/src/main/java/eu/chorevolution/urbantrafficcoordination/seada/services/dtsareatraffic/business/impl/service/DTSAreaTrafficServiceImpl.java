package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.DTSAreaTrafficService;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.repositories.RouteSegmentRepository;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegment;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.RouteSegmentTrafficInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model.Waypoint;

@Service
@Transactional
public class DTSAreaTrafficServiceImpl implements DTSAreaTrafficService {

	private static Logger logger = LoggerFactory.getLogger(DTSAreaTrafficServiceImpl.class);

	@Autowired
	private RouteSegmentRepository routeSegmentRepository;

	@Autowired
	private SeadaAreaTrafficServiceImpl seadaAreaTrafficService;

	@Override
	public List<RouteSegmentTrafficInfo>
			getTrafficRouteInformation(List<RouteSegment> routeSegments) throws BusinessException {

		List<Waypoint> origins = new ArrayList<>();
		List<Waypoint> destinations = new ArrayList<>();
		// get the origins and destinations of routeSegments
		for (RouteSegment routeSegment : routeSegments) {
			origins.add(routeSegment.getOrigin());
			destinations.add(routeSegment.getDestination());
		}
		// find the routeSegmentsInfo of routeSegments
		List<RouteSegmentTrafficInfo> routeSegmentsInfo = routeSegmentRepository
				.findRouteSegmentsInfoByOriginsAndDestinations(origins, destinations);

		logger.info(routeSegmentsInfo.size() + " route segments found into the DB");

		// get the list of route segments not found
		List<RouteSegment> routeSegmentsNotFound = new ArrayList<>();
				
		for (RouteSegment routeSegment : routeSegments) {
			boolean found = false;
			for (RouteSegmentTrafficInfo routeSegmentInfo : routeSegmentsInfo) {
				if (routeSegmentInfo.getRouteSegment().getOrigin().getLat() == routeSegment
						.getOrigin().getLat()
						&& routeSegmentInfo.getRouteSegment().getOrigin().getLon() == routeSegment
								.getOrigin().getLon()
						&& routeSegmentInfo.getRouteSegment().getDestination()
								.getLat() == routeSegment.getDestination().getLat()
						&& routeSegmentInfo.getRouteSegment().getDestination()
								.getLon() == routeSegment.getDestination().getLon()
						&& !found) {
					found = true;
				}
			}
			if (!found) {
				routeSegmentsNotFound.add(routeSegment);
			}
		}
		// check if there are route segments not found
		if (routeSegmentsNotFound.size() > 0) {

			logger.info(routeSegmentsNotFound.size() + " route segments out of "
					+ routeSegments.size() + " not found into the DB");

			// there are route segments not found
			// get the route segments not found with the related route segment from
			// seadaAreaTraffic
			List<RouteSegment> routeSegmentsNotFoundWithTrafficInfo = seadaAreaTrafficService
					.getRouteSegmentsWithRouteSegmentsInfoFromSeadaAreaTraffic(
							routeSegmentsNotFound);
			// save the route segments
			routeSegmentRepository.save(routeSegmentsNotFoundWithTrafficInfo);
			// add the route segment info related to the route segments not found to
			// routeSegmentsInfo
			for (RouteSegment routeSegment : routeSegmentsNotFoundWithTrafficInfo) {
				routeSegmentsInfo.add(routeSegment.getRouteSegmentInfo());
			}
		}

		return routeSegmentsInfo;
	}

	@Override
	public void setTrafficAreaInformation(List<RouteSegmentTrafficInfo> routeSegmentsInfo)
			throws BusinessException {

		List<Waypoint> origins = new ArrayList<>();
		List<Waypoint> destinations = new ArrayList<>();
		// get the origins and destinations of routeSegments
		for (RouteSegmentTrafficInfo routeSegmentInfo : routeSegmentsInfo) {
			origins.add(routeSegmentInfo.getRouteSegment().getOrigin());
			destinations.add(routeSegmentInfo.getRouteSegment().getDestination());
		}
		// find the routeSegments of routeSegmentsInfo
		List<RouteSegment> routeSegments =
				routeSegmentRepository.findByOriginsAndDestinations(origins, destinations);
		// iterate over routeSegmentsInfo to find the related route segment
		for (RouteSegmentTrafficInfo routeSegmentInfo : routeSegmentsInfo) {
			for (RouteSegment routeSegment : routeSegments) {
				if (routeSegmentInfo.getRouteSegment().getOrigin().getLat() == routeSegment
						.getOrigin().getLat()
						&& routeSegmentInfo.getRouteSegment().getOrigin().getLon() == routeSegment
								.getOrigin().getLon()
						&& routeSegmentInfo.getRouteSegment().getDestination()
								.getLat() == routeSegment.getDestination().getLat()
						&& routeSegmentInfo.getRouteSegment().getDestination()
								.getLon() == routeSegment.getDestination().getLon()) {
					// set routeSegment in routeSegmentInfo
					routeSegmentInfo.setRouteSegment(routeSegment);
					// set routeSegmentInfo in routeSegmentInfo
					routeSegment.setRouteSegmentInfo(routeSegmentInfo);
					// update routeSegment
					routeSegmentRepository.save(routeSegment);
					// break the cycle since the route segment has been found
					break;
				}
			}
		}

		logger.info(routeSegmentsInfo.size() + " route segments info inserted into the DB");
	}

	@Override
	public List<RouteSegment> getAreaSegments(Waypoint origin, Waypoint destination)
			throws BusinessException {

		// The following lines of code retrieve the list of route segments inside a
		// rectangular area identified by using origin and destination

		// Map<String, Waypoint> convertedWaypoints =
		// ConvertionUtility.convertOriginAndDestination(origin, destination);
		// origin = convertedWaypoints.get("origin");
		// destination = convertedWaypoints.get("destination");
		// List<RouteSegment> routeSegments = routeSegmentRepository.findByArea(origin,
		// destination);

		// The following lines of code retrieve the list of route segments inside a
		// circular area identified by using origin and destination
		Waypoint center = new Waypoint();
		center.setLon((origin.getLon() + destination.getLon()) / 2);
		center.setLat((origin.getLat() + destination.getLat()) / 2);
		double radius = Math.sqrt(Math.pow(Math.abs(destination.getLon() - origin.getLon()), 2)
				+ Math.pow(Math.abs(destination.getLat() - origin.getLat()), 2)) / 2;
		List<RouteSegment> routeSegments = routeSegmentRepository.findByArea(center, radius);

		logger.info(routeSegments.size() + " route segments found into the DB");

		return routeSegments;
		
	}


}
