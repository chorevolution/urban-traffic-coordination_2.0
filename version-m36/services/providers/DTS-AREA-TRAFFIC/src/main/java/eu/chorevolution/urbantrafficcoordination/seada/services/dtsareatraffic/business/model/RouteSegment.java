package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "route_segments")
public class RouteSegment implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6910993966589938550L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "route_segment_id")
	private Long id;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "lat", column = @Column(name = "origin_latitude")),
			@AttributeOverride(name = "lon", column = @Column(name = "origin_longitude")) })
	private Waypoint origin;
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "lat", column = @Column(name = "destination_latitude")),
			@AttributeOverride(name = "lon", column = @Column(name = "destination_longitude")) })
	private Waypoint destination;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "route_segment_info_id", nullable = true)
	private RouteSegmentTrafficInfo routeSegmentInfo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Waypoint getOrigin() {
		return origin;
	}

	public void setOrigin(Waypoint origin) {
		this.origin = origin;
	}

	public Waypoint getDestination() {
		return destination;
	}

	public void setDestination(Waypoint destination) {
		this.destination = destination;
	}

	public RouteSegmentTrafficInfo getRouteSegmentInfo() {
		return routeSegmentInfo;
	}

	public void setRouteSegmentInfo(RouteSegmentTrafficInfo routeSegmentInfo) {
		this.routeSegmentInfo = routeSegmentInfo;
	}
}
