
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

import java.util.ArrayList;
import java.util.List;

public class TrafficRouteSegmentsInformationResponse {

	private List<RouteSegmentInfo> routeSegmentInfo;

	public List<RouteSegmentInfo> getRouteSegmentInfo() {
		if (routeSegmentInfo == null) {
			routeSegmentInfo = new ArrayList<RouteSegmentInfo>();
		}
		return this.routeSegmentInfo;
	}

}
