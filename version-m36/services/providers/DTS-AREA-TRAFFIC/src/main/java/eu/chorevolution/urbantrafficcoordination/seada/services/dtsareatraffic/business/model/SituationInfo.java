
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "situations_info")
public class SituationInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 864314211193112670L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "situation_id")
	private Long id;

	@Column(name = "situation_summary_info", nullable = true, length = 2000)
	private String situationSummaryInfo;

	@Column(name = "type", nullable = true, length = 255)
	private String type;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER, mappedBy = "situationInfo")
	private Set<AccidentInfo> accidents = new HashSet<>();

	@OneToOne(mappedBy = "situationInfo")
	private RouteSegmentTrafficInfo routeSegmentInfo;

	public RouteSegmentTrafficInfo getRouteSegmentInfo() {
		return routeSegmentInfo;
	}

	public void setRouteSegmentInfo(RouteSegmentTrafficInfo routeSegmentInfo) {
		this.routeSegmentInfo = routeSegmentInfo;
	}

	public Set<AccidentInfo> getAccidents() {
		return accidents;
	}

	public void setAccidents(Set<AccidentInfo> accidents) {
		this.accidents = accidents;
	}

	public String getSituationSummaryInfo() {
		return situationSummaryInfo;
	}

	public void setSituationSummaryInfo(String situationSummaryInfo) {
		this.situationSummaryInfo = situationSummaryInfo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void addAccident(AccidentInfo accidentInfo) {
		accidentInfo.setSituationInfo(this);
		this.getAccidents().add(accidentInfo);
	}

}
