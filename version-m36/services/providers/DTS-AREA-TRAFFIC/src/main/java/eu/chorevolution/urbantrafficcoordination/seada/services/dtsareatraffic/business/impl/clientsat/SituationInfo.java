
package eu.chorevolution.urbantrafficcoordination.seada.services.dtsareatraffic.business.impl.clientsat;

import java.util.ArrayList;
import java.util.List;

public class SituationInfo {

	private List<AccidentInfo> accidents;
	private String situationSummaryInfo;
	private String type;

	public List<AccidentInfo> getAccidents() {
		if (accidents == null) {
			accidents = new ArrayList<AccidentInfo>();
		}
		return this.accidents;
	}

	public String getSituationSummaryInfo() {
		return situationSummaryInfo;
	}

	public void setSituationSummaryInfo(String value) {
		this.situationSummaryInfo = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String value) {
		this.type = value;
	}

}
