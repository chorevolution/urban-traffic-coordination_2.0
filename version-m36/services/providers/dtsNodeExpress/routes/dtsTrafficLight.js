var express = require('express');
var router = express.Router();
var request = require('request');
var fs = require('fs');

/* POST users listing. */


/**
 * Returns the distance between two LatLng objects in meters.
 *
 * Slightly modified version of the harversine formula used in Leaflet:
 *  https://github.com/Leaflet/Leaflet
 */
function distance(p1, p2) {
    // convert degrees to radians
    const EARTH_RADIUS = 6378137;
    const DEG_TO_RAD_FACTOR = Math.PI / 180;
    var lat1 = p1.lat * DEG_TO_RAD_FACTOR;
    var lat2 = p2.lat * DEG_TO_RAD_FACTOR;
    var a = Math.sin(lat1) * Math.sin(lat2) +
        Math.cos(lat1) * Math.cos(lat2) * Math.cos((p2.lon - p1.lon) * DEG_TO_RAD_FACTOR);

    return parseFloat(EARTH_RADIUS * Math.acos(Math.min(a, 1)));
};

function distance2(p1, p2) {
    var p = 0.017453292519943295; // Math.PI / 180
    var c = Math.cos;
    var a = 0.5 - c((p2.lat - p1.lat) * p) / 2 +
        c(p1.lat * p) * c(p2.lat * p) *
        (1 - c((p2.lon - p1.lon) * p)) / 2;

    return 12756274 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6378137 m
};

router.post('/', function (req, res, next) {
    // Example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
    // req.body.
    // var myfunc = function () {
    //     alert('It works!');
    // }
    // var as_string = myfunc.toString();
    // as_string = as_string.replace('It works', 'It really works');
    // var as_func = eval('(' + as_string + ')');
    // as_func();

    var segment = req.body.segmentInformationRequest;
    var trafficLightArray = JSON.parse(fs.readFileSync('routes/trafficLightsGoteborg.json', 'utf8'));
    var minDist = 1000;
    var distance2TrafficLight;
    var trafficLightCoords = {};
    trafficLightArray.forEach(function (trafficLightItem) {
        distance2TrafficLight = distance2(segment.waypoint1, trafficLightItem);
        if (minDist > distance2TrafficLight) {
            trafficLightCoords = trafficLightItem;
            minDist = distance2TrafficLight;
        }
    });

    var funcString = "";
    var redIntervals = [], greenIntervals= [], yellowIntervals = [];
    var trafficLightStatus = "noTrafficLight";

    if (minDist < 1000 ) {
        var startCount = new Date().getTime();
        var redInterval =  Math.round(((parseInt(trafficLightCoords.id) % 100)*10)*20) + 5000;
        var greenInterval = Math.round(((parseInt(trafficLightCoords.id) % 100)*10)*18) + 6000;
        var yellowInterval = 2000;
        var trafficLightCycle = redInterval+redInterval+yellowInterval;
        startCount -= startCount % trafficLightCycle;
        var trafficLightFunc = function () {
            // Here is how to call this function
            // var getTrafficLight = eval('(' + trafficLightFunc + ')');
            // getTrafficLight();
            var currentTime = new Date();
            var currentDayInMillis = currentTime.getTime();
            var thisCounter = startCount;
            var interval1 = redInterval;
            var interval2 = yellowInterval;
            var fullCycle = trafficLightCycle;

            var timeInLightCycle = (currentDayInMillis - thisCounter) % fullCycle;
            if (timeInLightCycle < interval1) {
                trafficLightStatus = "red";
            } else if (timeInLightCycle < interval1 + interval2) {
                trafficLightStatus = "yellow";
            } else trafficLightStatus = "green";
            return trafficLightStatus;
        };
        funcString = trafficLightFunc.toString()
        .replace('startCount', startCount.toString())
        .replace('redInterval', redInterval.toString())
        .replace('yellowInterval', yellowInterval.toString())
        .replace('trafficLightCycle', trafficLightCycle.toString());
        if (minDist < 5) {
            var trafficLightStatus = trafficLightFunc();
            
            for (i = 0; i<30; i++) {
                var startCycle = startCount + i * trafficLightCycle;
                redIntervals.push({
                    start: startCycle + 1,
                    end: startCycle + redInterval
                });
                yellowIntervals.push({
                    start: startCycle + redInterval + 1,
                    end: startCycle + redInterval + yellowInterval
                });
                greenIntervals.push({
                    start: startCycle + redInterval + yellowInterval + 1,
                    end: startCycle + trafficLightCycle
                });
            }
        
        }
    }

    
    res.send({
        trafficLightInformationResponse: {
        trafficLightInfo: {
            trafficLightStatus: trafficLightStatus, //2: no traffic light, 1: green traffic light, 0: red traffic light
            redIntervals: redIntervals,
            yellowIntervals: yellowIntervals,
            greenIntervals: greenIntervals,
            trafficLight: {
                id: trafficLightCoords.id,
                lat: trafficLightCoords.lat,
                lon: trafficLightCoords.lon
            },
            distance: Math.round(minDist),
            trafficLightFunction: funcString
        },
        waypoint0: segment.waypoint0,
        waypoint1: segment.waypoint1,
        type: "trafficLightInfo"
    }
    });
});
module.exports = router;