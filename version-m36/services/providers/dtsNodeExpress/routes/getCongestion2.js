var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');
var https = require("https");
/* POST users listing. */

var async = require("async");
var xml2js = require("xml2js");


//request example 
/*
{ 
    "congestionStatusRequest":
    [{"start": { "lon": "57.68611907959",
                                "lat": "11.93115234375"},
                     "end": { "lon": "11.93115234375",
                              "lat": "57.68611907959"}  
                    },
    {"start": { "lon": "57.736198",
                               "lat": "11.972487"},
                    "end": { "lon": "57.748813",
                             "lat": "11.980341"}    
    }]
}
*/
/*response example
{
  "congestionStatusResponse": [
    {
      "start": {
        "lon": "57.68611907959",
        "lat": "11.93115234375"
      },
      "end": {
        "lon": "11.93115234375",
        "lat": "57.68611907959"
      },
      "congestionLevel": -100,
      "speed": -100
    },
    {
      "start": {
        "lon": "57.736198",
        "lat": "11.972487"
      },
      "end": {
        "lon": "57.748813",
        "lat": "11.980341"
      },
      "speed": 30,
      "congestionLevel": 2
    }
  ]
}
*/

// BIG CONFUSION: Lat should be 57 not 11. Tomtom get in point in order lat lon, i.e. 57 11

router.post('/', function (req, res, next) {
    //console.log(req);
    //console.log(req.body);
    var segment = req.body.segmentInformationRequest; //check if it is an array
    //jsontext='[{"routeSegment" : {"start": {"lat": "57.714659", "lon": " 11.966673"}, "end": {"lat": "57.714659", "lon": " 11.966673"}}}, {"routeSegment": {"start": { "lat": "57.736198", "lon": "11.972487"}, "end": { "lat": "57.748813", "lon": "11.980341"}}}]';
    //var segments = JSON.parse(jsontext);

    var urls = [];
    var TOMTOM_URL_CONST = 'https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/xml?key=v9byd7y3x9dwgjhzyg36s4pa&point=';
    var UNIT_CONST = '&unit=KMPH';
    urls.push(TOMTOM_URL_CONST + segment.waypoint0.lat + ',' + segment.waypoint0.lon + UNIT_CONST);
    urls.push(TOMTOM_URL_CONST + segment.waypoint1.lat + ',' + segment.waypoint1.lon + UNIT_CONST);
    var results = [];
    var calls = [];
    var resultSegments = [];
    var parser = new xml2js.Parser();

    urls.forEach(function (element) {
        calls.push(function (callback) {
            https.get(element, function (resource) {
                resource.setEncoding('binary');
                resource.on('data', function (data) {
                    parser.parseString(data, function (err, result) {
                        results[element] = result;
                        //console.log(results);
                    });
                    callback();
                });
            });
        });
    });

    async.series(calls, function (err) {
        //console.log(results.length);
        var startURL = TOMTOM_URL_CONST + segment.waypoint0.lat + ',' + segment.waypoint0.lon + UNIT_CONST;
        var endURL = TOMTOM_URL_CONST + segment.waypoint1.lat + ',' + segment.waypoint1.lon + UNIT_CONST;
        resultSegment = {};
        resultSegment.segmentCongestionInformationResponse = {};
        if ((typeof results[startURL].flowSegmentData === 'undefined') || (typeof results[startURL].flowSegmentData === 'undefined')) {
            resultSegment.segmentCongestionInformationResponse.congestion = -100;
            resultSegment.segmentCongestionInformationResponse.speed = -100;
        } else {
            resultSegment.segmentCongestionInformationResponse.speed = Math.round((parseFloat(results[startURL].flowSegmentData.currentSpeed[0]) + parseFloat(results[endURL].flowSegmentData.currentSpeed[0])) / 2);
            if (resultSegment.segmentCongestionInformationResponse.speed < (parseFloat(results[startURL].flowSegmentData.freeFlowSpeed[0]) + parseFloat(results[endURL].flowSegmentData.freeFlowSpeed[0])) / 2 * 0.75) {
                resultSegment.segmentCongestionInformationResponse.congestion = 2;
            } else {
                resultSegment.segmentCongestionInformationResponse.congestion = 1;
            }
        }
        resultSegment.segmentCongestionInformationResponse.type = "congestion";
        resultSegment.segmentCongestionInformationResponse.waypoint0 = segment.waypoint0;
        resultSegment.segmentCongestionInformationResponse.waypoint1 = segment.waypoint1;
        res.send({
            segmentCongestionInformationResponse: resultSegment.segmentCongestionInformationResponse
        });
    });


});
module.exports = router;