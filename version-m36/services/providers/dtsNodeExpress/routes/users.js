var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.render('ecoRoute', {
    title: 'Ooops',
    header: 'ecoRoute Example with CHOReVOLUTION'
  });
});

module.exports = router;