var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */

router.post('/', function (req, res, next) {
    // takes bridgeId, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 

    request({
        url: 'http://data.goteborg.se/BridgeService/v1.0/GetGABOpenedStatus/152d47b0-754b-4718-a635-6a7db1374572?format=json',
        method: 'GET'
    }, function (error, response, body) {
        var isBridgeClosed = JSON.parse(response.body).Value;
        var currentTime = new Date();
        var nextOpenedTime;
        var nextClosureFrom;
        var nextClosureTo;

        if (isBridgeClosed) {
            nextOpenedTime = currentTime.getTime() + 7 * 60000; // we estimate that the bridge is closed for 7 minutes more in case that it is already closed
        } else {
            nextOpenedTime = currentTime.getTime(); //+1-1 is transformation to int
        }

        var currentHour = currentTime.getHours(); //hour of the day, 
        var currentMinute = currentTime.getMinutes(); //minute of the hour
        var closure1f = new Date();
        var closure2f = new Date();
        var closure1t = new Date();
        var closure2t = new Date();
        closure1f.setHours(9 - closure1f.getTimezoneOffset());
        closure1f.setMinutes(0);
        closure1f.setSeconds(0);
        closure1f.setMilliseconds(0);

        closure1t.setHours(9 - closure1t.getTimezoneOffset());
        closure1t.setMinutes(10);
        closure1t.setSeconds(0);
        closure1t.setMilliseconds(0);

        closure2f.setHours(18 - closure2f.getTimezoneOffset());
        closure2f.setHours(0);
        closure2f.setSeconds(0);
        closure2f.setMilliseconds(0);

        closure2t.setHours(18 - closure2t.getTimezoneOffset());
        closure2t.setHours(10);
        closure2t.setSeconds(0);
        closure2t.setMilliseconds(0);

        var currentDayInMillis = currentTime.getTime();
        if (currentDayInMillis > closure2t.getTime()) {
            nextClosureFrom = closure1f.getTime() + 86400000;
            nextClosureTo = closure1t.getTime() + 86400000; // add number of milliseconds of a day to get the next day
            isBridgeClosed = false;
        } else if (currentDayInMillis > closure1t.getTime()) {
            nextClosureFrom = closure2f.getTime();
            nextClosureTo = closure2t.getTime();
        } else if (currentDayInMillis < closure1f.getTime()) {
            nextClosureFrom = closure1f.getTime();
            nextClosureTo = closure1t.getTime();
        }
        
        res.send({
            bridgeStatusInformationResponse: {
                closureStatus: {
                    isClosed: isBridgeClosed,
                    opensAtTimeMillis: nextOpenedTime
                },
                nextClosure: {
                    fromMillis: nextClosureFrom,
                    toMillis: nextClosureTo
                }
            }
        });
    });
});
module.exports = router;