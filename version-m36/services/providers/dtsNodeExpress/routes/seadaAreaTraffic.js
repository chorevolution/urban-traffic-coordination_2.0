var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');
var async = require('async');

/* POST users listing. */

//var trafficURL = 'http://jinx.viktoria.chalmers.se:3000/';

var trafficURL = 'http://localhost:3000/';

router.post('/', function (req, res, next) {
	// taken in a list of routes with route segment information reponse with filled traffic information
	// send request to other dtsService if needed, otherwise response with MySQL DB data
	// var waypoint0=waypoint1={};
	// waypoint0.lat = req.body.trafficRouteInformationRequest.routes[0].routeSegments[0].waypoint0.lat;
	// waypoint0.lon = req.body.trafficRouteInformationRequest.routes[0].routeSegments[0].waypoint0.lon;
	// waypoint1.lat = req.body.trafficRouteInformationRequest.routes[0].routeSegments[0].waypoint1.lat;
	// waypoint1.lon = req.body.trafficRouteInformationRequest.routes[0].routeSegments[0].waypoint1.lon;
	if (typeof req.body.trafficRouteInformationRequest !== 'undefined') {
		var operation = "trafficRouteInformationRequest";
		var requestedRoutes = req.body.trafficRouteInformationRequest.routes;
	} else if (typeof req.body.trafficRouteSegmentsInformationRequest !== 'undefined') {
		var operation = "trafficRouteSegmentsInformationRequest";
		var requestedRoutes = [];
		var requestedrouteSegments = req.body.trafficRouteSegmentsInformationRequest;
		requestedRoutes.push({
			id: 0,
			provider: "routeSegmentRequest",
			origin: requestedrouteSegments.routeSegments[0].waypoint0,
			destination: requestedrouteSegments.routeSegments[requestedrouteSegments.routeSegments.length - 1].waypoint1,
			routePolyline: "",
			routeSegments: requestedrouteSegments.routeSegments
		});
	} else {
		var operation = "unknown";
	};
	console.dir(requestedRoutes);
	var reqBody = {};
	reqBody.segmentInformationRequest = {};
	reqBody.segmentInformationRequest.waypoint0 = {};
	reqBody.segmentInformationRequest.waypoint1 = {};
	var responseObject = {};
	responseObject.routesInfo = [];
	var numRunningQueries = 0;
	requestedRoutes.forEach(function (route, indexRoute) {
		responseObject.routesInfo[indexRoute] = {};
		responseObject.routesInfo[indexRoute].id = route.id;
		responseObject.routesInfo[indexRoute].provider = route.provider;
		responseObject.routesInfo[indexRoute].origin = route.origin;
		responseObject.routesInfo[indexRoute].destination = route.destination;
		responseObject.routesInfo[indexRoute].routePolyline = route.routePolyline;
		responseObject.routesInfo[indexRoute].routeSegmentInfo = [];
		route.routeSegments.forEach(function (routeSegment, indexRouteSegment) {
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment] = {};
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].routeSegment = routeSegment;
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].id = indexRoute * 1000 + indexRouteSegment;
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].ecoValue = -100;
			var d = new Date();
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].updatedAt = d.toString("dd\/MM\/yyyy HH:mm");
			responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].type = "aggregatedTrafficInfo";
			reqBody.segmentInformationRequest.waypoint0 = routeSegment.waypoint0;
			reqBody.segmentInformationRequest.waypoint1 = routeSegment.waypoint1;
			++numRunningQueries;
			request({
				url: trafficURL + 'waypointWeatherInformation',
				method: 'POST',
				//headers: ['"content-type":"application/json"'],
				json: true,
				body: reqBody,
			}, function (error, response, body) {
				--numRunningQueries;
				if (typeof body.segmentWeatherInformationResponse.weatherInfo !== 'undefined') {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].weatherInfo = body.segmentWeatherInformationResponse.weatherInfo;
					//console.log("route: ", indexRoute, "Segment: ", indexRouteSegment, "Temperature: ", responseObject.routes[indexRoute].routeSegments[indexRouteSegment].weatherInfo);
				} else {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].weatherInfo = "noWeatherInfo";
				};
				if (numRunningQueries === 0) {
					finish(res, responseObject);
				}
			});
			++numRunningQueries;
			request({
				url: trafficURL + 'routeCongestionInformation',
				method: 'POST',
				//headers: ['"content-type":"application/json"'],
				json: true,
				body: reqBody,
			}, function (error, response, body) {
				// console.log(body.segmentCongestionInformationResponse.speed);
				--numRunningQueries;
				if (typeof body.segmentCongestionInformationResponse.speed !== 'undefined') {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].speed = body.segmentCongestionInformationResponse.speed;
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].congestion = body.segmentCongestionInformationResponse.congestion;
				} else {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].speed = -100;
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].congestion = -100;
				};
				if (numRunningQueries === 0) {
					finish(res, responseObject);
				}
			});
			++numRunningQueries;
			request({
				url: trafficURL + 'routeSegmentBridgeStatusInformation',
				method: 'POST',
				//headers: ['"content-type":"application/json"'],
				json: true,
				body: reqBody,
			}, function (error, response, body) {
				// 0: Closed, 1: Opened, 2: No Bridge. Logic: 0 have to take alternative route, positive ok to take this route.
				--numRunningQueries;
				if (typeof body.segmentBridgeInformationResponse.bridgeStatus !== 'undefined') {
					if (body.segmentBridgeInformationResponse.type !== 'NoBridge') {
						responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].bridgeStatus = body.segmentBridgeInformationResponse.bridgeStatus ? 0 : 1;
					} else {
						responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].bridgeStatus = 2;
					}
				} else {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].bridgeStatus = -100;
				};
				if (numRunningQueries === 0) {
					finish(res, responseObject);
				}
			});
			++numRunningQueries;
			request({
				url: trafficURL + 'waypointAccidentInformation',
				method: 'POST',
				//headers: ['"content-type":"application/json"'],
				json: true,
				body: reqBody,
			}, function (error, response, body) {
				// console.log(body.segmentAccidentInformationResponse.situationInfo);
				--numRunningQueries;
				if (typeof body.segmentAccidentInformationResponse.situationInfo !== 'undefined') {
					//responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].situationInfo = body.segmentAccidentInformationResponse.situationInfo;
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].situationInfo = body.segmentAccidentInformationResponse.situationInfo;
				} else {
					responseObject.routesInfo[indexRoute].routeSegmentInfo[indexRouteSegment].situationInfo = {
						situationSummaryInfo: "noSituationInfo",
						type: "trafficSituation",
						accidents: []
					};
				};
				if (numRunningQueries === 0) {
					finish(res, responseObject);
				}
			});
		})
	});

	function finish(res, responseObject) {
		if (operation == "trafficRouteInformationRequest") {
			res.send({
				trafficRouteInformationResponse: responseObject,
			});
		} else if (operation == "trafficRouteSegmentsInformationRequest") {
			res.send({
				trafficRouteSegmentsInformationResponse: {
					routeSegmentInfo: responseObject.routesInfo[0].routeSegmentInfo
				}
			});
		}
	}
})


module.exports = router;