var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */

router.post('/', function (req, res, next) {
    //request example { "lon": "11.93115234375",  "lat": "57.68611907959"}
    //if there is no accidents, an object {"accidents": [ {} ] } is returned
    var longitude = 11.991044; // req.body.accidentInformationRequest.lon;
    var latitude = 57.719014; //req.body.accidentInformationRequest.lat;

    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.3/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='Situation'>" +
            "<FILTER><WITHIN name='Deviation.Geometry.WGS84' shape='center' value='" + longitude + " " + latitude + "' radius='500m' /></FILTER>" + ///CHANGE THE RADIUS HERE!!! must be 500m
            "<GTE name='Deviation.EndTime' value='$now'/>" +
            "</QUERY></REQUEST>"
    }, function (error, response, body) {
        var responseContent = JSON.parse(response.body).RESPONSE.RESULT[0].Situation;
        console.log(response.body);
        var accidents = [];
        //building the response
        var accidentInformationResponse = {};

        if (Array.isArray(responseContent)) {
            responseContent.forEach(function (element, index) {
                var accident = {};
                var re = new RegExp(" \\(*|\\)");
                var coordinates = element.Deviation[0].Geometry.WGS84.split(re);
                accident.longitude = parseFloat(coordinates[1]);
                accident.latitude = parseFloat(coordinates[2]);
                accident.messageCodeValue = element.Deviation[0].MessageCodeValue;
                accident.messageTypeValue = element.Deviation[0].MessageTypeValue;
                accident.severityCode = element.Deviation[0].SeverityCode;
                accident.description = element.Deviation[0].Message;
                accidents[index] = {};
                accidents[index].accidentInformation = accident;
            });
            accidentInformationResponse.accidents = accidents;
        } else {
            accidentInformationResponse.accidents = [];
        }


        res.send({
            accidentInformationResponse: accidentInformationResponse
        });
    });
});

module.exports = router;