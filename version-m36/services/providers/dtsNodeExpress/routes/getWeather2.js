var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */
// Weather update every 10 minutes -> answer should have Measure time, if Measure time is outdated then ask for weather again

router.post('/', function (req, res, next) {
    var longitude = req.body.segmentInformationRequest.waypoint0.lon; //11.9691; 
    var latitude = req.body.segmentInformationRequest.waypoint0.lat; //57.7087; 
    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.3/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='WeatherStation'>" +
            "<FILTER><WITHIN name='Geometry.WGS84' shape='center' value='" + longitude + " " + latitude + "' radius='10000m' /></FILTER>" +
            "<INCLUDE>Measurement.Air.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.MeasureTime</INCLUDE>" +
            "<INCLUDE>Measurement.Road.Temp</INCLUDE>" +
            "<INCLUDE>Measurement.Air.RelativeHumidity</INCLUDE>" +
            "<INCLUDE>Measurement.Wind</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "<INCLUDE>Active</INCLUDE>" +
            "<INCLUDE>Name</INCLUDE>" +
            "</QUERY></REQUEST>"
    }, function (error, response, body) {
        var responseContent = JSON.parse(response.body).RESPONSE.RESULT;
        try {
            segmentWeatherInformationResponse = {
                waypoint0: req.body.segmentInformationRequest.waypoint0,
                waypoint1: req.body.segmentInformationRequest.waypoint1,
                type: "weather",
                weatherInfo: {
                    roadTemperature: responseContent[0].WeatherStation[0].Measurement.Road.Temp,
                    airTemperature: responseContent[0].WeatherStation[0].Measurement.Air.Temp,
                    airRelativeHumidity: responseContent[0].WeatherStation[0].Measurement.Air.RelativeHumidity,
                    windForce: responseContent[0].WeatherStation[0].Measurement.Wind.Force
                }
            };
        } catch (e) {
            segmentWeatherInformationResponse = {
                waypoint0: req.body.segmentInformationRequest.waypoint0,
                waypoint1: req.body.segmentInformationRequest.waypoint1,
                type: "weather",
                weatherInfo: {
                    roadTemperature: -100,
                    airTemperature: -100,
                    airRelativeHumidity: -100,
                    windForce: -100
                }
            };
        }
        res.send({
            segmentWeatherInformationResponse: segmentWeatherInformationResponse
        });
    });
});

module.exports = router;