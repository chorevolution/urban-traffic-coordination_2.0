var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */

router.post('/', function (req, res, next) {
    //request example { "lon": "11.93115234375",  "lat": "57.68611907959"}
    //if there is no accidents, an object {"accidents": [ {} ] } is returned
    var longitude1 = parseFloat(req.body.segmentInformationRequest.waypoint0.lon); // 11.995123;  
    var latitude1 = parseFloat(req.body.segmentInformationRequest.waypoint0.lat); // 57.713152;
    var longitude2 = parseFloat(req.body.segmentInformationRequest.waypoint1.lon); // 11.980963;
    var latitude2 = parseFloat(req.body.segmentInformationRequest.waypoint1.lat); // 57.725695;
    // Assume longitude1 LTE longitude2, if not swap this 2 points
    var r = 0.001;
    if (longitude1 > longitude2) {
        var tmp = longitude1;
        longitude1 = longitude2;
        longitude2 = tmp;
        tmp = latitude1;
        latitude1 = latitude2;
        latitude2 = tmp;
    }
    if (latitude1 > latitude2) {
        var polygonstring = (longitude1 - r) + " " + (latitude1 - r) + "," + // CHECK IF LAT LON IS REVERSED HERE??????s
            (longitude1 - r) + " " + (latitude1 + r) + "," +
            (longitude1 + r) + " " + (latitude1 + r) + "," +
            (longitude2 + r) + " " + (latitude2 + r) + "," +
            (longitude2 + r) + " " + (latitude2 - r) + "," +
            (longitude2 - r) + " " + (latitude2 - r) + "," +
            (longitude1 - r) + " " + (latitude1 - r);
    } else {
        var polygonstring = (longitude1 - r) + " " + (latitude1 - r) + "," +
            (longitude1 - r) + " " + (latitude1 + r) + "," +
            (longitude2 - r) + " " + (latitude2 + r) + "," +
            (longitude2 + r) + " " + (latitude2 + r) + "," +
            (longitude2 + r) + " " + (latitude2 - r) + "," +
            (longitude1 + r) + " " + (latitude1 - r) + "," +
            (longitude1 - r) + " " + (latitude1 - r);
    }


    request({
        url: 'http://api.trafikinfo.trafikverket.se/v1.3/data.json',
        method: 'POST',
        headers: [{
            name: 'content-type',
            value: 'text/xml'
        }],
        body: "<REQUEST><LOGIN authenticationkey='f3c1bde2360a47bea3576b8274ce8b28'/><QUERY objecttype='Situation'>" +
            "<FILTER><WITHIN name='Deviation.Geometry.WGS84' shape='polygon' value='" + polygonstring + "' /></FILTER>" + ///CHANGE THE RADIUS HERE!!! must be 500m
            "<GTE name='Deviation.EndTime' value='$now'/>" +
            "</QUERY></REQUEST>"
    }, function (error, response, body) {
        try {
            var responseContent = JSON.parse(response.body).RESPONSE.RESULT[0].Situation;
        } catch (e) {
            var responseContent = null;
        }
        var accidents = [];
        //building the response
        var segmentAccidentInformationResponse = {};
        try {
            var minSeverityCode = 10;
            var accidentCode = "noSituationInfo";
            responseContent.forEach(function (element, index) {
                var accident = {};
                var re = new RegExp(" \\(*|\\)");
                var coordinates = element.Deviation[0].Geometry.WGS84.split(re);
                accident.longitude = parseFloat(coordinates[1]);
                accident.latitude = parseFloat(coordinates[2]);
                accident.messageCodeValue = element.Deviation[0].MessageCodeValue;
                accident.messageTypeValue = element.Deviation[0].MessageTypeValue;
                accident.severityCode = element.Deviation[0].SeverityCode;
                accident.description = element.Deviation[0].Message.substring(0,254);
                accidents[index] = {};
                //accidents[index].accidentInformation = accident;
                accidents[index] = accident;
                if (minSeverityCode > accident.severityCode) {
                    minSeverityCode = accident.severityCode;
                    accidentCode = accident.messageCodeValue;
                }
            });
        } catch (e) {
            accidents = [];
        };
        segmentAccidentInformationResponse.waypoint0 = req.body.segmentInformationRequest.waypoint0;
        segmentAccidentInformationResponse.waypoint1 = req.body.segmentInformationRequest.waypoint1;
        segmentAccidentInformationResponse.type = "roadSituation2";
        segmentAccidentInformationResponse.situationInfo = {
            situationSummaryInfo: accidentCode,
            type: "trafficSituation",
            accidents: accidents
        };
        res.send({
            segmentAccidentInformationResponse: segmentAccidentInformationResponse
        });
    });
});

module.exports = router;