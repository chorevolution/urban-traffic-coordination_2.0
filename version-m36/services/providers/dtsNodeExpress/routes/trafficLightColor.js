var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */

router.post('/', function (req, res, next) {
	// takes time in milliseconds, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
	var date = req.body.date;
	if (isOdd(Math.floor(parseInt(date) / 60000))) {
		res.send({
			color: "green"
		});
	} else {
		res.send({
			color: "red"
		});
	}
})


module.exports = router;


function isOdd(num) {
	return num % 2;
}