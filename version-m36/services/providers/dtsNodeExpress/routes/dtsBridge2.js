var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */

function isCrossingBridge(waypoint0, waypoint1) {
    var bridge = {};
    bridge.lat = 57.714714;
    bridge.lon = 11.966767; //center location of the Gotaalvbron bridge in Goteborg
    // We consider if that center location is inside the rectangular defined by the two segment waypoints then the segment will cross the bridge
    if (((bridge.lat - waypoint0.lat) * (bridge.lat - waypoint1.lat) < 0) && ((bridge.lon - waypoint0.lon) * (bridge.lon - waypoint1.lon) < 0)) {
        return true;
    } else {
        return false;
    }
}

router.post('/', function (req, res, next) {
    //console.log(req.body);
    console.log(req.app.locals.bridgeStatus);

    if (typeof req.body.toggleBridgeStatus !== 'undefined') {
        req.app.locals.bridgeStatus = !req.app.locals.bridgeStatus;
        res.send({
            bridgeStatus: req.app.locals.bridgeStatus
        });
        return;
    }

    if (typeof req.body.setBridgeStatus !== 'undefined') {
        req.app.locals.bridgeStatus = req.body.setBridgeStatus;
        res.send({
            bridgeStatus: req.app.locals.bridgeStatus
        });
        return;
    }

    if (typeof req.body.getBridgeStatus !== 'undefined') {
        res.send({
            bridgeStatus: req.app.locals.bridgeStatus
        });
        return;
    }

    // takes bridgeId, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
    var longitude0 = req.body.segmentInformationRequest.waypoint0.lon; //11.9691; 
    var latitude0 = req.body.segmentInformationRequest.waypoint0.lat; //57.7087; 
    var longitude1 = req.body.segmentInformationRequest.waypoint1.lon; //11.9691; 
    var latitude1 = req.body.segmentInformationRequest.waypoint1.lat; //57.7087; 
    if (!isCrossingBridge(req.body.segmentInformationRequest.waypoint0, req.body.segmentInformationRequest.waypoint1)) {
        isBridgeClosed = false;
        if (req.app.locals.bridgeStatus === 0) {
            isBridgeClosed = true
        };
        res.send({
            segmentBridgeInformationResponse: {
                waypoint0: req.body.segmentInformationRequest.waypoint0,
                waypoint1: req.body.segmentInformationRequest.waypoint1,
                bridgeStatus: isBridgeClosed,
                type: "NoBridge",
                closureStatus: {
                    isClosed: isBridgeClosed,
                    opensAtTimeMillis: 0
                },
                nextClosure: {
                    fromMillis: 0,
                    toMillis: 0
                }
            }
        });
        return;
    }
    request({
        url: 'http://data.goteborg.se/BridgeService/v1.0/GetGABOpenedStatus/152d47b0-754b-4718-a635-6a7db1374572?format=json',
        method: 'GET'
    }, function (error, response, body) {
        var isBridgeClosed = JSON.parse(response.body).Value;
        if (req.app.locals.bridgeStatus === 1) {
            isBridgeClosed = false
        };
        if (req.app.locals.bridgeStatus === 0) {
            isBridgeClosed = true
        };
        var currentTime = new Date();
        var nextOpenedTime;
        var nextClosureFrom;
        var nextClosureTo;

        if (isBridgeClosed) {
            nextOpenedTime = currentTime.getTime() + 7 * 60000; // we estimate that the bridge is closed for 7 minutes more in case that it is already closed
        } else {
            nextOpenedTime = currentTime.getTime(); //+1-1 is transformation to int
        }

        var currentHour = currentTime.getHours(); //hour of the day, 
        var currentMinute = currentTime.getMinutes(); //minute of the hour
        var closure1f = new Date();
        var closure2f = new Date();
        var closure1t = new Date();
        var closure2t = new Date();
        closure1f.setHours(9 - closure1f.getTimezoneOffset());
        closure1f.setMinutes(0);
        closure1f.setSeconds(0);
        closure1f.setMilliseconds(0);

        closure1t.setHours(9 - closure1t.getTimezoneOffset());
        closure1t.setMinutes(10);
        closure1t.setSeconds(0);
        closure1t.setMilliseconds(0);

        closure2f.setHours(18 - closure2f.getTimezoneOffset());
        closure2f.setHours(0);
        closure2f.setSeconds(0);
        closure2f.setMilliseconds(0);

        closure2t.setHours(18 - closure2t.getTimezoneOffset());
        closure2t.setHours(10);
        closure2t.setSeconds(0);
        closure2t.setMilliseconds(0);

        var currentDayInMillis = currentTime.getTime();
        if (currentDayInMillis > closure2t.getTime()) {
            nextClosureFrom = closure1f.getTime() + 86400000;
            nextClosureTo = closure1t.getTime() + 86400000; // add number of milliseconds of a day to get the next day
            isBridgeClosed = false;
        } else if (currentDayInMillis > closure1t.getTime()) {
            nextClosureFrom = closure2f.getTime();
            nextClosureTo = closure2t.getTime();
        } else if (currentDayInMillis < closure1f.getTime()) {
            nextClosureFrom = closure1f.getTime();
            nextClosureTo = closure1t.getTime();
        }
        res.send({
            segmentBridgeInformationResponse: {
                waypoint0: req.body.segmentInformationRequest.waypoint0,
                waypoint1: req.body.segmentInformationRequest.waypoint1,
                bridgeStatus: isBridgeClosed,
                type: "isBridgeClosed",
                closureStatus: {
                    isClosed: isBridgeClosed,
                    opensAtTimeMillis: nextOpenedTime
                },
                nextClosure: {
                    fromMillis: nextClosureFrom,
                    toMillis: nextClosureTo
                }
            }
        });
    });
});
module.exports = router;