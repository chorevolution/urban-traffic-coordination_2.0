var express = require('express');
var router = express.Router();

var request = require('request');

router.post('/', function (req, res, next) {
    var segment = req.body.segmentInformationRequest;
    var shortcut = true; //This parameter to avoid exceeding Tomtom limit of 2500 segment request per day. This will generate random number instead.
    // Change the value to false to get the real information from Tomtom for ecoSpeed and congestions
    if (shortcut) {
        segmentCongestionInformationResponse = {
            speed: Math.floor(Math.random() * 50) + 30,
            congestion: 0,
            type: 'congestion',
            waypoint0: segment.waypoint0,
            waypoint1: segment.waypoint1,
        };
        res.send({
            segmentCongestionInformationResponse: segmentCongestionInformationResponse
        });
        return;
    }
    request({
        url: 'https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/json?' +
//            'key=v9byd7y3x9dwgjhzyg36s4pa' +
            'key=sE5VgZaDGVyVcgQhUfQj9hFjYobf8HGv' +
            '&point=' + segment.waypoint0.lat + ',' + segment.waypoint0.lon +
            '&unit=KMPH',
        method: 'GET',
    }, function (error, response, body) {
        resultSegment = {};
        resultSegment.segmentCongestionInformationResponse = {};
        var responseContent = JSON.parse(response.body);
        try {
            resultSegment.segmentCongestionInformationResponse.speed = parseInt(currentSpeed);
            if (resultSegment.segmentCongestionInformationResponse.speed < parseFloat(freeFlowSpeed) * 0.75) {
                resultSegment.segmentCongestionInformationResponse.congestion = 1;
            } else {
                resultSegment.segmentCongestionInformationResponse.congestion = 0;
            }
        } catch (e) {
            resultSegment.segmentCongestionInformationResponse.congestion = -100;
            resultSegment.segmentCongestionInformationResponse.speed = -100;
        };
        resultSegment.segmentCongestionInformationResponse.type = "congestion";
        resultSegment.segmentCongestionInformationResponse.waypoint0 = segment.waypoint0;
        resultSegment.segmentCongestionInformationResponse.waypoint1 = segment.waypoint1;
        res.send({
            segmentCongestionInformationResponse: resultSegment.segmentCongestionInformationResponse
        });
    });
});

module.exports = router;