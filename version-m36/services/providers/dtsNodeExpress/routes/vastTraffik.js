var express = require('express');
var router = express.Router();
var request = require('request');
var bodyParser = require('body-parser');
var xml2js = require("xml2js");
var parser = new xml2js.Parser();
var dateFormat = require('dateformat');


KEY = 'fTuONd5NDKjzpjY2sCHXkdbytAIa'; //'fRJUFeL5KXN_sCV6nuY51eUD844a';
SECRET = 'p4yK3lnE0tkvQP1RBWc1TH0eXgca'; //'twdD20wDNdpfQnpkmxCH2DKAO1ka';

router.get('/', function (req, res, next) {
    // takes time in milliseconds, example of post request body {"date" : "1475746321493", "nextTrafficLightId":"28"} 
    request({
        url: 'https://api.vasttrafik.se/token',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Basic ZlR1T05kNU5ES2p6cGpZMnNDSFhrZGJ5dEFJYTpwNHlLM2xuRTB0a3ZRUDFSQldjMVRIMGVYZ2Nh`
        },
        body: `grant_type=client_credentials&scope=device_id`
    }, function (error, response, body) {
        var token = JSON.parse(response.body).access_token;
        var now = new Date();
        request({
            url: 'https://api.vasttrafik.se/bin/rest.exe/v2/departureBoard?id=9022014002472002&date=' + dateFormat(now, "yy-mm-dd") + '&time=' + dateFormat(now, "HH:MM"),
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token
            },
        }, function (error, response, body) {
            parser.parseString(response.body, function (err, result) {
                //var t = JSON.parse(result);
                //res.send(result);
                var sendText = "<table style='width:100%; border: 1px solid black'><tr><th>STOP</th><th>BUS</th><th>Date</th><th>Planned</th><th>Actual</th><th>Track</th><th>Direction</th></tr>";
                var arr = result.DepartureBoard.Departure
                arr.forEach(function (element) {
                    sendText += "<tr> <td>" + element.$.stop + "</td><td><a href='" + element.JourneyDetailRef[0].$.ref + "'>" + element.$.name + "</a></td><td>" + element.$.date + "</td><td>" + element.$.time + "</td><td>" + element.$.rtTime + "</td><td>" + element.$.track + "</td><td>" + element.$.direction + "</td></tr>";
                });
                res.send(sendText);
                console.log(dateFormat(now, "HH:MM:ss"));
                //console.log('called parsing');
                //console.log(results[element]);
            });
        })
    })
})


module.exports = router;