var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */
// Weather update every 10 minutes -> answer should have Measure time, if Measure time is outdated then ask for weather again

router.post('/', function (req, res, next) {
    request({
        url: 'https://maps.googleapis.com/maps/api/directions/json?' +
            'key=AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw&alternatives=true' +
            //'waypoints=via:57.721578,11.987252' + //uncomment this line if bridge is opened
            '&origin=' + req.body.routesRequest.origin.lat + ', ' + req.body.routesRequest.origin.lon +
            '&destination=' + req.body.routesRequest.destination.lat + ', ' + req.body.routesRequest.destination.lon +
            '&mode=DRIVING',
        method: 'GET',
    }, function (error, response, body) {
        var responseContent = JSON.parse(response.body).routes;
        var GoogleRouteSuggestion = [];
        responseContent.forEach(function (elementRoute, index) {
            var legs = elementRoute.legs[0].steps;
            var routeSegments = [];
            legs.forEach(function (elementLeg, legIndex) {
                routeSegments[legIndex] = {
                    'waypoint0': {
                        'lat': elementLeg.start_location.lat,
                        'lon': elementLeg.start_location.lng
                    },
                    'waypoint1': {
                        'lat': elementLeg.end_location.lat,
                        'lon': elementLeg.end_location.lng
                    },
                    'instruction': elementLeg.html_instructions + '<div class="hiddentext" style="display: none;">'+ elementLeg.maneuver + '</div>',
                    'time': elementLeg.duration.value,
                    'distance': elementLeg.distance.value,
                    'polyline': elementLeg.polyline.points
                };
            });
            GoogleRouteSuggestion[index] = {
                //'id': index,
                //'provider': "google",
                'origin': req.body.routesRequest.origin,
                'destination': req.body.routesRequest.destination,
                'routePolyline': elementRoute.overview_polyline.points,
                'routeSegments': routeSegments
            }
        });
        res.send({
            routesSuggestion: {
                routes: GoogleRouteSuggestion
            }
        });
    });
});

module.exports = router;