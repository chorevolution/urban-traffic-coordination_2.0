var express = require('express');
var router = express.Router();
var polyline = require('polyline');

var bodyParser = require('body-parser');
var request = require('request');

/* POST users listing. */
// Weather update every 10 minutes -> answer should have Measure time, if Measure time is outdated then ask for weather again

router.post('/', function (req, res, next) {
    request({
        url: 'https://route.cit.api.here.com/routing/7.2/calculateroute.json?' +
            'app_id=k0YXz4I407cw0jDmwdf9' +
            '&app_code=MhWNbYvql9cQeswuIYUCfg' + 
            '&instructionformat=text' +
            '&avoidareas=57.715454,11.965512;57.713829,11.967937' + //uncomment this line if bridge should be avoid
            '&waypoint0=geo!' + req.body.routesRequest.origin.lat + ',' + req.body.routesRequest.origin.lon +
            '&waypoint1=geo!' + req.body.routesRequest.destination.lat + ',' + req.body.routesRequest.destination.lon +
            '&mode=fastest;car;traffic:enabled&departure=now&alternatives=3&representation=display' +
            '&returnelevation=true&maneuverattributes=direction,action&routeattributes=waypoints,summary,shape,legs',
        method: 'GET',
    }, function (error, response, body) {
        var responseContent = JSON.parse(response.body).response.route;
        //console.log(responseContent);
        var HereRouteSuggestion = [];
        responseContent.forEach(function (elementRoute, index) {
            var legs = elementRoute.leg[0].maneuver;
            var routeSegments = [];
            legs.forEach(function (elementLeg, legIndex) {
                var polyArray = [];
                elementLeg.shape.forEach(function (elementShape, shapeIndex) {
                    polyArray[shapeIndex] = [parseFloat(elementShape.split(",")[0]), parseFloat(elementShape.split(",")[1])];
                });
                routeSegments[legIndex] = {
                    'waypoint0': {
                        'lat': elementLeg.position.latitude,
                        'lon': elementLeg.position.longitude
                    },
                    'waypoint1': {
                        'lat': parseFloat(elementLeg.shape[elementLeg.shape.length - 1].split(",")[0]),
                        'lon': parseFloat(elementLeg.shape[elementLeg.shape.length - 1].split(",")[1])
                    },
                    'instruction': elementLeg.instruction + '<div class="hiddentext" style="display: none;">'+ elementLeg.action + '</div>',
                    'time': elementLeg.travelTime,
                    'distance': elementLeg.length,
                    'polyline': polyline.encode(polyArray)
                };
            });
            routeSegments.splice(-1,1); //Remove the last leg since it is only one waypoint of destination
            var polyArray = [];
            elementRoute.shape.forEach(function (elementShape, shapeIndex) {
                polyArray[shapeIndex] = [parseFloat(elementShape.split(",")[0]), parseFloat(elementShape.split(",")[1])];
            });
            //console.log(polyline.decode(polyline.encode(polyArray))[polyArray.length-1]);
            HereRouteSuggestion[index] = {
                //'id': index,
                //'provider': "here",
                'origin': req.body.routesRequest.origin,
                'destination': req.body.routesRequest.destination,
                'routePolyline': polyline.encode(polyArray),
                'routeSegments': routeSegments
            }
        });
        res.send({
            routesSuggestion: {
                routes: HereRouteSuggestion
            }
        });
    });
});

module.exports = router;