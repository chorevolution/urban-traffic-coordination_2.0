'use strict';

var mysql     = require('mysql');

var database = {
  initDB: function() {
    this.pool  = mysql.createPool({
        connectionLimit : 100,
        host            : 'localhost',
        user            : 'seada',
        password        : 'rGsA3AL2w0hRfCll',
        database        : 'seada'
    });
  },

  query: function(query, callback) {
    this.pool.query(query, function(err, res) {
      if (err) {
        this.initDB();
        //you can call the query again here if you want
      } else {
        callback(res);
      }
    });
  },

  disconnect: function() {
    if (this.pool) {
      this.pool.end();
    }
  }

};

module.exports = database;