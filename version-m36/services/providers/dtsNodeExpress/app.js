var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
//var logger = require('morgan');
//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var index = require('./routes/index');
var users = require('./routes/users');
var dtsAccident2 = require('./routes/dtsAccident2');
var bridgeNextClosure2 = require('./routes/dtsBridge2');
var getWeather2 = require('./routes/getWeather2');
var trafficLightColor = require('./routes/trafficLightColor');
var getCongestion3 = require('./routes/getCongestion3');
var vastTraffik = require('./routes/vastTraffik');
var dtsGoogle = require('./routes/dtsGoogle');
var dtsHere = require('./routes/dtsHere');
var seadaAreaTraffic = require('./routes/seadaAreaTraffic');
var trafficLightInformation = require('./routes/dtsTrafficLight');

var seadaDB = require('./controllers/database');

var app = express();

app.locals.bridgeStatus = 15;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', index);
app.use('/ecoRoute', users);
app.use('/waypointAccidentInformation', dtsAccident2);
app.use('/routeSegmentBridgeStatusInformation', bridgeNextClosure2);
app.use('/waypointWeatherInformation', getWeather2);
app.use('/trafficLightColor', trafficLightColor);
app.use('/routeCongestionInformation', getCongestion3);
app.use('/vastTraffik', vastTraffik);
app.use('/dtsGoogle', dtsGoogle);
app.use('/dtsHere', dtsHere);
app.use('/seadaAreaTraffic', seadaAreaTraffic);
app.use('/trafficLightInformation', trafficLightInformation);

//seadaDB.query('SELECT * FROM trafficinfo;', console.log());
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;