package eu.chorevolution.prosumer.seadaseatsa.business.impl.service;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Consumer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoFriendlyRoutesInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.Route;
import eu.chorevolution.prosumer.seadaseatsa.RouteData;
import eu.chorevolution.prosumer.seadaseatsa.RouteInfo;
import eu.chorevolution.prosumer.seadaseatsa.RouteSegment;
import eu.chorevolution.prosumer.seadaseatsa.RouteSegmentInfo;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoFriendlyRoutesInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponse;

import eu.chorevolution.prosumer.seadaseatsa.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.seadaseatsa.business.SEADASEATSAService;

import eu.chorevolution.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

@Service
public class SEADASEATSAServiceImpl implements SEADASEATSAService {

	private double estimateEmissionRate(double speed, double congestionStatus, double airTemperature) {
		double spdMetersPerSec = speed / 3.6; // convert speed to m/s from km/h
		double baseEcoRate = Math
				.round((0.000065 * Math.pow(spdMetersPerSec, 2) + 0.533 / spdMetersPerSec + 0.09) * 1000.0) / 1000.0;
		return baseEcoRate * (1 + (24.22 * Math.exp(airTemperature * -0.0269) - 13.21) / 100 + congestionStatus * 0.32);
	}

	private double estimateRouteSegmentEmission(double speed, double congestionStatus, double airTemperature,
			int distance) {
		/*
		 * calculate emission per route segment. Speed can be taken from the Congestion
		 * service, or by calculated from time and distance info from routesSuggestion
		 * Unit speed: km/h. distance: m we assume car should change the speed to 20km/h
		 * at the two end waypoint of a route segment Still need to work out accelerator
		 */
		return Math.round(distance * estimateEmissionRate(speed, congestionStatus, airTemperature));
	}

	@Override
	public void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest parameter, String choreographyTaskName,
			String senderParticipantName) {
		/**
		 * TODO Add your business logic upon the reception of
		 * EcoSpeedRouteInformationRequest message from senderParticipantName
		 */

	}

	@Override
	public void getEcoFriendlyRoutesInformation(EcoFriendlyRoutesInformationRequest parameter,
			String choreographyTaskName, String senderParticipantName) {
		/**
		 * TODO Add your business logic upon the reception of
		 * EcoFriendlyRoutesInformationRequest message from senderParticipantName
		 * 
		 **/
	}

	@Override
	public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName,
			String receiverParticipantName) {

		/**
		 * TODO write the code that generates EcoSpeedRouteInformationResponse message
		 * from receiving trafficRouteInformationResponse. Put eco calculation into the
		 * response as well This works on routeSegment
		 */
		TrafficRouteInformationResponse inMessage = (TrafficRouteInformationResponse) choreographyInstanceMessages
				.getMessage("trafficRouteInformationResponse");

		EcoSpeedRouteInformationResponse result = new EcoSpeedRouteInformationResponse();

		RouteInfo routeItem = inMessage.getTrafficRouteInfo();
		for (Iterator<RouteSegmentInfo> i = routeItem.getRouteSegmentInfo().iterator(); i.hasNext();) {
			RouteSegmentInfo routeSegmentItem = i.next();
			routeSegmentItem.setEcoValue(estimateEmissionRate((double) routeSegmentItem.getSpeed(),
					(double) routeSegmentItem.getCongestion(),
					(double) routeSegmentItem.getWeatherInfo().getAirTemperature()));
			routeSegmentItem.setType("ecoEvaluated");
			result.getRouteSegmentInfo().add(routeSegmentItem);
		}

		return result;
	}

	@Override
	public TrafficRouteInformationRequest createTrafficRouteInformationRequest(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName,
			String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
		/**
		 * This function will create trafficRouteInformationRequest upon receiving two
		 * different types of messages for the route planning and also when driver is in
		 * the selected route asking for updated traffic information of route segments
		 * Note that the incoming messages are of different types, route planning
		 * requests are list of routes, while updated traffic information request are
		 * list of route segments When receiving ecoFriendlyRoutesInformationRequest,
		 * this function will create list of trafficRouteInformationRequest per route
		 * (index of the selected route is provided with ChoreographyLoopIndex) in the
		 * list of routes When receiving ecoSpeedRoutesInformationRequest, this will
		 * create 01 trafficRouteInformationRequest by forming a route from a
		 * routeSegment or list of routeSegments get message
		 * ecoFriendlyRoutesInformationRequest, create trafficRouteInformationRequest
		 * get message ecoSpeedRoutesInformationRequest, create
		 * trafficRouteInformationRequest
		 */
		TrafficRouteInformationRequest result = new TrafficRouteInformationRequest();

		int routeTrafficRequestSegmentIndex = (int) choreographyLoopIndexes.getChoreographyLoopIndex("GetTrafficRouteInformation");
		if(routeTrafficRequestSegmentIndex != -1) {
			EcoFriendlyRoutesInformationRequest routeTrafficRequest2 = (EcoFriendlyRoutesInformationRequest) choreographyInstanceMessages
					.getMessage("ecoFriendlyRoutesInformationRequest");	
			RouteData friendlyRoute = routeTrafficRequest2.getRoutes().get(routeTrafficRequestSegmentIndex);
			result.setTrafficRoute(friendlyRoute);
		}
		else {
			EcoSpeedRouteInformationRequest routeSegmentTrafficRequest = (EcoSpeedRouteInformationRequest) choreographyInstanceMessages
					.getMessage("ecoSpeedRouteInformationRequest");
			RouteData speedRoute = new RouteData();
			speedRoute.setId(1);
			speedRoute.setProvider("speedRoute");
			List<RouteSegment> routeSegments = routeSegmentTrafficRequest.getRouteSegments();
			boolean isFirst = true;
			for (Iterator<RouteSegment> i = routeSegments.iterator(); i.hasNext();) {
				RouteSegment routeSegment = i.next();
				speedRoute.getRouteSegments().add(routeSegment);
				if (isFirst) {
					speedRoute.setOrigin(routeSegment.getWaypoint0());
					speedRoute.setRoutePolyline(routeSegment.getPolyline()); // Polyline of first routeSegment is used
																				// for the traffic Route Request, this
																				// is added for completion of data type
																				// only
					isFirst = false;
				}
				if (!i.hasNext()) {
					speedRoute.setDestination(routeSegment.getWaypoint1());
				}
			}
			result.setTrafficRoute(speedRoute);			
		}

		return result;
	}

	@Override
	public EcoFriendlyRoutesInformationResponse createEcoFriendlyRoutesInformationResponse(
			ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName,
			String receiverParticipantName) {
		// Create ecoFriendlyRoutesInformationResponse from
		// trafficRouteInformationResponse
		// Same datatype but eco value should be added

		List<TrafficRouteInformationResponse> inMessage = choreographyInstanceMessages
				.getMessages("trafficRouteInformationResponse");
		EcoFriendlyRoutesInformationResponse result = new EcoFriendlyRoutesInformationResponse();
		for (TrafficRouteInformationResponse trafficRouteInfo : inMessage) {
			result.getRoutes().add(trafficRouteInfo.getTrafficRouteInfo());
		}
		System.out.println("Number of route responses: " + result.getRoutes().size());
		ListIterator<RouteInfo> routeItem = result.getRoutes().listIterator();
		while (routeItem.hasNext()) {
			ListIterator<RouteSegmentInfo> routeSegmentItem = routeItem.next().getRouteSegmentInfo().listIterator();
			while (routeSegmentItem.hasNext()) {
				RouteSegmentInfo r = routeSegmentItem.next();
				r.setEcoValue(estimateEmissionRate((double) r.getSpeed(), (double) r.getCongestion(),
						(double) r.getWeatherInfo().getAirTemperature()));
				r.setType("ecoEvaluated");
			}
		}
		return result;
	}

	@Override
	public void receiveTrafficRouteInformationResponse(TrafficRouteInformationResponse parameter,
			String choreographyTaskName, String senderParticipantName) {
		/**
		 * TODO Add your business logic upon the reception of
		 * (TrafficRouteInformationResponse message from senderParticipantName within
		 * the interaction belonging to choreographyTaskName
		 */
	}

}
