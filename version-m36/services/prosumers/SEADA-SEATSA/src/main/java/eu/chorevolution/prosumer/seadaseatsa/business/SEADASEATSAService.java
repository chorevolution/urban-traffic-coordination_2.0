package eu.chorevolution.prosumer.seadaseatsa.business;

import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoFriendlyRoutesInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoFriendlyRoutesInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponse;

import eu.chorevolution.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

public interface SEADASEATSAService {

	void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void getEcoFriendlyRoutesInformation(EcoFriendlyRoutesInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
	TrafficRouteInformationRequest createTrafficRouteInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
	EcoFriendlyRoutesInformationResponse createEcoFriendlyRoutesInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    void receiveTrafficRouteInformationResponse(TrafficRouteInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
}
