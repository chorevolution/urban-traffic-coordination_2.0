package eu.chorevolution.prosumer.seadaseatsa.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
