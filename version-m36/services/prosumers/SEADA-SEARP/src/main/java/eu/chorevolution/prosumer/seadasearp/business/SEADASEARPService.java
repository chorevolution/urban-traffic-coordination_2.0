package eu.chorevolution.prosumer.seadasearp.business;

import eu.chorevolution.prosumer.seadasearp.EcoRoutesRequest;
import eu.chorevolution.prosumer.seadasearp.EcoFriendlyRoutesInformationResponse;
import eu.chorevolution.prosumer.seadasearp.RoutesRequest;
import eu.chorevolution.prosumer.seadasearp.EcoRoutesResponse;
import eu.chorevolution.prosumer.seadasearp.EcoFriendlyRoutesInformationRequest;
import eu.chorevolution.prosumer.seadasearp.RoutesSuggestion;

import eu.chorevolution.prosumer.seadasearp.model.ChoreographyLoopIndexes;

public interface SEADASEARPService {

	void getEcoRoutes(EcoRoutesRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void setEcoFriendlyRoutesInformation(EcoFriendlyRoutesInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
	      
	RoutesRequest createRoutesRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
	EcoRoutesResponse createEcoRoutesResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
	EcoFriendlyRoutesInformationRequest createEcoFriendlyRoutesInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    void receiveRoutesSuggestion(RoutesSuggestion parameter, String choreographyTaskName, String senderParticipantName);
    
}
