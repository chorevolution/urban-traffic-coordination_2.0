package eu.chorevolution.prosumer.seadasearp.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
