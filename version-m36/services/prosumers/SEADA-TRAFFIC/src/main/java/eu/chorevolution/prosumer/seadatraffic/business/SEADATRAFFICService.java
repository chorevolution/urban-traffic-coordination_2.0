package eu.chorevolution.prosumer.seadatraffic.business;

import eu.chorevolution.prosumer.seadatraffic.AreaOfInterestRequest;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsRequest;
import eu.chorevolution.prosumer.seadatraffic.AreaTrafficInformation;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsResponse;

import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

public interface SEADATRAFFICService {

	void setAreaOfInterest(AreaOfInterestRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void setSegmentTrafficInformation(SegmentTrafficInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
	      
	AreaSegmentsRequest createAreaSegmentsRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
	AreaTrafficInformation createAreaTrafficInformation(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
	SegmentTrafficInformationRequest createSegmentTrafficInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    void receiveAreaSegmentsResponse(AreaSegmentsResponse parameter, String choreographyTaskName, String senderParticipantName);
    
}
