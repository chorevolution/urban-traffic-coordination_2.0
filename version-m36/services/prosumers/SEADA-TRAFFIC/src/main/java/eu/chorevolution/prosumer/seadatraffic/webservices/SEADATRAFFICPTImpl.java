package eu.chorevolution.prosumer.seadatraffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.prosumer.seadatraffic.SEADATRAFFICPT;
import eu.chorevolution.prosumer.seadatraffic.AreaOfInterestRequestType;
import eu.chorevolution.prosumer.seadatraffic.AreaOfInterestRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.SendRequestType;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.AreaSegmentsRequest;
import eu.chorevolution.prosumer.seadatraffic.SendRequestType;
import eu.chorevolution.prosumer.seadatraffic.AreaTrafficInformationReturnType;
import eu.chorevolution.prosumer.seadatraffic.AreaTrafficInformation;
import eu.chorevolution.prosumer.seadatraffic.SendRequestTypeWithLoop;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.SegmentTrafficInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.ReceiveAreaSegmentsResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveAreaSegmentsResponseReturnType;

import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyDataService;
import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.prosumer.seadatraffic.business.SEADATRAFFICService;

import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

@Component(value="SEADATRAFFICPTImpl")
public class SEADATRAFFICPTImpl implements SEADATRAFFICPT {
	
	private static Logger logger = LoggerFactory.getLogger(SEADATRAFFICPTImpl.class);

	@Autowired
	private SEADATRAFFICService service;
	
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
    @Override
    public AreaOfInterestRequestReturnType setAreaOfInterest(AreaOfInterestRequestType parameters) {
    	logger.info("CALLED setAreaOfInterest ON SEADA-TRAFFIC");	
    	AreaOfInterestRequestReturnType result = new AreaOfInterestRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.setAreaOfInterest(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public SegmentTrafficInformationResponseReturnType setSegmentTrafficInformation(SegmentTrafficInformationResponseType parameters) {
    	logger.info("CALLED setSegmentTrafficInformation ON SEADA-TRAFFIC");	
    	SegmentTrafficInformationResponseReturnType result = new SegmentTrafficInformationResponseReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.setSegmentTrafficInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public AreaSegmentsRequestReturnType sendAreaSegmentsRequest(SendRequestType parameters) {
    	logger.info("CALLED sendAreaSegmentsRequest ON SEADA-TRAFFIC");	
	    ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		AreaSegmentsRequest businessResult = service.createAreaSegmentsRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
    	store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		AreaSegmentsRequestReturnType result = new AreaSegmentsRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public AreaTrafficInformationReturnType sendAreaTrafficInformation(SendRequestType parameters) {
    	logger.info("CALLED sendAreaTrafficInformation ON SEADA-TRAFFIC");	
	    ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		AreaTrafficInformation businessResult = service.createAreaTrafficInformation(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName());
    	store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		AreaTrafficInformationReturnType result = new AreaTrafficInformationReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public SegmentTrafficInformationRequestReturnType sendSegmentTrafficInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendSegmentTrafficInformationRequest ON SEADA-TRAFFIC");	
		logger.info("sendSegmentTrafficInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		SegmentTrafficInformationRequest businessResult = service.createSegmentTrafficInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
    	store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		SegmentTrafficInformationRequestReturnType result = new SegmentTrafficInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
	@Override
	public ReceiveAreaSegmentsResponseReturnType receiveAreaSegmentsResponse(ReceiveAreaSegmentsResponseType parameters) {
		logger.info("CALLED receiveAreaSegmentsResponse ON SEADA-TRAFFIC");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveAreaSegmentsResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveAreaSegmentsResponseReturnType();
	}
     

	
}
