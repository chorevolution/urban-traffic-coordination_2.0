package eu.chorevolution.prosumer.dtssegmenttraffic.business;

public interface ChoreographyDataService {

	ChoreographyInstanceMessagesStore getChoreographyInstanceMessages(String choreographyID);
	
}
