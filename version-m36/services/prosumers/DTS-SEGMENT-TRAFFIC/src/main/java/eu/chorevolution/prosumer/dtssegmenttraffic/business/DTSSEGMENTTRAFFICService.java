package eu.chorevolution.prosumer.dtssegmenttraffic.business;

import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationRequest;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentInformationRequest;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentAccidentInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentBridgeInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentCongestionInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentWeatherInformationResponse;

import eu.chorevolution.prosumer.dtssegmenttraffic.model.ChoreographyLoopIndexes;

public interface DTSSEGMENTTRAFFICService {

	void getSegmentTrafficInformation(SegmentTrafficInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	SegmentInformationRequest createSegmentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
	SegmentTrafficInformationResponse createSegmentTrafficInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    void receiveSegmentAccidentInformationResponse(SegmentAccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveSegmentBridgeInformationResponse(SegmentBridgeInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveSegmentCongestionInformationResponse(SegmentCongestionInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveSegmentWeatherInformationResponse(SegmentWeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
}
