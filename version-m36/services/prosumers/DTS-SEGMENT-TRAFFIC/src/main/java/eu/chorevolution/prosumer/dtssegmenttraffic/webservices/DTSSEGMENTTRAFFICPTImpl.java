package eu.chorevolution.prosumer.dtssegmenttraffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.prosumer.dtssegmenttraffic.DTSSEGMENTTRAFFICPT;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationRequestType;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationRequestReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.SendRequestTypeWithLoop;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentInformationRequestReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentInformationRequest;
import eu.chorevolution.prosumer.dtssegmenttraffic.SendRequestTypeWithLoop;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationResponseReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentAccidentInformationResponseType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentAccidentInformationResponseReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentBridgeInformationResponseType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentBridgeInformationResponseReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentCongestionInformationResponseType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentCongestionInformationResponseReturnType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentWeatherInformationResponseType;
import eu.chorevolution.prosumer.dtssegmenttraffic.ReceiveSegmentWeatherInformationResponseReturnType;

import eu.chorevolution.prosumer.dtssegmenttraffic.business.ChoreographyDataService;
import eu.chorevolution.prosumer.dtssegmenttraffic.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.prosumer.dtssegmenttraffic.business.DTSSEGMENTTRAFFICService;

import eu.chorevolution.prosumer.dtssegmenttraffic.model.ChoreographyLoopIndexes;

@Component(value="DTSSEGMENTTRAFFICPTImpl")
public class DTSSEGMENTTRAFFICPTImpl implements DTSSEGMENTTRAFFICPT {
	
	private static Logger logger = LoggerFactory.getLogger(DTSSEGMENTTRAFFICPTImpl.class);

	@Autowired
	private DTSSEGMENTTRAFFICService service;
	
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
    @Override
    public SegmentTrafficInformationRequestReturnType getSegmentTrafficInformation(SegmentTrafficInformationRequestType parameters) {
    	logger.info("CALLED getSegmentTrafficInformation ON DTS-SEGMENT-TRAFFIC");	
    	SegmentTrafficInformationRequestReturnType result = new SegmentTrafficInformationRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.getSegmentTrafficInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public SegmentInformationRequestReturnType sendSegmentInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendSegmentInformationRequest ON DTS-SEGMENT-TRAFFIC");	
		logger.info("sendSegmentInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		SegmentInformationRequest businessResult = service.createSegmentInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
    	store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		SegmentInformationRequestReturnType result = new SegmentInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public SegmentTrafficInformationResponseReturnType sendSegmentTrafficInformationResponse(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendSegmentTrafficInformationResponse ON DTS-SEGMENT-TRAFFIC");	
		logger.info("sendSegmentTrafficInformationResponse - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		SegmentTrafficInformationResponse businessResult = service.createSegmentTrafficInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
    	store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		SegmentTrafficInformationResponseReturnType result = new SegmentTrafficInformationResponseReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
	@Override
	public ReceiveSegmentAccidentInformationResponseReturnType receiveSegmentAccidentInformationResponse(ReceiveSegmentAccidentInformationResponseType parameters) {
		logger.info("CALLED receiveSegmentAccidentInformationResponse ON DTS-SEGMENT-TRAFFIC");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveSegmentAccidentInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveSegmentAccidentInformationResponseReturnType();
	}
     
	@Override
	public ReceiveSegmentBridgeInformationResponseReturnType receiveSegmentBridgeInformationResponse(ReceiveSegmentBridgeInformationResponseType parameters) {
		logger.info("CALLED receiveSegmentBridgeInformationResponse ON DTS-SEGMENT-TRAFFIC");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveSegmentBridgeInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveSegmentBridgeInformationResponseReturnType();
	}
     
	@Override
	public ReceiveSegmentCongestionInformationResponseReturnType receiveSegmentCongestionInformationResponse(ReceiveSegmentCongestionInformationResponseType parameters) {
		logger.info("CALLED receiveSegmentCongestionInformationResponse ON DTS-SEGMENT-TRAFFIC");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveSegmentCongestionInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveSegmentCongestionInformationResponseReturnType();
	}
     
	@Override
	public ReceiveSegmentWeatherInformationResponseReturnType receiveSegmentWeatherInformationResponse(ReceiveSegmentWeatherInformationResponseType parameters) {
		logger.info("CALLED receiveSegmentWeatherInformationResponse ON DTS-SEGMENT-TRAFFIC");
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveSegmentWeatherInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveSegmentWeatherInformationResponseReturnType();
	}
     

	
}
