package eu.chorevolution.prosumer.dtssegmenttraffic.business.impl.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.dtssegmenttraffic.RouteSegmentPoints;
import eu.chorevolution.prosumer.dtssegmenttraffic.RouteSegmentTrafficInfo;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentAccidentInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentBridgeInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentCongestionInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentInformationRequest;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationRequest;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentTrafficInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.SegmentWeatherInformationResponse;
import eu.chorevolution.prosumer.dtssegmenttraffic.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.dtssegmenttraffic.business.DTSSEGMENTTRAFFICService;
import eu.chorevolution.prosumer.dtssegmenttraffic.model.ChoreographyLoopIndexes;

@Service
public class DTSSEGMENTTRAFFICServiceImpl implements DTSSEGMENTTRAFFICService {

	@Override
	public void getSegmentTrafficInformation(SegmentTrafficInformationRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of SegmentTrafficInformationRequest message from senderParticipantName
		*/
	}     

    @Override
	public SegmentInformationRequest createSegmentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
	    	/**
	    	 * Create SegmentInformationRequest from SegmentTrafficInformationRequest
			 */	
	//    	SegmentTrafficInformationRequest inMessage = (SegmentTrafficInformationRequest) choreographyInstanceMessages.getMessage("segmentTrafficInformationRequest");
	
	    	SegmentTrafficInformationRequest inMessage = (SegmentTrafficInformationRequest) choreographyInstanceMessages.getMessages("segmentTrafficInformationRequest").get(choreographyLoopIndexes.getChoreographyLoopIndex("TrafficSegmentInformationCollection"));
	    	
	    	SegmentInformationRequest result = new SegmentInformationRequest();
	    	result.setWaypoint0(inMessage.getWaypoint0());
	    	result.setWaypoint1(inMessage.getWaypoint1());
	    	return result;
    }
    
    @Override
	public SegmentTrafficInformationResponse createSegmentTrafficInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
	    	SegmentTrafficInformationResponse result = new SegmentTrafficInformationResponse();
	    	/**
	    	 * Create SegmentTrafficInformationResponse from 
	    	 * segmentAccidentInformationResponse
	    	 * segmentBridgeInformationResponse
	    	 * segmentCongestionInformationResponse
	    	 * segmentWeatherInformationResponse
			 */	
	    SegmentAccidentInformationResponse resultAccident = (SegmentAccidentInformationResponse) choreographyInstanceMessages.getMessages("segmentAccidentInformationResponse").get(choreographyLoopIndexes.getChoreographyLoopIndex("TrafficSegmentInformationCollection"));
	    	SegmentBridgeInformationResponse resultBridge = (SegmentBridgeInformationResponse) choreographyInstanceMessages.getMessages("segmentBridgeInformationResponse").get(choreographyLoopIndexes.getChoreographyLoopIndex("TrafficSegmentInformationCollection"));
	    	SegmentCongestionInformationResponse resultCongestion = (SegmentCongestionInformationResponse) choreographyInstanceMessages.getMessages("segmentCongestionInformationResponse").get(choreographyLoopIndexes.getChoreographyLoopIndex("TrafficSegmentInformationCollection"));
	    	SegmentWeatherInformationResponse resultWeather = (SegmentWeatherInformationResponse) choreographyInstanceMessages.getMessages("segmentWeatherInformationResponse").get(choreographyLoopIndexes.getChoreographyLoopIndex("TrafficSegmentInformationCollection"));
	    	
	    	RouteSegmentTrafficInfo resultSegment = new RouteSegmentTrafficInfo();
	    	resultSegment.setId(1);
	    	String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
	    	resultSegment.setUpdatedAt(timeStamp);
	    	resultSegment.setSituationInfo(resultAccident.getSituationInfo());
	    	resultSegment.setWeatherInfo(resultWeather.getWeatherInfo());
	    	if (resultBridge.getType().equals("NoBridge")) { //0: routeSegment passes the Bridge while it is closed, 1, 2: does not need to care
	    		resultSegment.setBridgeStatus(2);
	    	}
	    	else {
	    		resultSegment.setBridgeStatus(resultBridge.isBridgeStatus()? 0:1);
	    	} 
	    resultSegment.setCongestion(resultCongestion.getCongestion());
	    resultSegment.setSpeed(resultCongestion.getSpeed());
	    RouteSegmentPoints routeSegment = new RouteSegmentPoints();
	    	routeSegment.setWaypoint0(resultBridge.getWaypoint0());
	    	routeSegment.setWaypoint1(resultBridge.getWaypoint1());
	    	resultSegment.setRouteSegmentPoints(routeSegment);
	    	result.setSegment(resultSegment);
	    	return result;
    }
    
	@Override    
    public void receiveSegmentAccidentInformationResponse(SegmentAccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (SegmentAccidentInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveSegmentBridgeInformationResponse(SegmentBridgeInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (SegmentBridgeInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveSegmentCongestionInformationResponse(SegmentCongestionInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (SegmentCongestionInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveSegmentWeatherInformationResponse(SegmentWeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (SegmentWeatherInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    

}
