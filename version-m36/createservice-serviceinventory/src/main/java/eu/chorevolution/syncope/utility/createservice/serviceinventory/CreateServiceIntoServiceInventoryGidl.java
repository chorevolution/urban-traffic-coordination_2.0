/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.syncope.utility.createservice.serviceinventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.cxf.helpers.IOUtils;
import org.apache.syncope.common.lib.to.AnyObjectTO;
import org.apache.syncope.common.lib.to.AttrTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.InterfaceDescriptionType;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.Service;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.ServiceRole;

public class CreateServiceIntoServiceInventoryGidl {
	private static final Logger LOG = LoggerFactory.getLogger(CreateServiceIntoServiceInventoryGidl.class);

	public static String URL = "http://localhost:9080/syncope/rest/";
	public static String USERNAME = "admin";
	public static String PASSWORD = "password";
	public static String DOMAIN = "Master";

	private static final String INPUT_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "main"
			+ File.separatorChar + "resources" + File.separatorChar;

	private static final String INTERFACE_INPUT_RESOURCES = INPUT_RESOURCES + "interface" + File.separatorChar + "gidl" + File.separatorChar;

	private static ApacheSyncopeUtilities syncopeUtilities;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		syncopeUtilities = new ApacheSyncopeUtilities(URL, USERNAME, PASSWORD, DOMAIN);

		addProviderServices();
	}

	private static void addProviderServices() {
		try {
			Service service = new Service("DTS-ACCIDENTS", "http://localhost:9090/bcDTS-ACCIDENTS/bcDTS-ACCIDENTS",
					IOUtils.readBytesFromStream(
							new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-accidents.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-ACCIDENTS_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-BRIDGE", "http://localhost:9090/bcDTS-BRIDGE/bcDTS-BRIDGE",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-bridge.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-BRIDGE_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-CONGESTION", "http://localhost:9090/bcDTS-CONGESTION/bcDTS-CONGESTION",
					IOUtils.readBytesFromStream(
							new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-congestion.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-CONGESTION_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-GOOGLE", "http://localhost:9090/bcDTS-GOOGLE/bcDTS-GOOGLE",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-google.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-GOOGLE_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-HERE", "http://localhost:9090/bcDTS-HERE/bcDTS-HERE",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "DTS-HERE.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("DTS-HERE_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-WEATHER", "http://localhost:9090/bcDTS-WEATHER/bcDTS-WEATHER",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-weather.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-WEATHER_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-AREA-TRAFFIC", "http://localhost:9090/DTS-AREA-TRAFFIC/DTS-AREA-TRAFFIC",
					IOUtils.readBytesFromStream(
							new FileInputStream(INTERFACE_INPUT_RESOURCES + "DTS-AREA-TRAFFIC.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("DTS-AREA-TRAFFIC_role", ""));
			syncopeUtilities.createService(service);

		} catch (IOException e) {
			LOG.error("While loading provider services", e);
		}
	}

	private static ServiceRole createRole(String roleName, String description) {
		AnyObjectTO role = syncopeUtilities.createRole(roleName, description);

		ServiceRole serviceRole = new ServiceRole();

		serviceRole.setKey(role.getKey());
		serviceRole.setName(role.getName());

		Iterator<AttrTO> iterator = role.getPlainAttrs().iterator();
		while (iterator.hasNext()) {

			AttrTO attr = iterator.next();
			if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.SERVICE_ROLE_DESCRIPTION)) {
				serviceRole.setDescription(attr.getValues().get(0));
			}

		}

		return serviceRole;

	}

}
