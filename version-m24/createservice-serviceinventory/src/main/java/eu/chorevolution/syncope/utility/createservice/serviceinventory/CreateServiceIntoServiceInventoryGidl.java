/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.syncope.utility.createservice.serviceinventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.common.util.Base64Exception;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.helpers.IOUtils;
import org.apache.syncope.common.lib.to.AnyObjectTO;
import org.apache.syncope.common.lib.to.AttrTO;
import org.apache.syncope.common.lib.to.GroupTO;
import org.apache.syncope.common.lib.to.RelationshipTO;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.modelingnotations.security.AuthenticationTypeForwarded;
import eu.chorevolution.modelingnotations.security.SecurityModel;
import eu.chorevolution.modelingnotations.security.impl.SecurityPackageImpl;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.EnactmentEngine;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.InterfaceDescriptionType;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.Service;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.ServiceAuthenticationType;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.ServiceRole;

public class CreateServiceIntoServiceInventoryGidl {
	private static final Logger LOG = LoggerFactory.getLogger(CreateServiceIntoServiceInventoryGidl.class);

	public static String URL = "http://localhost:9080/syncope/rest/";
	public static String USERNAME = "admin";
	public static String PASSWORD = "password";
	public static String DOMAIN = "Master";

	private static final String INPUT_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "main"
			+ File.separatorChar + "resources" + File.separatorChar;

	private static final String OUTPUT_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "main"
			+ File.separatorChar + "resources" + File.separatorChar + "out" + File.separatorChar;

	private static final String INTERFACE_INPUT_RESOURCES = INPUT_RESOURCES + "interface" + File.separatorChar;
	private static final String INTERFACE_OUTPUT_RESOURCES = OUTPUT_RESOURCES + "interface" + File.separatorChar;

	private static final String SECURITY_INPUT_RESOURCES = INPUT_RESOURCES + "security" + File.separatorChar;
	private static final String SECURITY_OUTPUT_RESOURCES = OUTPUT_RESOURCES + "security" + File.separatorChar;

	private static final String CHOREOGRAPHY_DEPLOYMENT_INPUT_RESOURCES = INPUT_RESOURCES + "deploymentdescription"
			+ File.separatorChar;

	private static ApacheSyncopeUtilities syncopeUtilities;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		syncopeUtilities = new ApacheSyncopeUtilities(URL, USERNAME, PASSWORD, DOMAIN);

		addProviderServices();
/*
		syncopeUtilities.createChoreography("wp5",
				new FileInputStream(CHOREOGRAPHY_DEPLOYMENT_INPUT_RESOURCES + "wp5.xml"),
				new EnactmentEngine("ef028fc9-23bb-491b-828f-c923bb791bf5", "Default"));
*/
		// get all Service Roles
		getAllServiceRoles();

		// get all Services
		getAllServices("");

		// get all Enactment Engines
		getAllEnactmentEngines();

		// get all Choreography
		getAllChoreographies();
	}

	private static void addProviderServices() {
		try {
			Service service = new Service("DTS-ACCIDENTS", "http://jinx.viktoria.chalmers.se:3003/getAccidents/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-accidents.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-ACCIDENTS_role", ""));
			syncopeUtilities.createService(service);

			service = new Service("DTS-BRIDGE", "http://jinx.viktoria.chalmers.se:3001/bridgeNextClosure/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-bridge.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-BRIDGE_role", ""));
			syncopeUtilities.createService(service);
			
			service = new Service("DTS-CONGESTION", "http://jinx.viktoria.chalmers.se:3004/getCongestion/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-congestion.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-CONGESTION_role", ""));
			syncopeUtilities.createService(service);
			
			service = new Service("DTS-GOOGLE", "http://192.168.150.146:8080/DTS-GOOGLE/DTS-GOOGLE/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "google.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("DTS-GOOGLE_role", ""));
			syncopeUtilities.createService(service);
			
			service = new Service("DTS-HERE", "http://192.168.150.146:8080/DTS-HERE/DTS-HERE/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "here.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("DTS-HERE_role", ""));
			syncopeUtilities.createService(service);
			
			service = new Service("DTS-WEATHER", "http://jinx.viktoria.chalmers.se:3002/getWeather/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "dts-weather.gidl")),
					InterfaceDescriptionType.GIDL);
			service.addServiceRole(createRole("DTS-WEATHER_role", ""));
			syncopeUtilities.createService(service);
			
			
		} catch (IOException e) {
			LOG.error("While loading provider services", e);
		}
	}

	private static List<Service> getAllServices(String roleName) throws FileNotFoundException, IOException {
		FileUtils.deleteDirectory(new File(OUTPUT_RESOURCES));
		FileUtils.forceMkdir(new File(INTERFACE_OUTPUT_RESOURCES));
		List<Service> list = new ArrayList<>();

		List<AnyObjectTO> services;

		if (roleName.isEmpty()) {
			services = syncopeUtilities.getServicesByNameContains("");
		} else {
			services = syncopeUtilities.getServicesByRoleName(roleName);
		}

		for (AnyObjectTO service : services) {
			Service serviceRecord = new Service();

			serviceRecord.setKey(service.getKey());

			serviceRecord.setName(service.getName());
			LOG.info("SERVICE " + service.getName() + " (" + service.getKey() + ")");
			Iterator<AttrTO> iterator = service.getPlainAttrs().iterator();
			while (iterator.hasNext()) {
				try {
					AttrTO attr = iterator.next();
					if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.INTERFACE_DESCRIPTION_CONTENT)) {
						LOG.info("        SCHEMA -- " + attr.getSchema() + "");
						serviceRecord.setInterfaceDescriptionContent(Base64Utility.decode(attr.getValues().get(0)));
						createWSDL(serviceRecord.getName(), serviceRecord.getInterfaceDescriptionContent());

					} else {
						LOG.info("        SCHEMA -- " + attr.getSchema() + " >> ");
					}
				} catch (Base64Exception e1) {
				}
			}

			for (RelationshipTO relationshipTO : service.getRelationships()) {
				LOG.info("        Service Relationships >> " + relationshipTO.getRightKey());
			}
			list.add(serviceRecord);
		}
		return list;
	}

	private static List<ServiceRole> getAllServiceRoles() {
		List<ServiceRole> list = new ArrayList<>();

		List<AnyObjectTO> roles = syncopeUtilities.getRolesByNameContains("");

		for (AnyObjectTO role : roles) {
			ServiceRole serviceRole = new ServiceRole();

			serviceRole.setKey(role.getKey());
			LOG.info("ROLE " + role.getName() + " (" + role.getKey() + ")");

			Iterator<AttrTO> iterator = role.getPlainAttrs().iterator();
			while (iterator.hasNext()) {

				AttrTO attr = iterator.next();
				if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.SERVICE_ROLE_DESCRIPTION)) {
					LOG.info("        SCHEMA -- " + attr.getSchema() + " >> " + attr.getValues().get(0));
					serviceRole.setDescription(attr.getValues().get(0));
				} else {
					LOG.info("        SCHEMA -- " + attr.getSchema() + " >> ");
				}

			}
			list.add(serviceRole);
		}
		return list;
	}

	private static List<EnactmentEngine> getAllEnactmentEngines() {
		List<EnactmentEngine> list = new ArrayList<>();

		List<AnyObjectTO> enactmentEngines = syncopeUtilities.getEnactmentEnginesByNameContains("");

		for (AnyObjectTO enactment : enactmentEngines) {
			EnactmentEngine enactmentEngine = new EnactmentEngine();

			enactmentEngine.setKey(enactment.getKey());
			enactmentEngine.setName(enactment.getName());

			LOG.info("ENACTMENT ENGINE " + enactment.getName() + " (" + enactment.getKey() + ")");

			Iterator<AttrTO> iterator = enactment.getPlainAttrs().iterator();
			while (iterator.hasNext()) {

				AttrTO attr = iterator.next();
				if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.ENACTMENT_ENGINE_URL)) {
					LOG.info("        SCHEMA -- " + attr.getSchema() + " >> " + attr.getValues().get(0));
					enactmentEngine.setLocation(attr.getValues().get(0));
				} else {
					LOG.info("        SCHEMA -- " + attr.getSchema() + " >> ");
				}

			}
			list.add(enactmentEngine);
		}
		return list;
	}

	private static List<EnactmentEngine> getAllChoreographies() {
		List<EnactmentEngine> list = new ArrayList<>();

		List<GroupTO> choreographies = syncopeUtilities.getChoreographiesByNameContains("");

		for (GroupTO choreography : choreographies) {
			// EnactmentEngine enactmentEngine = new EnactmentEngine();

			// enactmentEngine.setKey(choreography.getKey());
			// enactmentEngine.setName(choreography.getName());

			LOG.info("CHOREOGRAPHY " + choreography.getName() + " (" + choreography.getKey() + ")");

			Iterator<AttrTO> iterator = choreography.getPlainAttrs().iterator();
			while (iterator.hasNext()) {
				AttrTO attr = iterator.next();
				LOG.info("        SCHEMA -- " + attr.getSchema() + " >> " + attr.getValues().get(0));

			}
			// list.add(enactmentEngine);
		}
		return list;
	}

	private static void createWSDL(String name, byte[] content) throws FileNotFoundException, IOException {
		org.apache.commons.io.IOUtils.write(content,
				new FileOutputStream(new File(INTERFACE_OUTPUT_RESOURCES + name + ".wsdl")));
	}

	private static ServiceRole createRole(String roleName, String description) {
		AnyObjectTO role = syncopeUtilities.createRole(roleName, description);

		ServiceRole serviceRole = new ServiceRole();

		serviceRole.setKey(role.getKey());
		serviceRole.setName(role.getName());

		Iterator<AttrTO> iterator = role.getPlainAttrs().iterator();
		while (iterator.hasNext()) {

			AttrTO attr = iterator.next();
			if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.SERVICE_ROLE_DESCRIPTION)) {
				serviceRole.setDescription(attr.getValues().get(0));
			}

		}

		return serviceRole;

	}

	private static ServiceAuthenticationType getServiceAuthenticationType(File securityFile) {
		SecurityPackageImpl.init();
		URI securityModelURI = URI.createURI(securityFile.toURI().toString());
		Resource resource = new XMIResourceFactoryImpl().createResource(securityModelURI);

		try {
			// load the resource
			resource.load(null);

		} catch (IOException e) {
			LOG.info("An error occurred loading resource: " + securityModelURI);
		}

		SecurityModel securityModel = (SecurityModel) resource.getContents().get(0);

		if (securityModel.getSecuritypolicyset().getAuthentication().get(0).getAuthNElement() == null) {
			return ServiceAuthenticationType.NONE;
		} else if (securityModel.getSecuritypolicyset().getAuthentication().get(0)
				.getAuthNTypeForwarded() == AuthenticationTypeForwarded.GENERIC_ACCOUNT) {
			return ServiceAuthenticationType.SHARED;
		} else if (securityModel.getSecuritypolicyset().getAuthentication().get(0)
				.getAuthNTypeForwarded() == AuthenticationTypeForwarded.USER_ACCOUNT) {
			return ServiceAuthenticationType.PER_USER;
		}

		return ServiceAuthenticationType.NONE;
	}

}
