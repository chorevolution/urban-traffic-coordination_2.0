package eu.chorevolution.urbantrafficcoordination.seada.bc.nd;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationRequestType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoSpeedRouteInformationResponseType;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.NDPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.NDService;

public class BcNDREST {

	private static Logger logger = LoggerFactory.getLogger(BcNDREST.class);
	
	private static NDService cdNDService;
	private static NDPT cdNDPT;
	
	static{
		cdNDService = new NDService();
		cdNDPT = cdNDService.getNDPort();
		Client client = ClientProxy.getClient(cdNDPT);
		HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(0);
		httpClientPolicy.setReceiveTimeout(0);
		httpConduit.setClient(httpClientPolicy);		
	}
	
	@Path("/getEcoRoutes")
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoRoutesResponseType getEcoRoutes(EcoRoutesRequest ecoRoutesRequest){
	
		logger.info("CALLED getEcoRoutes ON BC-ND-REST");			
		// create ecoRoutesRequestType
		EcoRoutesRequestType ecoRoutesRequestType = new EcoRoutesRequestType();
		ecoRoutesRequestType.setMessageData(ecoRoutesRequest);
		// call getEcoRoutes on cdND
		return cdNDPT.getEcoRoutes(ecoRoutesRequestType);
	}

	@Path("/getEcoSpeedRouteInformation")	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public EcoSpeedRouteInformationResponseType getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequestType ecoSpeedRouteInformationRequestType){
		
		logger.info("CALLED getEcoSpeedRouteInformation ON BC-ND-REST");
		return cdNDPT.getEcoSpeedRouteInformation(ecoSpeedRouteInformationRequestType);
	}
	
}
