
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeSegmentCongestionStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeSegmentCongestionStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="routeSegment" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegment"/&gt;
 *         &lt;element name="congestionLevel" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="speed" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeSegmentCongestionStatus", propOrder = {
    "routeSegment",
    "congestionLevel",
    "speed"
})
public class RouteSegmentCongestionStatus {

    @XmlElement(required = true)
    protected RouteSegment routeSegment;
    protected int congestionLevel;
    protected int speed;

    /**
     * Gets the value of the routeSegment property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegment }
     *     
     */
    public RouteSegment getRouteSegment() {
        return routeSegment;
    }

    /**
     * Sets the value of the routeSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegment }
     *     
     */
    public void setRouteSegment(RouteSegment value) {
        this.routeSegment = value;
    }

    /**
     * Gets the value of the congestionLevel property.
     * 
     */
    public int getCongestionLevel() {
        return congestionLevel;
    }

    /**
     * Sets the value of the congestionLevel property.
     * 
     */
    public void setCongestionLevel(int value) {
        this.congestionLevel = value;
    }

    /**
     * Gets the value of the speed property.
     * 
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * Sets the value of the speed property.
     * 
     */
    public void setSpeed(int value) {
        this.speed = value;
    }

}
