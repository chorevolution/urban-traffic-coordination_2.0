package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.wrapper;

import javax.xml.bind.annotation.XmlRootElement;

import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.ChoreographyInstanceRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client.EcoRoutesResponse;

@XmlRootElement
public class EcoRoutesResponseReturnType{
	
    private ChoreographyInstanceRequest choreographyId;
    private EcoRoutesResponse ecoRoutesResponse;
       
	public EcoRoutesResponseReturnType(){
	}    
    
	public EcoRoutesResponseReturnType(ChoreographyInstanceRequest choreographyId,
			EcoRoutesResponse ecoRoutesResponse) {
		this.choreographyId = choreographyId;
		this.ecoRoutesResponse = ecoRoutesResponse;
	}


	public ChoreographyInstanceRequest getChoreographyId() {
		return choreographyId;
	}


	public void setChoreographyId(ChoreographyInstanceRequest choreographyId) {
		this.choreographyId = choreographyId;
	}


	public EcoRoutesResponse getEcoRoutesResponse() {
		return ecoRoutesResponse;
	}


	public void setEcoRoutesResponse(EcoRoutesResponse ecoRoutesResponse) {
		this.ecoRoutesResponse = ecoRoutesResponse;
	}
   
	
    
}
