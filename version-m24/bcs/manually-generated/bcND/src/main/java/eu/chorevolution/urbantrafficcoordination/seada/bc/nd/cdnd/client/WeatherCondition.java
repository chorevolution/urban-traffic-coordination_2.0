
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for weatherCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="weatherCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="roadTemperature" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="airTemperature" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="airRelativeHumidity" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="windForce" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "weatherCondition", propOrder = {
    "roadTemperature",
    "airTemperature",
    "airRelativeHumidity",
    "windForce"
})
public class WeatherCondition {

    protected double roadTemperature;
    protected double airTemperature;
    protected double airRelativeHumidity;
    protected double windForce;

    /**
     * Gets the value of the roadTemperature property.
     * 
     */
    public double getRoadTemperature() {
        return roadTemperature;
    }

    /**
     * Sets the value of the roadTemperature property.
     * 
     */
    public void setRoadTemperature(double value) {
        this.roadTemperature = value;
    }

    /**
     * Gets the value of the airTemperature property.
     * 
     */
    public double getAirTemperature() {
        return airTemperature;
    }

    /**
     * Sets the value of the airTemperature property.
     * 
     */
    public void setAirTemperature(double value) {
        this.airTemperature = value;
    }

    /**
     * Gets the value of the airRelativeHumidity property.
     * 
     */
    public double getAirRelativeHumidity() {
        return airRelativeHumidity;
    }

    /**
     * Sets the value of the airRelativeHumidity property.
     * 
     */
    public void setAirRelativeHumidity(double value) {
        this.airRelativeHumidity = value;
    }

    /**
     * Gets the value of the windForce property.
     * 
     */
    public double getWindForce() {
        return windForce;
    }

    /**
     * Sets the value of the windForce property.
     * 
     */
    public void setWindForce(double value) {
        this.windForce = value;
    }

}
