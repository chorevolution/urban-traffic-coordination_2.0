
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeSegmentData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeSegmentData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="routeSegment" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegment"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeSegmentData", propOrder = {
    "routeSegment"
})
public class RouteSegmentData {

    @XmlElement(required = true)
    protected RouteSegment routeSegment;

    /**
     * Gets the value of the routeSegment property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegment }
     *     
     */
    public RouteSegment getRouteSegment() {
        return routeSegment;
    }

    /**
     * Sets the value of the routeSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegment }
     *     
     */
    public void setRouteSegment(RouteSegment value) {
        this.routeSegment = value;
    }

}
