
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for routeSegmentCongestionStatusData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeSegmentCongestionStatusData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="routeSegmentCongestionStatus" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}routeSegmentCongestionStatus"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeSegmentCongestionStatusData", propOrder = {
    "routeSegmentCongestionStatus"
})
public class RouteSegmentCongestionStatusData {

    @XmlElement(required = true)
    protected RouteSegmentCongestionStatus routeSegmentCongestionStatus;

    /**
     * Gets the value of the routeSegmentCongestionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link RouteSegmentCongestionStatus }
     *     
     */
    public RouteSegmentCongestionStatus getRouteSegmentCongestionStatus() {
        return routeSegmentCongestionStatus;
    }

    /**
     * Sets the value of the routeSegmentCongestionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteSegmentCongestionStatus }
     *     
     */
    public void setRouteSegmentCongestionStatus(RouteSegmentCongestionStatus value) {
        this.routeSegmentCongestionStatus = value;
    }

}
