
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bridgeStatusInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bridgeStatusInformationResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="closureStatus" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}closureStatus"/&gt;
 *         &lt;element name="nextClosure" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}nextClosure"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bridgeStatusInformationResponse", propOrder = {
    "closureStatus",
    "nextClosure"
})
public class BridgeStatusInformationResponse {

    @XmlElement(required = true)
    protected ClosureStatus closureStatus;
    @XmlElement(required = true)
    protected NextClosure nextClosure;

    /**
     * Gets the value of the closureStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ClosureStatus }
     *     
     */
    public ClosureStatus getClosureStatus() {
        return closureStatus;
    }

    /**
     * Sets the value of the closureStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClosureStatus }
     *     
     */
    public void setClosureStatus(ClosureStatus value) {
        this.closureStatus = value;
    }

    /**
     * Gets the value of the nextClosure property.
     * 
     * @return
     *     possible object is
     *     {@link NextClosure }
     *     
     */
    public NextClosure getNextClosure() {
        return nextClosure;
    }

    /**
     * Sets the value of the nextClosure property.
     * 
     * @param value
     *     allowed object is
     *     {@link NextClosure }
     *     
     */
    public void setNextClosure(NextClosure value) {
        this.nextClosure = value;
    }

}
