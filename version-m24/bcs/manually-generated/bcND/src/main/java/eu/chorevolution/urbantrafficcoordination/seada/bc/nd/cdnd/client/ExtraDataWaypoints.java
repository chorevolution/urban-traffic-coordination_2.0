
package eu.chorevolution.urbantrafficcoordination.seada.bc.nd.cdnd.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for extraDataWaypoints complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="extraDataWaypoints"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="extraDataWaypointsInformation" type="{http://eu.chorevolution.urbantrafficcoordination.seada/cdnd}extraDataWaypoint"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extraDataWaypoints", propOrder = {
    "extraDataWaypointsInformation"
})
public class ExtraDataWaypoints {

    protected List<ExtraDataWaypoint> extraDataWaypointsInformation;

    /**
     * Gets the value of the extraDataWaypointsInformation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extraDataWaypointsInformation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtraDataWaypointsInformation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtraDataWaypoint }
     * 
     * 
     */
    public List<ExtraDataWaypoint> getExtraDataWaypointsInformation() {
        if (extraDataWaypointsInformation == null) {
            extraDataWaypointsInformation = new ArrayList<ExtraDataWaypoint>();
        }
        return this.extraDataWaypointsInformation;
    }

}
