package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.DTSACCIDENTSPT;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;

@Component(value = "DTSACCIDENTSPTImpl")
public class DTSACCIDENTSPTImpl implements DTSACCIDENTSPT{

	private static Logger logger = LoggerFactory.getLogger(DTSACCIDENTSPTImpl.class);

	@Autowired
	private DTSAccidentsService service;	
	
	@Override
	public AccidentInformationResponse waypointAccidentInformation(AccidentInformationRequest parameters) {
		
		logger.info("CALLED waypointAccidentInformation ON bcDTS-ACCIDENTS");
		logger.info("START TIME OPERATION waypointAccidentInformation ON bcDTS-ACCIDENTS: "+System.currentTimeMillis());
		AccidentInformationResponse accidentInformationResponse = service.getWaypointAccidentInformation(parameters);
		logger.info("END TIME OPERATION waypointAccidentInformation ON bcDTS-ACCIDENTS: "+System.currentTimeMillis());		
		return accidentInformationResponse;	
	}
	
}
