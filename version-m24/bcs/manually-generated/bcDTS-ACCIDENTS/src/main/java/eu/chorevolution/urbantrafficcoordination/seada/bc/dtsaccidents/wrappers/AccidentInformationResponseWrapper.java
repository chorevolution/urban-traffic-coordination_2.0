package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationResponse;

public class AccidentInformationResponseWrapper {

	private AccidentInformationResponse accidentInformationResponse;

	public AccidentInformationResponse getAccidentInformationResponse() {
		return accidentInformationResponse;
	}

	public void setAccidentInformationResponse(AccidentInformationResponse accidentInformationResponse) {
		this.accidentInformationResponse = accidentInformationResponse;
	}
	
	
}
