package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationRequest;

public class AccidentInformationRequestWrapper {
	
	private AccidentInformationRequest accidentInformationRequest;

	public AccidentInformationRequest getAccidentInformationRequest() {
		return accidentInformationRequest;
	}

	public void setAccidentInformationRequest(AccidentInformationRequest accidentInformationRequest) {
		this.accidentInformationRequest = accidentInformationRequest;
	}

}
