package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformation;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationData;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.AccidentInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsaccidents.business.DTSAccidentsService;

@Service
public class DummyDTSAccidentsServiceImpl implements DTSAccidentsService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSAccidentsServiceImpl.class);
	
	@Override
	public AccidentInformationResponse getWaypointAccidentInformation(AccidentInformationRequest parameters) {
		
		logger.info("CALLED getWaypointAccidentInformation ON DUMMY bcDTS-ACCIDENTS");	
		
		AccidentInformationResponse accidentInformationResponse = new AccidentInformationResponse();
		AccidentInformationData accidentInformationData = new AccidentInformationData();
		AccidentInformation accidentInformation = new AccidentInformation();
		accidentInformation.setDescription("description");
		accidentInformationData.setAccidentInformation(accidentInformation);
		accidentInformationResponse.getAccidents().add(accidentInformationData);
		return new AccidentInformationResponse();
	}
}
