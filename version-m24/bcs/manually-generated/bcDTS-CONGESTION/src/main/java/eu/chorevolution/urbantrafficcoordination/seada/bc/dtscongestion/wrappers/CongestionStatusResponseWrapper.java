package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusResponse;

public class CongestionStatusResponseWrapper {

	private CongestionStatusResponse congestionStatusResponse;

	public CongestionStatusResponse getCongestionStatusResponse() {
		return congestionStatusResponse;
	}

	public void setCongestionStatusResponse(CongestionStatusResponse congestionStatusResponse) {
		this.congestionStatusResponse = congestionStatusResponse;
	}
	
}
