package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business.DTSCongestionService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers.CongestionStatusRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers.CongestionStatusResponseWrapper;

@Service
public class RESTDTSCongestionServiceImpl implements DTSCongestionService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSCongestionServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;	
	
	@Override
	public CongestionStatusResponse getRouteCongestionInformation(CongestionStatusRequest congestionStatusRequest) {
		
		logger.info("CALLED getRouteCongestionInformation ON REST bcDTS-CONGESTION");	
		CongestionStatusResponse response = null;	
		try {
			Gson gson = new GsonBuilder().create();
			HttpClient httpClient = HttpClientBuilder.create().build();			
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			CongestionStatusRequestWrapper congestionStatusRequestWrapper = new CongestionStatusRequestWrapper();
			congestionStatusRequestWrapper.setCongestionStatusRequest(congestionStatusRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(congestionStatusRequestWrapper), ContentType.APPLICATION_JSON);	
			postRequest.setEntity(requestEntity);				
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String json = EntityUtils.toString(httpResponse.getEntity());
			System.out.println(json);
			CongestionStatusResponseWrapper congestionStatusResponseWrapper = gson.fromJson(json, CongestionStatusResponseWrapper.class);
			response = congestionStatusResponseWrapper.getCongestionStatusResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
	}

}
