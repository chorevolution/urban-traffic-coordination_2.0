package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.business;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusResponse;

public interface DTSCongestionService {

	CongestionStatusResponse getRouteCongestionInformation(CongestionStatusRequest parameters); 
}
