package eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.wrappers;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtscongestion.CongestionStatusRequest;

public class CongestionStatusRequestWrapper {

	private CongestionStatusRequest congestionStatusRequest;

	public CongestionStatusRequest getCongestionStatusRequest() {
		return congestionStatusRequest;
	}

	public void setCongestionStatusRequest(CongestionStatusRequest congestionStatusRequest) {
		this.congestionStatusRequest = congestionStatusRequest;
	}
		
}
