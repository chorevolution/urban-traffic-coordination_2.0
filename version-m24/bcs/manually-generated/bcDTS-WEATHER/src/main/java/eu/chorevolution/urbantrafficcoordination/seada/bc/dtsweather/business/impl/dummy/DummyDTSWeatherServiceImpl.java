package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.WeatherInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsweather.business.DTSWeatherService;

@Service
public class DummyDTSWeatherServiceImpl implements DTSWeatherService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSWeatherServiceImpl.class);	
	
	@Override
	public WeatherInformationResponse getWaypointWeatherInformation(WeatherInformationRequest parameters) {

		logger.info("CALLED getWaypointWeatherInformation ON DUMMY bcDTS-WEATHER");	
		WeatherInformationResponse weatherInformationResponse = new WeatherInformationResponse();
		weatherInformationResponse.setAirRelativeHumidity(1);
		return weatherInformationResponse;
	}
}
