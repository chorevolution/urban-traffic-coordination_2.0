package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.impl.dummy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.ClosureStatus;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;

@Service
public class DummyDTSBridgeServiceImpl implements DTSBridgeService{

	private static Logger logger = LoggerFactory.getLogger(DummyDTSBridgeServiceImpl.class);	
	
	@Override
	public BridgeStatusInformationResponse getRouteSegmentBridgeStatusInformation(BridgeStatusInformationRequest parameters) {

		logger.info("CALLED getBridgeStatusRequest ON DUMMY bcDTS-BRIDGE");	
		BridgeStatusInformationResponse bridgeStatusInformationResponse = new BridgeStatusInformationResponse();
		ClosureStatus closureStatus = new ClosureStatus();
		bridgeStatusInformationResponse.setClosureStatus(closureStatus);
		return bridgeStatusInformationResponse;
	}

}
