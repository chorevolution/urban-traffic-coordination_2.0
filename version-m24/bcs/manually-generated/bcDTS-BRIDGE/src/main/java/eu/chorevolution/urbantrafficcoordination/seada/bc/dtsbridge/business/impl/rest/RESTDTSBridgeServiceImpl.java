package eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationRequest;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.BridgeStatusInformationResponse;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.business.DTSBridgeService;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers.BridgeStatusInformationRequestWrapper;
import eu.chorevolution.urbantrafficcoordination.seada.bc.dtsbridge.wrappers.BridgeStatusInformationResponseWrapper;

@Service
public class RESTDTSBridgeServiceImpl implements DTSBridgeService{

	private static Logger logger = LoggerFactory.getLogger(RESTDTSBridgeServiceImpl.class);
	
	@Value("#{cfgproperties.host}")
	private String host;

	@Value("#{cfgproperties.port}")
	private int port;

	@Value("#{cfgproperties.path}")
	private String path;
	
	@Override
	public BridgeStatusInformationResponse getRouteSegmentBridgeStatusInformation(BridgeStatusInformationRequest bridgeStatusInformationRequest) {

		logger.info("CALLED getBridgeStatusRequest ON REST bcDTS-BRIDGE");	
		BridgeStatusInformationResponse response = null;		
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			Gson gson = new GsonBuilder().create();
			URI uri = new URIBuilder()
					.setScheme("http")
					.setHost(host)
					.setPort(port)
					.setPath(path)
					.build();
			HttpPost postRequest = new HttpPost(uri);
			BridgeStatusInformationRequestWrapper bridgeStatusInformationRequestWrapper = new BridgeStatusInformationRequestWrapper();
			bridgeStatusInformationRequestWrapper.setBridgeStatusInformationRequest(bridgeStatusInformationRequest);
			StringEntity requestEntity = new StringEntity(gson.toJson(bridgeStatusInformationRequestWrapper), ContentType.APPLICATION_JSON);	
			postRequest.setEntity(requestEntity);			
			HttpResponse httpResponse = httpClient.execute(postRequest);
			String json = EntityUtils.toString(httpResponse.getEntity());
			BridgeStatusInformationResponseWrapper bridgeStatusInformationResponseWrapper = gson.fromJson(json, BridgeStatusInformationResponseWrapper.class);
			response = bridgeStatusInformationResponseWrapper.getBridgeStatusInformationResponse();
		} catch (URISyntaxException | IOException e) {
			logger.error(e.getMessage(),e);
		}
		return response;
	}
}
