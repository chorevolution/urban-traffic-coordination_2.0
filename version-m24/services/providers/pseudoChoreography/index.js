var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var request = require('request');
var converters = require('./converters');
var trafficService = require('./trafficService');
async = require("async");

var portNumber = 3005;
var app = express()
app.use(bodyParser.json());


app.post('/getEcoRoutes', function(req, res) {
    //console.log(req.body);
    var origin = req.body.ecoRoutesRequest.origin;
    var destination = req.body.ecoRoutesRequest.destination;
    var urlGoogle = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.lat + "," + origin.lon + "&" +
        "destination=" + destination.lat + "," + destination.lon + "&key=" + "AIzaSyBhfNR1PHo8EsuxjLr3EO-sNnfoUDh4ilw" + "&alternatives=true";
    var urlHere = "https://route.cit.api.here.com/routing/7.2/calculateroute.json?waypoint0=" + origin.lat + "," + origin.lon + "&waypoint1=" +
        destination.lat + "," + destination.lon + "&mode=fastest;car;traffic:enabled&app_id=" + "k0YXz4I407cw0jDmwdf9" + "&" +
        "app_code=" + "MhWNbYvql9cQeswuIYUCfg" + "&departure=now";
    var routes = [];
    request(urlGoogle, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            routes.push(converters.googleResponseToChoreographyRoute(response));
            //console.log(converters.googleResponseToChoreographyRoute(response));
        }
        request(urlHere, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                routes.push(converters.hereResponseToChoreographyRoute(response));
            }
            routes = converters.routesSuggestionsToRoutes(routes);
            var result = getTrafficData(routes, res);            
        });
    });

});

var getTrafficData = function(routes, response){
    var routeRequest = {};
    return trafficService.getCongestions(converters.routesToCongestionStatusRequests(routes), response);
}

app.post('/getEcoSpeedRouteInformation', function(req, res) {
    console.log("restarted");
});

app.listen(portNumber);
console.log('server is listening on port ' + portNumber);
