var googleResponseToChoreographyRoute = function(googleResp) {
    var googleRoutes = JSON.parse(googleResp.body).routes;
    var routes = [];
    var waypointId = 0;
    var routeId = 0;
    googleRoutes.forEach(function(element, indexRoute) {
        var route = {};
        var waypoints = [];
        element.legs.forEach(function(leg, indexLeg) {
            leg.steps.forEach(function(step, indexStep) {
                if (indexStep == 0) {
                    waypoints.push(buildWaypointInfo(step.start_location, waypointId++));
                }
                waypoints.push(buildWaypointInfo(step.end_location, waypointId++, step.polyline.points));
                //console.log(decodeLine(step.polyline.points));
            })
        })
        route.waypoints = waypoints;
        route.id = routeId++;
        routes.push(route);
    });
    var result = {};
    result.routesSuggestion = {};
    result.routesSuggestion.routes = routes;
    return result;
};

var buildWaypointInfo = function(location, waypointId, path) {
    var waypoint = {};
    waypoint.lat = location.lat;
    waypoint.lon = location.lng;
    var waypointInfo = {};
    waypointInfo.waypoint = waypoint;
    waypointInfo.id = waypointId;
    waypointInfo.path = path;
    return waypointInfo;
}

var hereResponseToChoreographyRoute = function(hereResp) {
    var hereRoutes = JSON.parse(hereResp.body).response.route;
    //console.log(hereResp.body);
    var routes = [];
    var waypointId = 0;
    var routeId = 0;
    hereRoutes.forEach(function(element, indexRoute) {
        var route = {};
        var waypoints = [];
        element.leg.forEach(function(leg, indexLeg) {
            leg.maneuver.forEach(function(maneuver, indexStep) {
                var waypoint = {};
                waypoint.lat = maneuver.position.latitude;
                waypoint.lon = maneuver.position.longitude;
                var waypointInfo = {};
                waypointInfo.waypoint = waypoint;
                waypointInfo.id = waypointId++;
                waypoints.push(waypointInfo);
            });
        })
        route.waypoints = waypoints;
        route.id = routeId++;
        routes.push(route);
    });
    var result = {};
    result.routesSuggestion = {};
    result.routesSuggestion.routes = routes;
    return result;
};

var routesSuggestionsToRoutes = function(routeSuggestions) {
    var routes = [];
    routeSuggestions.forEach(function(element) {
        element.routesSuggestion.routes.forEach(function(waypointSequence) {
            var route = [];
            var segment = {};
            segment.start = {};
            segment.end = {};
            waypointSequence.waypoints.forEach(function(element, index) {
                if (index == 0) {
                    segment.start.lon = element.waypoint.lon;
                    segment.start.lat = element.waypoint.lat;
                } else {
                    segment.start = JSON.parse(JSON.stringify(segment.end)); //here we do deep cloning of the segment.end
                    segment.end.lat = element.waypoint.lat;
                    segment.end.lon = element.waypoint.lon;
                    segment.path = element.path;
                    route.push(JSON.parse(JSON.stringify(segment))); //here we do deep cloning of the segment
                }
            });
            routes.push(route);
        })
    });
    return routes;

}

var routesToCongestionStatusRequests = function(routes) {
    var congestionStatusRequests = [];
    routes.forEach(function(element) {
        var congestionStatusRequest = {};
        congestionStatusRequest.routeSegments = [];
        element.forEach(function(segment) {
            var routeSegment = {};
            routeSegment.routeSegment = segment; // actions in order to transform one json object to another one            
            congestionStatusRequest.routeSegments.push(routeSegment);
        });
        var routeReq = {};
        routeReq.congestionStatusRequest = congestionStatusRequest;
        congestionStatusRequests.push(routeReq);
    });
    return congestionStatusRequests;
}

var congestionStatusResponseToSEARPResponse = function(congestions) { //transformation to displayAndShowSuggestedRoutesMessageResponse without corresponding weather/accident/bridge data
    var searpResponses = [];
    congestions.forEach(function(element) {
        var searpResponse = {};
        searpResponse.ecoSpeedRouteSegmentsInformation = [];
        element.congestionStatusResponse.routeSegmentsCongestionStatus.forEach(function(csElement) {
            var routeSegment = {
                start: {
                    lat: csElement.routeSegmentCongestionStatus.routeSegment.start.lat,
                    lon: csElement.routeSegmentCongestionStatus.routeSegment.start.lon
                },
                end: {
                    lat: csElement.routeSegmentCongestionStatus.routeSegment.end.lat,
                    lon: csElement.routeSegmentCongestionStatus.routeSegment.end.lon
                },
                path: csElement.routeSegmentCongestionStatus.routeSegment.path
            };
            var segment = {
                routeSegment: routeSegment,
                congestionLevel: csElement.routeSegmentCongestionStatus.congestionLevel,
                ecoSpeed: csElement.routeSegmentCongestionStatus.speed
            };
            //Here estimated emission and ecolevel are supposed to be set
            searpResponse.ecoSpeedRouteSegmentsInformation.push(segment);
        });
        searpResponses.push(searpResponse);
    });
    return searpResponses;
}

var accidentsResponseArrayToExtraDataWaypointsInfo = function(accidentsResponseArray) { //transformation to displayAndShowSuggestedRoutesMessageResponse without corresponding weather/accident/bridge data
    var extraDataWaypointsInformation = [];
    if (accidentsResponseArray != undefined) {
        accidentsResponseArray.forEach(function(element) {
            var extraDataWaypoint = {};
            extraDataWaypoint.longitude = element.accidentInformation.longitude;
            extraDataWaypoint.latitude = element.accidentInformation.latitude;
            extraDataWaypoint.situationType = element.accidentInformation.messageCodeValue;
            extraDataWaypointsInformation.push(extraDataWaypoint);
        });
    }

    return extraDataWaypointsInformation;
}




module.exports.accidentsResponseArrayToExtraDataWaypointsInfo = accidentsResponseArrayToExtraDataWaypointsInfo;
module.exports.googleResponseToChoreographyRoute = googleResponseToChoreographyRoute;
module.exports.hereResponseToChoreographyRoute = hereResponseToChoreographyRoute;
module.exports.routesSuggestionsToRoutes = routesSuggestionsToRoutes;
module.exports.routesToCongestionStatusRequests = routesToCongestionStatusRequests;
module.exports.congestionStatusResponseToSEARPResponse = congestionStatusResponseToSEARPResponse;
