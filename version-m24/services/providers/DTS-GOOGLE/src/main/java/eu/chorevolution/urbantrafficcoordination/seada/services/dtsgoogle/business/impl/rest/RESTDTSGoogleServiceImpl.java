package eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.impl.rest;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.Route;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.RoutesRequest;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.RoutesSuggestion;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.Waypoint;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.WaypointInfo;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.BusinessException;
import eu.chorevolution.urbantrafficcoordination.seada.services.dtsgoogle.business.DTSGoogleService;



@Service
public class RESTDTSGoogleServiceImpl implements DTSGoogleService {
	
	private static Logger logger = LoggerFactory.getLogger(RESTDTSGoogleServiceImpl.class);

	@Value("#{cfgproperties.apikey}")
	private String API_KEY;
	@Value("#{cfgproperties.dtsgoogleurl}")	
	private String dtsgoogleurl;
	
	@Override
	public RoutesSuggestion getRoutes(RoutesRequest routesRequest) throws BusinessException {
				
		logger.info("CALLED getRoutes ON REST DTS-GOOGLE");			
        logger.info("Getting routes (origin: " + routesRequest.getOrigin() + " --> " + routesRequest.getDestination() + ")");

		List<Route> routes = new LinkedList<Route>();		
        try 
        {	
            //Time to start to query GOOGLE API!
            URL url = new URL(dtsgoogleurl +
                    "origin=" + routesRequest.getOrigin().getLat() + "," + routesRequest.getOrigin().getLon() + "&" +
                    "destination=" + routesRequest.getDestination().getLat() + "," + routesRequest.getDestination().getLon() + "&" + 
                    "key=" + API_KEY + "&" + 
                    "alternatives=true");

            logger.error("URL = " + url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-length", "0");
            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.connect();
            int status = connection.getResponseCode();     
            
            logger.info("Status code: " + status);

            //What happened?
            switch (status)
            {
                case 200:
                {
                    //Great -- success -- let's read the response and build a string which we can parse into route(s)
                    JSONTokener tokener = new JSONTokener(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                    JSONObject jsonRoot = new JSONObject(tokener);

                    //Convert JSON into our own list of "Routes"
                    JSONArray jsonRoutes = jsonRoot.getJSONArray("routes");
                    logger.info(jsonRoutes.toString());

                    logger.info("Received " + jsonRoutes.length() + " routes");

                    for (int routeIndex = 0; routeIndex < jsonRoutes.length(); routeIndex++) {
                    	JSONObject jsonRoute = jsonRoutes.getJSONObject(routeIndex);
                        JSONArray jsonLegs = jsonRoute.getJSONArray("legs");

                        //Create route object so that we can fill up with new waypoints as we continue parsing the JSON
                        Route route = new Route();
                        int waypointId = 0;

                        logger.info("Legs; " + jsonLegs);

                        for (int legIndex = 0; legIndex < jsonLegs.length(); legIndex++) {
                        	JSONObject jsonLeg = jsonLegs.getJSONObject(legIndex);
                            JSONArray jsonSteps = jsonLeg.getJSONArray("steps");

                            for (int stepIndex = 0; stepIndex < jsonSteps.length(); stepIndex++) {
                                JSONObject jsonStep = jsonSteps.getJSONObject(stepIndex);

                                //If this is the "first" step of the route, let's add the start location as first WP to the route, 
                                // since we want a Route object to start at our origin and NOT at the end of the first leg.
                                if (route.getWaypoints().size() == 0)
                                {
                                    JSONObject jsonStartLocation = jsonStep.getJSONObject("start_location");
                                    double startLocationLatitude = jsonStartLocation.getDouble("lat");
                                    double startLocationLongitude = jsonStartLocation.getDouble("lng");

                                    Waypoint waypoint = new Waypoint();
                                    waypoint.setLat(startLocationLatitude);
                                    waypoint.setLon(startLocationLongitude);

                                    WaypointInfo waypointInfo = new WaypointInfo();
                                    waypointInfo.setWaypoint(waypoint);
                                    waypointInfo.setId(waypointId++);

                                    route.getWaypoints().add(waypointInfo);
                                }
                                
                                JSONObject jsonEndLocation = jsonStep.getJSONObject("end_location");
                                double endLocationLatitude = jsonEndLocation.getDouble("lat");
                                double endLocationLongitude = jsonEndLocation.getDouble("lng");

                                //String maneuver = jsonStep.has("maneuver") ? jsonStep.getString("maneuver") : null;
                                //String travelMode = jsonStep.getString("travel_mode");
                                String htmlInstructions = jsonStep.getString("html_instructions");

                                //Create waypoint and info-object to wrap up and represent what we've found
                                Waypoint waypoint = new Waypoint();
                                waypoint.setLat(endLocationLatitude);
                                waypoint.setLon(endLocationLongitude);

                                WaypointInfo waypointInfo = new WaypointInfo();
                                waypointInfo.setWaypoint(waypoint);
                                waypointInfo.setInstruction(htmlInstructions);
                                waypointInfo.setId(waypointId++);

                                //.. and then simply append to the ongoing route
                                route.getWaypoints().add(waypointInfo);
                            }
                        }
                        
                        //Route finished -- no more info, so let's archive the route and proceed
                        routes.add(route);
                    }

                    break;

                }
                default:
                {
                    //Opps -- unexpected status-code returned from server...
                	throw new BusinessException("UNEXPECTED STATUS-CODE RETURNED FROM SERVER");

                }
            }
            
            //Close connection -- we're done
            connection.disconnect();
        }
        catch (Exception e) {
        	throw new BusinessException(e.getMessage());
        }
        RoutesSuggestion routesSuggestion = new RoutesSuggestion();
        routesSuggestion.getRoutes().addAll(routes);
        return routesSuggestion;
	}

}
