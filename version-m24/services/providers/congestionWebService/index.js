var express = require('express');
var bodyParser = require('body-parser');
var https = require('https');
var request = require('request');
async = require("async");

xml2js = require("xml2js");

var portNumber = 3004;
var app = express()
app.use(bodyParser.json());

//request example 
/*
{ 
    "congestionStatusRequest":
    [{"start": { "lon": "57.68611907959",
                                "lat": "11.93115234375"},
                     "end": { "lon": "11.93115234375",
                              "lat": "57.68611907959"}  
                    },
    {"start": { "lon": "57.736198",
                               "lat": "11.972487"},
                    "end": { "lon": "57.748813",
                             "lat": "11.980341"}    
    }]
}
*/
/*response example
{
  "congestionStatusResponse": [
    {
      "start": {
        "lon": "57.68611907959",
        "lat": "11.93115234375"
      },
      "end": {
        "lon": "11.93115234375",
        "lat": "57.68611907959"
      },
      "congestionLevel": -100,
      "speed": -100
    },
    {
      "start": {
        "lon": "57.736198",
        "lat": "11.972487"
      },
      "end": {
        "lon": "57.748813",
        "lat": "11.980341"
      },
      "speed": 30,
      "congestionLevel": 2
    }
  ]
}
*/
app.post('/getCongestion', function(req, res) {
    //console.log(req);
    //console.log(req.body);
    var segments = req.body.congestionStatusRequest.routeSegments; //check if it is an array
    var urls = [];
    var TOMTOM_URL_CONST = 'https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/xml?key=v9byd7y3x9dwgjhzyg36s4pa&point=';
    var UNIT_CONST = '&unit=KMPH';
    segments.forEach(function(element) {
        urls.push(TOMTOM_URL_CONST + element.routeSegment.start.lat + ',' + element.routeSegment.start.lon + UNIT_CONST);
        urls.push(TOMTOM_URL_CONST + element.routeSegment.end.lat + ',' + element.routeSegment.end.lon + UNIT_CONST);
    });
    var results = [];
    var calls = [];
    var resultSegments = [];
    var parser = new xml2js.Parser();
    
    urls.forEach(function(element) {
        calls.push(function(callback) {
            https.get(element, function(resource) {
                resource.setEncoding('binary');
                resource.on('data', function(data) {
                    parser.parseString(data, function(err, result) {
                        results[element] = result;
                        //console.log('called parsing');
                        //console.log(results[element]);
                    });
                    callback();
                });
            });
        });
    });

    async.series(calls, function(err) {
        //console.log(results.length);
        segments.forEach(function(element, index){
            var startURL = TOMTOM_URL_CONST + element.routeSegment.start.lat + ',' + element.routeSegment.start.lon + UNIT_CONST;
            var endURL = TOMTOM_URL_CONST + element.routeSegment.end.lat + ',' + element.routeSegment.end.lon + UNIT_CONST;
            resultSegments[index] = {};
            resultSegments[index].routeSegmentCongestionStatus = element;
            if (results[startURL].errorResponse != undefined || results[endURL].errorResponse != undefined){
                resultSegments[index].routeSegmentCongestionStatus.congestionLevel = -100;
                resultSegments[index].routeSegmentCongestionStatus.speed = -100;
            } else{
                resultSegments[index].routeSegmentCongestionStatus.speed = Math.round((parseFloat(results[startURL].flowSegmentData.currentSpeed[0]) + parseFloat(results[endURL].flowSegmentData.currentSpeed[0]))/2);
                if(resultSegments[index].routeSegmentCongestionStatus.speed < (parseFloat(results[startURL].flowSegmentData.freeFlowSpeed[0]) + parseFloat(results[endURL].flowSegmentData.freeFlowSpeed[0]))/2*0.75){
                    resultSegments[index].routeSegmentCongestionStatus.congestionLevel = 2;
                }else{
                    resultSegments[index].routeSegmentCongestionStatus.congestionLevel = 1;
                }
            }
            
        });

        //building the correct response
        var congestionStatusResponse = {};
        congestionStatusResponse.routeSegmentsCongestionStatus = resultSegments;

        res.json({
            congestionStatusResponse: congestionStatusResponse
        });
    });


});


app.listen(portNumber);
console.log('server is listening on port ' + portNumber);
