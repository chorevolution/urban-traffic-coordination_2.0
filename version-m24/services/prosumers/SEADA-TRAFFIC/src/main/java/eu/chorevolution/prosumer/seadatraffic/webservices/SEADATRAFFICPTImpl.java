package eu.chorevolution.prosumer.seadatraffic.webservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.chorevolution.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusRequest;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveBridgeStatusInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveBridgeStatusInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveCongestionStatusResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveCongestionStatusResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveDestinationAccidentInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveDestinationAccidentInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveDestinationWeatherInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveDestinationWeatherInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveOriginAccidentInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveOriginAccidentInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveOriginWeatherInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.ReceiveOriginWeatherInformationResponseType;
import eu.chorevolution.prosumer.seadatraffic.SEADATRAFFICPT;
import eu.chorevolution.prosumer.seadatraffic.SendRequestType;
import eu.chorevolution.prosumer.seadatraffic.SendRequestTypeWithLoop;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationRequestType;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationResponseReturnType;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationRequestReturnType;
import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyDataService;
import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyInstanceMessagesStore;
import eu.chorevolution.prosumer.seadatraffic.business.SEADATRAFFICService;
import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;


@Component(value="SEADATRAFFICPTImpl")
public class SEADATRAFFICPTImpl implements SEADATRAFFICPT {
	
	private static Logger logger = LoggerFactory.getLogger(SEADATRAFFICPTImpl.class);

	@Autowired
	private SEADATRAFFICService service;
	
	@Autowired
	private ChoreographyDataService choreographyDataService;
	
    @Override
    public TrafficRouteInformationRequestReturnType getRouteTrafficInformation(TrafficRouteInformationRequestType parameters) {
    	logger.info("CALLED getRouteTrafficInformation ON SEADA-TRAFFIC");	
    	TrafficRouteInformationRequestReturnType result = new TrafficRouteInformationRequestReturnType();
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), parameters.getInputMessageData());
  		service.getRouteTrafficInformation(parameters.getInputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		
		return result;
    }
     
    @Override
    public TrafficRouteInformationResponseReturnType sendTrafficRouteInformationResponse(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendTrafficRouteInformationResponse ON SEADA-TRAFFIC");	
    	logger.info("sendTrafficRouteInformationResponse - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendTrafficRouteInformationResponse - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		TrafficRouteInformationResponse businessResult = service.createTrafficRouteInformationResponse(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		TrafficRouteInformationResponseReturnType result = new TrafficRouteInformationResponseReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public CongestionStatusRequestReturnType sendCongestionStatusRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendCongestionStatusRequest ON SEADA-TRAFFIC");	
    	logger.info("sendCongestionStatusRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendCongestionStatusRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		CongestionStatusRequest businessResult = service.createCongestionStatusRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		CongestionStatusRequestReturnType result = new CongestionStatusRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public BridgeStatusInformationRequestReturnType sendBridgeStatusInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendBridgeStatusInformationRequest ON SEADA-TRAFFIC");	
    	logger.info("sendBridgeStatusInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendBridgeStatusInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		BridgeStatusInformationRequest businessResult = service.createBridgeStatusInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		BridgeStatusInformationRequestReturnType result = new BridgeStatusInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public AccidentInformationRequestReturnType sendOriginAccidentInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendOriginAccidentInformationRequest ON SEADA-TRAFFIC");	
    	logger.info("sendOriginAccidentInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendOriginAccidentInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		AccidentInformationRequest businessResult = service.createOriginAccidentInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		AccidentInformationRequestReturnType result = new AccidentInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public WeatherInformationRequestReturnType sendOriginWeatherInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendOriginWeatherInformationRequest ON SEADA-TRAFFIC");	
    	logger.info("sendOriginWeatherInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendOriginWeatherInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		WeatherInformationRequest businessResult = service.createOriginWeatherInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		WeatherInformationRequestReturnType result = new WeatherInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public AccidentInformationRequestReturnType sendDestinationAccidentInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendDestinationAccidentInformationRequest ON SEADA-TRAFFIC");	
    	logger.info("sendDestinationAccidentInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendDestinationAccidentInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		AccidentInformationRequest businessResult = service.createDestinationAccidentInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		AccidentInformationRequestReturnType result = new AccidentInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
    @Override
    public WeatherInformationRequestReturnType sendDestinationWeatherInformationRequest(SendRequestTypeWithLoop parameters) {
    	logger.info("CALLED sendDestinationWeatherInformationRequest ON SEADA-TRAFFIC");	
    	logger.info("sendDestinationWeatherInformationRequest - loopIndexes= " + parameters.getLoopIndexes());
    	ChoreographyLoopIndexes choreographyLoopIndexes = new ChoreographyLoopIndexes(parameters.getLoopIndexes());
    	//logger.info("sendDestinationWeatherInformationRequest - choreographyLoopIndexes.loopIndexes.size()= " + choreographyLoopIndexes.loopIndexes.size());
    	ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		WeatherInformationRequest businessResult = service.createDestinationWeatherInformationRequest(store, parameters.getChoreographyTaskName(), parameters.getReceiverParticipantName(), choreographyLoopIndexes);
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getInputMessageName(), parameters.getChoreographyTaskName(), businessResult);
		WeatherInformationRequestReturnType result = new WeatherInformationRequestReturnType();
		result.setInputMessageData(businessResult);
	 
		return result;
    }
     
	@Override
	public ReceiveCongestionStatusResponseReturnType receiveCongestionStatusResponse(ReceiveCongestionStatusResponseType parameters) {
		logger.info("CALLED receiveCongestionStatusResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveCongestionStatusResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveCongestionStatusResponseReturnType();
	}
     
	@Override
	public ReceiveBridgeStatusInformationResponseReturnType receiveBridgeStatusInformationResponse(ReceiveBridgeStatusInformationResponseType parameters) {
		logger.info("CALLED receiveBridgeStatusInformationResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveBridgeStatusInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveBridgeStatusInformationResponseReturnType();
	}
     
	@Override
	public ReceiveOriginAccidentInformationResponseReturnType receiveOriginAccidentInformationResponse(ReceiveOriginAccidentInformationResponseType parameters) {
		logger.info("CALLED receiveOriginAccidentInformationResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveOriginAccidentInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveOriginAccidentInformationResponseReturnType();
	}
     
	@Override
	public ReceiveOriginWeatherInformationResponseReturnType receiveOriginWeatherInformationResponse(ReceiveOriginWeatherInformationResponseType parameters) {
		logger.info("CALLED receiveOriginWeatherInformationResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveOriginWeatherInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveOriginWeatherInformationResponseReturnType();
	}
     
	@Override
	public ReceiveDestinationAccidentInformationResponseReturnType receiveDestinationAccidentInformationResponse(ReceiveDestinationAccidentInformationResponseType parameters) {
		logger.info("CALLED receiveDestinationAccidentInformationResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveDestinationAccidentInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveDestinationAccidentInformationResponseReturnType();
	}
     
	@Override
	public ReceiveDestinationWeatherInformationResponseReturnType receiveDestinationWeatherInformationResponse(ReceiveDestinationWeatherInformationResponseType parameters) {
		logger.info("CALLED receiveDestinationWeatherInformationResponse ON SEADA-TRAFFIC");			
		ChoreographyInstanceMessagesStore store = choreographyDataService.getChoreographyInstanceMessages(parameters.getChoreographyId().getChoreographyId());
		store.storeMessage(parameters.getSenderParticipantName(), parameters.getReceiverParticipantName(), parameters.getOutputMessageName(), parameters.getChoreographyTaskName(), parameters.getOutputMessageData());
		service.receiveDestinationWeatherInformationResponse(parameters.getOutputMessageData(), parameters.getChoreographyTaskName(), parameters.getSenderParticipantName());
		return new ReceiveDestinationWeatherInformationResponseReturnType();
	}
     

	
}
