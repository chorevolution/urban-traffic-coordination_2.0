package eu.chorevolution.prosumer.seadatraffic.business;

import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusRequest;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusResponse;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationResponse;

public interface SEADATRAFFICService {

	void getRouteTrafficInformation(TrafficRouteInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
    TrafficRouteInformationResponse createTrafficRouteInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    CongestionStatusRequest createCongestionStatusRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    BridgeStatusInformationRequest createBridgeStatusInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    AccidentInformationRequest createOriginAccidentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    WeatherInformationRequest createOriginWeatherInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    AccidentInformationRequest createDestinationAccidentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    WeatherInformationRequest createDestinationWeatherInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
    void receiveCongestionStatusResponse(CongestionStatusResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveBridgeStatusInformationResponse(BridgeStatusInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveOriginAccidentInformationResponse(AccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveOriginWeatherInformationResponse(WeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveDestinationAccidentInformationResponse(AccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
    void receiveDestinationWeatherInformationResponse(WeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
    
}
