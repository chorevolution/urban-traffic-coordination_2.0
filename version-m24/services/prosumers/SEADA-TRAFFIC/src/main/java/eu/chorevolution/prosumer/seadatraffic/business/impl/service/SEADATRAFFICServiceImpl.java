package eu.chorevolution.prosumer.seadatraffic.business.impl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.seadatraffic.AccidentInformationData;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.AccidentInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.BridgeStatusInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusRequest;
import eu.chorevolution.prosumer.seadatraffic.CongestionStatusResponse;
import eu.chorevolution.prosumer.seadatraffic.ExtraDataWaypoint;
import eu.chorevolution.prosumer.seadatraffic.ExtraDataWaypoints;
import eu.chorevolution.prosumer.seadatraffic.RouteSegment;
import eu.chorevolution.prosumer.seadatraffic.RouteSegmentCongestionStatusData;
import eu.chorevolution.prosumer.seadatraffic.RouteSegmentData;
import eu.chorevolution.prosumer.seadatraffic.SituationType;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.TrafficRouteSegmentInformation;
import eu.chorevolution.prosumer.seadatraffic.Waypoint;
import eu.chorevolution.prosumer.seadatraffic.WeatherCondition;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationRequest;
import eu.chorevolution.prosumer.seadatraffic.WeatherInformationResponse;
import eu.chorevolution.prosumer.seadatraffic.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.seadatraffic.business.SEADATRAFFICService;
import eu.chorevolution.prosumer.seadatraffic.model.ChoreographyLoopIndexes;

@Service
public class SEADATRAFFICServiceImpl implements SEADATRAFFICService {

	@Override
	public void getRouteTrafficInformation(TrafficRouteInformationRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of TrafficRouteInformationRequest message from senderParticipantName
		*/
	}     

    @Override
    public TrafficRouteInformationResponse createTrafficRouteInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	

    	System.out.println("reached SEADATRAFFICServiceImpl.createTrafficRouteInformationResponse");
		TrafficRouteInformationResponse trafficRouteInformationResponse = new TrafficRouteInformationResponse();
		
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		System.out.println("CREATING TRAFFIC ROUTE INFORMATION RESPONSE FOR ROUTE NUMBER: "+indexValue);		
		List<WeatherInformationResponse> weatherOrigins = choreographyInstanceMessages.getMessages("originWeatherInformationResponse");
		List<WeatherInformationResponse> weatherDestinations = choreographyInstanceMessages.getMessages("destinationWeatherInformationResponse");
		List<AccidentInformationResponse> accidentOrigins = choreographyInstanceMessages.getMessages("originAccidentInformationResponse");
		List<AccidentInformationResponse> accidentDestinations = choreographyInstanceMessages.getMessages("destinationAccidentInformationResponse");
		List<BridgeStatusInformationResponse> bridgeResponses = choreographyInstanceMessages.getMessages("bridgeStatusInformationResponse");
		CongestionStatusResponse congestionStatusResponse = (CongestionStatusResponse) choreographyInstanceMessages.getMessage("congestionStatusResponse");
		/*System.out.println("accidentOrigins size: " + accidentOrigins.size());
		System.out.println("accidentDestinations size: " + accidentDestinations.size());
		System.out.println("weatherOrigins size: " + accidentOrigins.size());
		System.out.println("weatherDestinations size: " + accidentDestinations.size());
		System.out.println("bridgeResponses size: " + bridgeResponses.size());
		System.out.println("CongestionStatusResponse: " + congestionStatusResponse);*/
		for (RouteSegmentCongestionStatusData segment : congestionStatusResponse.getRouteSegmentsCongestionStatus()) {
			ExtraDataWaypoints extraDataWaypoints = new ExtraDataWaypoints();
			
			ExtraDataWaypoint speedContainer = new ExtraDataWaypoint(); //inside latitude of this this waypoint we will store the ecospeed, longitude 0 will indicate that this waypoint is ecospeed container
			speedContainer.setLongitude(0);
			speedContainer.setLatitude(segment.getRouteSegmentCongestionStatus().getSpeed());
			extraDataWaypoints.getExtraDataWaypointsInformation().add(speedContainer);//The dirty hack introduced in order to bypass the absence of ecospeed field in TrafficRouteInformationResponse object
			
			int index = congestionStatusResponse.getRouteSegmentsCongestionStatus().indexOf(segment);
			TrafficRouteSegmentInformation trafficRouteSegmentInformation = new TrafficRouteSegmentInformation();
			trafficRouteSegmentInformation.setRouteSegment(segment.getRouteSegmentCongestionStatus().getRouteSegment());
			trafficRouteSegmentInformation.setCongestionLevel(segment.getRouteSegmentCongestionStatus().getCongestionLevel());
			WeatherCondition weather = new WeatherCondition();
			weather.setAirRelativeHumidity(weatherOrigins.get(index).getAirRelativeHumidity());
			weather.setAirTemperature(weatherOrigins.get(index).getAirTemperature());
			weather.setRoadTemperature(weatherOrigins.get(index).getRoadTemperature());
			weather.setWindForce(weatherOrigins.get(index).getWindForce());
			trafficRouteSegmentInformation.setWeatherCondition(weather);
			for (AccidentInformationData accident : accidentOrigins.get(index).getAccidents()) {
				ExtraDataWaypoint extraDataWaypoint = new ExtraDataWaypoint();
				extraDataWaypoint.setLatitude(accident.getAccidentInformation().getLatitude());
				extraDataWaypoint.setLongitude(accident.getAccidentInformation().getLongitude());
				SituationType accidentType;
				try {
					accidentType = SituationType.fromValue(accident.getAccidentInformation().getMessageCodeValue());
				    } catch (IllegalArgumentException ex) {  
				    	accidentType = SituationType.OTHER;
				}
				System.out.println("accident: " + accidentType.value());
				extraDataWaypoint.setSituationType(accidentType);
				extraDataWaypoints.getExtraDataWaypointsInformation().add(extraDataWaypoint);
			}
			System.out.println("Segment number " + index + " has " + extraDataWaypoints.getExtraDataWaypointsInformation().size() + " accidents");
			trafficRouteSegmentInformation.setExtraDataWaypoints(extraDataWaypoints);
			trafficRouteInformationResponse.getTrafficRouteInformation().add(trafficRouteSegmentInformation);
		}
		
		// Here i have to assemble all responses from providers into trafficRouteSegmentInformation. I have to link corresponding responses between 
		
		//trafficRouteInformationResponse.getTrafficRouteInformation().add(indexValue, trafficRouteSegmentInformation);
		/**
		*	TODO write the code that generates EcoSpeedRoutesInformationResponse message
		*	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		*	You can exploit the following API from choreographyInstanceMessages
		*	1. Get a Message using its name 
		*		- example: 
		*			TrafficRouteInformationRequest message = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
		*		A null value is returned if no message has been found.
		*	2. Get a list of messages using a message name
		*		- example: 
		*			List<TrafficRouteInformationRequest> messages = choreographyInstanceMessages.getMessages("TrafficRouteInformationRequest");
		*		A null value is returned if no message has been found. 
		*	3. Get a message using its name and the name of the sender participant
		*		- example: 
		*			TrafficRouteInformationRequest message = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("TrafficRouteInformationRequest","SEADA-SEATSA");
		*		A null value is returned if no message has been found. 
		*	4. Get a list of messages using a message name and a sender participant name
		*		- example: 
		*			List<TrafficRouteInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("TrafficRouteInformationRequest","SEADA-SEATSA"); 
		*		A null value is returned if no message has been found. 
		*	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			TrafficRouteInformationRequest message = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessageSentFromParticipant("TrafficRouteInformationRequest","SEADA-SEATSA","Get Route Traffic Information");	
		*		A null value is returned if no message has been found.
		*	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<TrafficRouteInformationRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("TrafficRouteInformationRequest","SEADA-SEATSA","Get Route Traffic Information");
		*		A null value is returned if no message has been found.
		*	7. Get a message using its name and the name of the receiver participant
		*		- example: 
		*			CongestionStatusRequest message = (CongestionStatusRequest) choreographyInstanceMessages.getMessageReceivedParticipant("CongestionStatusRequest", "DTS-CONGESTION");
		*		A null value is returned if no message has been found. 
		*	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		*		the message has been exchanged
		*		- example: 
		*			CongestionStatusRequest message = (CongestionStatusRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("CongestionStatusRequest", "DTS-CONGESTION", "Route Congestion Information");	
		*		A null value is returned if no message has been found.
		*	9. Get a list of messages using a message name and a receiver participant name
		*		- example: 
        *			List<CongestionStatusRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("CongestionStatusRequest", "DTS-CONGESTION");
		*	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		*		the messages has been exchanged
		*		- example: 
		*			List<CongestionStatusRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("CongestionStatusRequest", "DTS-CONGESTION", "Route Congestion Information");
		*		A null value is returned if no message has been found.
		*/	
				
		/**
		 * 	NOTE: The creation of this message is part of a choreography task involved in a loop. 
		 * 	Using the name of the choreography task or sub-choreography looped you can get the value of 
		 * 	the related index and exploit this to retrieve information in order to generate DestinationAccidentInformationRequest message
		 * 		- example: 
		 * 			int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		 * 		The value -1 is returned if no index value has been found.
		 * 	NOTE: Each index item numbering begins with 0.
		 * 	NOTE: The value of EcoFriendlyRouteInformationCollection index is available only when Eco Friendly Route Information Collection
		 *  	is triggered by SEADA-SEATSA after Get Eco Speed Route Information choreography task
		 */			
		return trafficRouteInformationResponse;
    }
    
    @Override
    public CongestionStatusRequest createCongestionStatusRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	
    	//System.out.println("reached SEADATRAFFICServiceImpl.createCongestionStatusRequest");
    			System.out.println("CREATE CONGESTION REQUEST FOR ROUTE NUMBER: "+choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection"));
    			CongestionStatusRequest congestionStatusRequest = new CongestionStatusRequest();
    			TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("trafficRouteInformationRequest");
    			for (RouteSegment routeSegment : trafficRouteInformationRequest.getRouteSegments()) {
    				RouteSegmentData routeSegmentData = new RouteSegmentData();
    				routeSegmentData.setRouteSegment(routeSegment);
    				congestionStatusRequest.getRouteSegments().add(routeSegmentData);
    			}
    			return congestionStatusRequest;
    }
    
    @Override
    public BridgeStatusInformationRequest createBridgeStatusInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	

    	//System.out.println("reached SEADATRAFFICServiceImpl.createBridgeStatusInformationRequest");
    			int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
    			System.out.println("BRIDGE STATUS REQUEST OF SEGMENT#: "+indexValue);		
    			
    			TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
    			BridgeStatusInformationRequest bridgeStatusInformationRequest = new BridgeStatusInformationRequest();
    			bridgeStatusInformationRequest.setOrigin(trafficRouteInformationRequest.getRouteSegments().get(indexValue).getStart());
    			bridgeStatusInformationRequest.setDestination(trafficRouteInformationRequest.getRouteSegments().get(indexValue).getEnd());

    			return bridgeStatusInformationRequest;
    }
    
    @Override
    public AccidentInformationRequest createOriginAccidentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		System.out.println("CREATING ORIGIN ACCIDENT INFORMATION REQUEST FOR ROUTE SEGMENT NUMBER: "+indexValue);		
		
		TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
		Waypoint waypoint = trafficRouteInformationRequest.getRouteSegments().get(indexValue).getStart();

		AccidentInformationRequest accidentInformationRequest = new AccidentInformationRequest();
		accidentInformationRequest.setLat(waypoint.getLat());
		accidentInformationRequest.setLon(waypoint.getLon());

		return accidentInformationRequest;
    }
    
    @Override
    public WeatherInformationRequest createOriginWeatherInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	
    	System.out.println("reached SEADATRAFFICServiceImpl.createOriginWeatherInformationRequest");
		WeatherInformationRequest weatherInformationRequest = new WeatherInformationRequest();
		
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		System.out.println("CREATING ORIGIN WEATHER INFORMATION REQUEST FOR ROUTE SEGMENT NUMBER: "+indexValue);
		TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
		Waypoint waypoint = trafficRouteInformationRequest.getRouteSegments().get(indexValue).getStart();

		weatherInformationRequest.setLat(waypoint.getLat());
		weatherInformationRequest.setLon(waypoint.getLon());
		return weatherInformationRequest;
    }
    
    @Override
    public AccidentInformationRequest createDestinationAccidentInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	
    	System.out.println("reached SEADATRAFFICServiceImpl.createDestinationAccidentInformationRequest");
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		System.out.println("CREATING DESTINATION ACCIDENT INFORMATION REQUEST FOR ROUTE SEGMENT NUMBER: "+indexValue);
		TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
		Waypoint waypoint = trafficRouteInformationRequest.getRouteSegments().get(indexValue).getEnd();

		AccidentInformationRequest accidentInformationRequest = new AccidentInformationRequest();	
		accidentInformationRequest.setLat(waypoint.getLat());
		accidentInformationRequest.setLon(waypoint.getLon());
		return accidentInformationRequest;
    	/**
		 *	TODO write the code that generates AccidentInformationRequest message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    }
    
    @Override
    public WeatherInformationRequest createDestinationWeatherInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
    	

    	System.out.println("reached SEADATRAFFICServiceImpl.createDestinationWeatherInformationRequest");
		WeatherInformationRequest weatherInformationRequest = new WeatherInformationRequest();
		
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("RouteSegmentTrafficInformationCollection");
		System.out.println("CREATING DESTINATION WEATHER INFORMATION REQUEST FOR ROUTE SEGMENT NUMBER: "+indexValue);
		
		TrafficRouteInformationRequest trafficRouteInformationRequest = (TrafficRouteInformationRequest) choreographyInstanceMessages.getMessage("TrafficRouteInformationRequest");
		Waypoint waypoint = trafficRouteInformationRequest.getRouteSegments().get(indexValue).getEnd();

		weatherInformationRequest.setLat(waypoint.getLat());
		weatherInformationRequest.setLon(waypoint.getLon());
		return weatherInformationRequest;
    }
    
	@Override    
    public void receiveCongestionStatusResponse(CongestionStatusResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (CongestionStatusResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveBridgeStatusInformationResponse(BridgeStatusInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (BridgeStatusInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveOriginAccidentInformationResponse(AccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (AccidentInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveOriginWeatherInformationResponse(WeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (WeatherInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveDestinationAccidentInformationResponse(AccidentInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (AccidentInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    
	@Override    
    public void receiveDestinationWeatherInformationResponse(WeatherInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
    	/**
		*	TODO Add your business logic upon the reception of (WeatherInformationResponse message from senderParticipantName 
		*		 within the interaction belonging to choreographyTaskName
		*/
    }
    

}
