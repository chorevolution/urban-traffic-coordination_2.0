package eu.chorevolution.prosumer.seadaseatsa.business;

import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequest;

public interface SEADASEATSAService {

	void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequest parameter, String choreographyTaskName, String senderParticipantName);
	      
	void setRouteTrafficInformation(TrafficRouteInformationResponse parameter, String choreographyTaskName, String senderParticipantName);
	      
    EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    EcoSpeedRoutesInformationResponse createEcoSpeedRoutesInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName);
    
    TrafficRouteInformationRequest createTrafficRouteInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes);
    
}
