package eu.chorevolution.prosumer.seadaseatsa.business.impl.service;

import org.springframework.stereotype.Service;

import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.EcoSpeedRoutesInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.RouteSegment;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationRequest;
import eu.chorevolution.prosumer.seadaseatsa.TrafficRouteInformationResponse;
import eu.chorevolution.prosumer.seadaseatsa.Waypoint;
import eu.chorevolution.prosumer.seadaseatsa.WaypointInfo;
import eu.chorevolution.prosumer.seadaseatsa.business.ChoreographyInstanceMessages;
import eu.chorevolution.prosumer.seadaseatsa.business.SEADASEATSAService;
import eu.chorevolution.prosumer.seadaseatsa.model.ChoreographyLoopIndexes;

@Service
public class SEADASEATSAServiceImpl implements SEADASEATSAService {

	@Override
	public void getEcoSpeedRouteInformation(EcoSpeedRouteInformationRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of EcoSpeedRouteInformationRequest message from senderParticipantName
		*/
	}     

	@Override
	public void getEcoSpeedRoutesInformation(EcoSpeedRoutesInformationRequest parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of EcoSpeedRoutesInformationRequest message from senderParticipantName
		*/
	}     

	@Override
	public void setRouteTrafficInformation(TrafficRouteInformationResponse parameter, String choreographyTaskName, String senderParticipantName) {
		/**
		*	TODO Add your business logic upon the reception of TrafficRouteInformationResponse message from senderParticipantName
		*/
	}     

    @Override
    public EcoSpeedRouteInformationResponse createEcoSpeedRouteInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	EcoSpeedRouteInformationResponse result = new EcoSpeedRouteInformationResponse();
    	/**
		 *	TODO write the code that generates EcoSpeedRouteInformationResponse message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public EcoSpeedRoutesInformationResponse createEcoSpeedRoutesInformationResponse(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName) {
    	EcoSpeedRoutesInformationResponse result = new EcoSpeedRoutesInformationResponse();
    	/**
		 *	TODO write the code that generates EcoSpeedRoutesInformationResponse message 
		 *	that has to be sent to receiverParticipantName within the interaction belonging to choreographyTaskName
		 *	You can exploit the following API from choreographyInstanceMessages
		 *	1. Get a Message using its name 
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessage("EcoRoutesRequest");
		 *		A null value is returned if no message has been found.
		 *	2. Get a list of messages using a message name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessages("EcoRoutesRequest");
		 *		A null value is returned if no message has been found. 
		 *	3. Get a message using its name and the name of the sender participant
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND");
		 *		A null value is returned if no message has been found. 
		 *	4. Get a list of messages using a message name and a sender participant name
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND"); 
		 *		A null value is returned if no message has been found.
		 *	5. Get a message using its name, the name of the sender participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			EcoRoutesRequest message = (EcoRoutesRequest) choreographyInstanceMessages.getMessageSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");	
		 *		A null value is returned if no message has been found.
		 *	6. Get a list of messages using a message name, the name of the sender participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<EcoRoutesRequest> messages = choreographyInstanceMessages.getMessagesSentFromParticipant("EcoRoutesRequest","ND","Get Eco Routes");
		 *		A null value is returned if no message has been found.
		 *	7. Get a message using its name and the name of the receiver participant
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedParticipant("RoutesRequest", "DTS-GOOGLE");
		 *		A null value is returned if no message has been found. 
		 *	8. Get a message using its name, the name of the receiver participant and the name of the task in which 
		 *		the message has been exchanged
		 *		- example: 
		 *			RoutesRequest message = (RoutesRequest) choreographyInstanceMessages.getMessageReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");;	
		 *		A null value is returned if no message has been found.
		 *	9. Get a list of messages using a message name and a receiver participant name
		 *		- example: 
         *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE");
		 *	10. Get a list of messages using a message name, the name of the receiver participant and the name of the task in which 
		 *		the messages has been exchanged
		 *		- example: 
		 *			List<RoutesRequest> messages = choreographyInstanceMessages.getMessagesReceivedFromParticipant("RoutesRequest", "DTS-GOOGLE", "Routes Request");
		 *		A null value is returned if no message has been found.
		 */	
    	return result;
    }
    
    @Override
    public TrafficRouteInformationRequest createTrafficRouteInformationRequest(ChoreographyInstanceMessages choreographyInstanceMessages, String choreographyTaskName, String receiverParticipantName, ChoreographyLoopIndexes choreographyLoopIndexes) {
System.out.println("reached SEADASEATSAServiceImpl.createTrafficRouteInformationRequest");
		
		TrafficRouteInformationRequest trafficRouteInformationRequest = new TrafficRouteInformationRequest();
		EcoSpeedRoutesInformationRequest ecoSpeedRoutesInformationRequest = (EcoSpeedRoutesInformationRequest) choreographyInstanceMessages.getMessage("ecoSpeedRoutesInformationRequest");
		int indexValue = choreographyLoopIndexes.getChoreographyLoopIndex("EcoFriendlyRouteInformationCollection");
		System.out.println("CREATING TRAFFIC ROUTE INFORMATION REQUEST FOR ROUTE NUMBER: "+indexValue);		
		System.out.println("SEADASEATSAServiceImpl.createTrafficRouteInformationRequest EcoFriendlyRouteInformationCollection index: " + indexValue); //indexValue is 0
		// i take the route from TrafficRouteInformationRequest based on the indexValue
		//i iterate through all waypoints that came from the routes and devide the routes into segments
		Waypoint origin = null;
		for (WaypointInfo waypoint : ecoSpeedRoutesInformationRequest.getRoutes().get(indexValue).getWaypoints()) {
			if (origin != null){
				RouteSegment segment = new RouteSegment();		
				Waypoint start = new Waypoint();
				start.setLat(origin.getLat());
				start.setLon(origin.getLon());
				segment.setStart(start);
				Waypoint destination = new Waypoint();
				destination.setLat(waypoint.getWaypoint().getLat());
				destination.setLon(waypoint.getWaypoint().getLon());
				segment.setEnd(destination);
				origin.setLat(destination.getLat());
				origin.setLon(destination.getLon());
				trafficRouteInformationRequest.getRouteSegments().add(segment);
			} else{
				origin = new Waypoint();
				origin.setLat(waypoint.getWaypoint().getLat());
				origin.setLon(waypoint.getWaypoint().getLon());
			}
		}	
		System.out.println("Number of routeSegments in trafficRouteInformationRequest: " + trafficRouteInformationRequest.getRouteSegments().size());
		System.out.println("ROUTE NUMBER: "+indexValue+" ROUTE SEGMENTS NUMBER: "+trafficRouteInformationRequest.getRouteSegments().size());
		for (RouteSegment segment : trafficRouteInformationRequest.getRouteSegments()) {
			System.out.println("Route segment in trafficRequest: {" + segment.getStart().getLat() + " , " + segment.getStart().getLon() + "}, {" + segment.getEnd().getLat() + " , " + segment.getEnd().getLon() + "}");
		}
		return trafficRouteInformationRequest;
    }
    

}
